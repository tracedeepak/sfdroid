package nansat.com.safebio;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import nansat.com.safebio.models.Device;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 08/02/18.
 */

public class DeviceListAdapter extends BaseAdapter {
    List<Device> mDeviceList;

    public DeviceListAdapter(List<Device> mDeviceList) {
        this.mDeviceList = mDeviceList;
    }

    @Override
    public int getCount() {
        return mDeviceList.size();
    }

    @Override
    public Device getItem(int i) {
        return mDeviceList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_device, viewGroup, false);
        AppTextView deviceName = (AppTextView) v.findViewById(R.id.deviceName);
        AppTextView deviceAddress = (AppTextView) v.findViewById(R.id.deviceAddress);
        deviceName.setText(mDeviceList.get(i).getDeviceName());
        deviceAddress.setText(mDeviceList.get(i).getDeviceAddress());

        return v;
    }
}
