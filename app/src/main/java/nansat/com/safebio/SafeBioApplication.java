package nansat.com.safebio;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;

import com.bugfender.sdk.Bugfender;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import nansat.com.safebio.webservices.SafeBioClient;

/**
 * Created by Phantasmist on 07/02/18.
 */

public class SafeBioApplication extends Application {
    private static SafeBioApplication mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        new SafeBioClient(this);
        Bugfender.init(this, "zu57pEFJAwpIfAa1C72j97k68AVb9ZbE", BuildConfig.DEBUG);
        Bugfender.enableLogcatLogging();
        Bugfender.enableUIEventLogging(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mInstance = null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static SafeBioApplication getInstance() {
        return mInstance;
    }




}
