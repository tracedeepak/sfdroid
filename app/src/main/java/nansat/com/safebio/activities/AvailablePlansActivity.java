package nansat.com.safebio.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.ViewFlipper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import nansat.com.safebio.R;
import nansat.com.safebio.adapters.AvailablePlansAdapter;
import nansat.com.safebio.contracts.AvailablePlansActContract;
import nansat.com.safebio.databinding.ActivityAvailablePlansBinding;
import nansat.com.safebio.datamodels.AvailablePlansViewModel;
import nansat.com.safebio.models.AvailablePlansResponse;
import nansat.com.safebio.models.SubscribePlanRequest;
import nansat.com.safebio.presenter.AvailablePlansPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 08/03/18.
 */

public class AvailablePlansActivity extends BaseActivity implements AvailablePlansActContract {

    private Toolbar mSubscriptionToolBar;
    private RecyclerView mSubscriptionRecycler;
    GridLayoutManager gridManager;
    AvailablePlansPresenter mAvailablePlansPresenter;
    AvailablePlansAdapter mAvailablePlansAdapter;
    List<AvailablePlansResponse.Data> mAvailablePlansList = new ArrayList<>();
    private AppTextView mToolbarTitle;
    ViewFlipper mSubscriptionFlipper;

    ActivityAvailablePlansBinding mActivityAvailablePlansBinding;
    AvailablePlansResponse.Data mSelecetedPlan;
    String ref = "";
    AvailablePlansViewModel model;

    private void assignViews() {
        mSubscriptionToolBar = findViewById(R.id.subscriptionToolBar);
        mSubscriptionRecycler = findViewById(R.id.subscriptionRecycler);
        gridManager = new GridLayoutManager(this, 2);
        mSubscriptionFlipper = findViewById(R.id.subscriptionFlipper);

        mToolbarTitle = mSubscriptionToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Subscribe Now");
        setSupportActionBar(mSubscriptionToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityAvailablePlansBinding = DataBindingUtil.setContentView(this, R.layout.activity_available_plans);
        // Call the function callInstamojo to start payment here
        assignViews();
        model = new AvailablePlansViewModel();
        mAvailablePlansPresenter = new AvailablePlansPresenter(this);
        mActivityAvailablePlansBinding.setPresenter(mAvailablePlansPresenter);
        mActivityAvailablePlansBinding.setInputData(model);

        mSubscriptionRecycler.setLayoutManager(gridManager);
        mAvailablePlansAdapter = new AvailablePlansAdapter(mAvailablePlansList, mAvailablePlansPresenter);
        mAvailablePlansAdapter.setHasStableIds(true);
        mSubscriptionRecycler.setAdapter(mAvailablePlansAdapter);
        mSubscriptionRecycler.setHasFixedSize(true);

        //Call the api
        mAvailablePlansPresenter.getActiveAvailablePlans();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (mSubscriptionFlipper.getDisplayedChild() == 1) {
            mToolbarTitle.setText("Subscribe Now");
            mSubscriptionFlipper.setDisplayedChild(0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Activity provideContext() {
        return this;
    }

    @Override
    public void proceedToPaymentOptions(AvailablePlansResponse.Data mData) {
        mSelecetedPlan = mData;
        if (mData.getBase_cost().equals("0")) {
            mActivityAvailablePlansBinding.getInputData().setReferenceNumber("000");
            mAvailablePlansPresenter.submitPayment(false);
        } else {
            mToolbarTitle.setText("Payment");
            mSubscriptionFlipper.setDisplayedChild(1);
        }
    }

    @Override
    public void inflateAvailablePlansToGrid(List<AvailablePlansResponse.Data> data) {
        if (!mAvailablePlansList.isEmpty()) {
            mAvailablePlansList.clear();
            mAvailablePlansAdapter.notifyDataSetChanged();
        }
        mAvailablePlansList.addAll(data);
        mAvailablePlansAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean validateReferenceNum() {
        if (TextUtils.isEmpty(mActivityAvailablePlansBinding.getInputData().getReferenceNumber())) {
            Logger.toast(this, "Please provide reference cheque number.");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public SubscribePlanRequest prepareSubscribePlanRequest() {
        SubscribePlanRequest mSubscribePlanRequest = new SubscribePlanRequest();
        mSubscribePlanRequest.setApi_token(Utils.getString(this, Constant.KEY_TOKEN));
        mSubscribePlanRequest.setPayment_mode("offline");
        mSubscribePlanRequest.setPayment_ref_number(mActivityAvailablePlansBinding.getInputData().getReferenceNumber());
        mSubscribePlanRequest.setPlan_id(mSelecetedPlan.getId());
        return mSubscribePlanRequest;
    }

    @Override
    public void startNextActivity() {
        if (Utils.getBoolean(this, Constant.SUBSCRIPTION_TAKEN)) {
            if (!Utils.getBoolean(this, Constant.KEY_ACTIVATION_STATUS)) {
                Intent in = new Intent(this, CallSupportActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                navigateToNextActivity(in, true);
            } else {
                Intent in = new Intent(this, HomeActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                navigateToNextActivity(in, true);
            }
        }
    }

    @Override
    public void callInstaMojoPay() {
        String email = Utils.getString(this, Constant.KEY_USER_EMAIL);
        String phone = Utils.getString(this, Constant.KEY_USER_PHONE);
        String name = Utils.getString(this, Constant.KEY_USER_NAME);
        callInstamojoPay(email, phone, "" + mSelecetedPlan.getBase_cost(), "plan buy", name);
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
                        .show();
                String[] responseData = response.split(":");
                for (int i = 0; i < responseData.length; i++) {
                    if (responseData[i].contains("paymentId")) {
                        String[] paymentDetails = responseData[i].split("=");
                        SubscribePlanRequest mSubscribePlanRequest = new SubscribePlanRequest();
                        mSubscribePlanRequest.setApi_token(Utils.getString(AvailablePlansActivity.this, Constant.KEY_TOKEN));
                        mSubscribePlanRequest.setPayment_mode("online");
                        mSubscribePlanRequest.setPayment_ref_number(paymentDetails[1]);
                        mSubscribePlanRequest.setPlan_id(mSelecetedPlan.getId());
                        mAvailablePlansPresenter.submitPaymentOnline(mSubscribePlanRequest);
                    }
                }
                Log.e("PAYMENT RESPONSE", "" + response);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }
}
