package nansat.com.safebio.activities;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.annimon.stream.Stream;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import nansat.com.safebio.R;
import nansat.com.safebio.adapters.CategoriesAdapter;
import nansat.com.safebio.adapters.OrderSummaryRecAdapter;
import nansat.com.safebio.adapters.ProductsAdapter;
import nansat.com.safebio.contracts.BrowseProductActContract;
import nansat.com.safebio.models.CreateOrderRequest;
import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.MyAccountResponse;
import nansat.com.safebio.models.ProductCategoryResponse;
import nansat.com.safebio.models.ProductsResponse;
import nansat.com.safebio.models.SendOrderEnquiryReq;
import nansat.com.safebio.models.UpdateOrderRequest;
import nansat.com.safebio.presenter.BrowseProductsActPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.RecyclerItemClickListener;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class BrowseProductsActivity extends BaseActivity implements BrowseProductActContract {

    private Toolbar mOrderToolBar;
    private RecyclerView mCategoryRecyclerView;
    private RecyclerView mProductsRecView;
    private RecyclerView mOrderReviewRecView;
    private AppTextView mToolbarTitle;
    private AppTextView mTotalQtyTV;
    private AppTextView mTotalPriceTV;
    private ViewFlipper mFlipper;
    private AppButton mOrderNowButt;
    private AppButton mPayNowButt;
    private AppButton mCodButt;
    CategoriesAdapter mCategoriesAdapter;
    ProductsAdapter mProductsAdapter;
    OrderSummaryRecAdapter mOrderSummaryRecAdapter;

    List<ProductCategoryResponse.Data> mCategoryList = new ArrayList<>();
    List<ProductsResponse.Data> mProductList = new ArrayList<>();
    List<ProductsResponse.Data> mSelectedProductsList = new ArrayList<>();

    BrowseProductsActPresenter mBrowseProductsActPresenter;
    String selectedProduct;
    double totalAmount = 0;


    private AppTextView mCgstTaxValue;
    private AppTextView mTandCTV;
    private AppTextView mIgstTaxValue;
    private AppTextView mCgstTaxPercent;
    private AppTextView mIgstTaxPercent;
    private AppTextView mGrandTotalTextView;
    MyAccountResponse.Hcf mHcfData;
    private LocationDTO mLocationsModel;
    private AppTextView mDeliveryChargesValue;
    private int postalCharge;
    private InstamojoPay instamojoPay;
    private IntentFilter filter;
    private String userRole;

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    private void assignViews() {
        mOrderToolBar = findViewById(R.id.orderToolBar);
        mCategoryRecyclerView = findViewById(R.id.categoryRecyclerView);
        mProductsRecView = findViewById(R.id.productsRecView);
        mOrderReviewRecView = findViewById(R.id.reviewRecView);
        mFlipper = findViewById(R.id.flipper);
        mTotalQtyTV = findViewById(R.id.appTextView10);
        mTotalPriceTV = findViewById(R.id.appTextView13);
        mCgstTaxValue = findViewById(R.id.appTextView133);
        mIgstTaxValue = findViewById(R.id.appTextView1333);
        mDeliveryChargesValue = findViewById(R.id.appTextView133333);
        mCgstTaxPercent = findViewById(R.id.appTextView122);
        mIgstTaxPercent = findViewById(R.id.appTextView1222);
        mGrandTotalTextView = findViewById(R.id.appTextView13333);
        mOrderNowButt = findViewById(R.id.orderNowButton);
        mPayNowButt = findViewById(R.id.payNowButton);
        mCodButt = findViewById(R.id.codButton);
        mTandCTV = findViewById(R.id.termsAndCondTV);
        mToolbarTitle = mOrderToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Order Now");
        setSupportActionBar(mOrderToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        GridLayoutManager categoryManager = new GridLayoutManager(this, 2);
        GridLayoutManager productsManager = new GridLayoutManager(this, 1);
        mBrowseProductsActPresenter = new BrowseProductsActPresenter(this);

        mCategoryRecyclerView.setLayoutManager(categoryManager);
        mCategoriesAdapter = new CategoriesAdapter(mCategoryList);
        mCategoriesAdapter.setHasStableIds(true);
        mCategoryRecyclerView.setAdapter(mCategoriesAdapter);
        mCategoryRecyclerView.setHasFixedSize(true);

        mProductsRecView.setLayoutManager(productsManager);
        mProductsAdapter = new ProductsAdapter(mProductList, mBrowseProductsActPresenter);
        mProductsAdapter.setHasStableIds(true);
        mProductsRecView.setAdapter(mProductsAdapter);
        mProductsRecView.setHasFixedSize(true);

        mOrderReviewRecView.setLayoutManager(new LinearLayoutManager(this));
        mOrderSummaryRecAdapter = new OrderSummaryRecAdapter(mSelectedProductsList);
        mOrderSummaryRecAdapter.setHasStableIds(true);
        mOrderReviewRecView.setAdapter(mOrderSummaryRecAdapter);
        mOrderReviewRecView.setHasFixedSize(true);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    int categoryId = 0;
    static int CATEGORY_ID_WEIGHINGSCALES = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_products);
        // Call the function callInstamojo to start payment here
        assignViews();

        userRole = Utils.getString(this, Constant.KEY_USER_ROLE);
        if (userRole.toUpperCase().equals(Constant.ROLE_HCF.toUpperCase())) {
            mPayNowButt.setVisibility(View.VISIBLE);
            mCodButt.setVisibility(View.GONE);
            mBrowseProductsActPresenter.getStates();
        } else {
            mPayNowButt.setVisibility(View.GONE);
            mCodButt.setVisibility(View.VISIBLE);
        }

        mTandCTV.setVisibility(View.GONE);

        mOrderNowButt.setVisibility(View.GONE);
        mBrowseProductsActPresenter.fetchCategories();
        mCategoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                selectedProduct = mCategoriesAdapter.getItemAtPosition(position).getName();
                mToolbarTitle.setText(selectedProduct);
                mBrowseProductsActPresenter.fetchProductsByCategory(mCategoriesAdapter.getItemAtPosition(position).getId());
                categoryId = mCategoriesAdapter.getItemAtPosition(position).getId();
                if (categoryId == CATEGORY_ID_WEIGHINGSCALES) {
                    mTandCTV.setVisibility(View.VISIBLE);
                } else {
                    mTandCTV.setVisibility(View.GONE);
                }
            }
        }));


        mProductsRecView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        }));

        mOrderNowButt.setOnClickListener((v) -> {
            showOrderSummary();
        });

        mPayNowButt.setOnClickListener((v) -> {
            mBrowseProductsActPresenter.createOrder(prepareCreatOrderRequest());
//            orderid leni hai
//            callInstamojoPay("email@s.com","8054427521","400","test","UDit");
//            callInstamojoPay("tester@gmail.com", "7875432991", "10", "official", "buyername", orderId);

        });
        mCodButt.setOnClickListener(v->{
            mBrowseProductsActPresenter.sendOrderEnquiry(prepareEnquiryRequest());
        });

    }


    @Override
    public Activity provideContext() {
        return this;
    }

    @Override
    public void populateCategoriesRecView(List<ProductCategoryResponse.Data> data) {
        if (!mCategoryList.isEmpty()) {
            mCategoryList.clear();
            mCategoriesAdapter.notifyDataSetChanged();
        }
        mCategoryList.addAll(data);
        mCategoriesAdapter.notifyDataSetChanged();
    }

    @Override
    public void populateProductsRecView(List<ProductsResponse.Data> data) {
        mFlipper.setDisplayedChild(1);
        mOrderNowButt.setVisibility(View.VISIBLE);
        if (!mProductList.isEmpty()) {
            mProductList.clear();
            mProductsAdapter.notifyDataSetChanged();
        }
        mProductList.addAll(data);
        mProductsAdapter.notifyDataSetChanged();
    }

    @Override
    public void incrementValByFactor(int val, int factor, int position, int min_order) {
        if (val == 0) {
            val = min_order;
        }
        mProductsAdapter.getItemAtPosition(position).setQuantity(val + factor);
        int newQty = mProductsAdapter.getItemAtPosition(position).getQuantity();
        double price = Double.parseDouble(mProductsAdapter.getItemAtPosition(position).getPrice());
        double totalPriceOfAllItems = newQty * price;
        mProductsAdapter.getItemAtPosition(position).setTotalPrice(totalPriceOfAllItems);
    }

    @Override
    public void changeCartValueOfProduct(int position, boolean addToCart) {
        mProductsAdapter.getItemAtPosition(position).setAddToCart(addToCart);
    }

    @Override
    public void decrementValByFactor(int val, int factor, int position, int min_order) {
        if (val == 0) {
            val = min_order;
        }
        mProductsAdapter.getItemAtPosition(position).setQuantity(val - factor);
        int newQty = mProductsAdapter.getItemAtPosition(position).getQuantity();
        double price = Double.parseDouble(mProductsAdapter.getItemAtPosition(position).getPrice());
        double totalPriceOfAllItems = newQty * price;
        mProductsAdapter.getItemAtPosition(position).setTotalPrice(totalPriceOfAllItems);
    }

    public void showOrderSummary() {


        if (!mSelectedProductsList.isEmpty()) {
            mSelectedProductsList.clear();
            mOrderSummaryRecAdapter.notifyDataSetChanged();
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            mSelectedProductsList.addAll(mProductList.stream().filter(data -> data.getQuantity() >= data.getMin_order_qty()).filter(data -> data.isAddToCart() == true).collect(Collectors.toList()));
            if (mSelectedProductsList.isEmpty()) {
                Logger.toast(this, "Please Select at least one product.");
            } else {
                mOrderNowButt.setVisibility(View.GONE);
                int allQTYSum = mSelectedProductsList.stream().collect(Collectors.summingInt(ProductsResponse.Data::getQuantity));
                double allPriceSum = mSelectedProductsList.stream().collect(Collectors.summingDouble(ProductsResponse.Data::getTotalPrice));
//                double totalCgst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getCgst()) / 100);
                double totalGst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getGst()) / 100);
//                double totalIgst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getIgst()) / 100);
                mTotalQtyTV.setText("" + allQTYSum);
                mTotalPriceTV.setText("" + allPriceSum);
                mCgstTaxPercent.setText(mSelectedProductsList.get(0).getGst() + "%");
//                mIgstTaxPercent.setText(mSelectedProductsList.get(0).getIgst() + "%");
                mCgstTaxValue.setText("" + round(totalGst, 2));
//                mIgstTaxValue.setText("" + round(totalIgst, 2));

//                mGrandTotalTextView.setText("" + (allPriceSum + round(totalCgst, 2) + round(totalIgst, 2) + Double.valueOf(postalCharge)));
                if (categoryId == CATEGORY_ID_WEIGHINGSCALES) {
                    mDeliveryChargesValue.setText("-");
                    mGrandTotalTextView.setText("" + (allPriceSum + round(totalGst, 2)));
                    setTotalAmount(allPriceSum + round(totalGst, 2));
                } else {
                    mDeliveryChargesValue.setText("" + postalCharge);
                    mGrandTotalTextView.setText("" + (allPriceSum + round(totalGst, 2) + Double.valueOf(postalCharge)));
                    setTotalAmount(allPriceSum + round(totalGst, 2) + Double.valueOf(postalCharge));
                }
//                setTotalAmount(allPriceSum);
                Log.e("SELECTED DATA", "" + mSelectedProductsList.size());
                mOrderSummaryRecAdapter.notifyDataSetChanged();
                mToolbarTitle.setText("Order Summary");
                mFlipper.setDisplayedChild(2);
            }
        } else {
            mSelectedProductsList.addAll(com.annimon.stream.Stream.of(mProductList).filter(data -> data.getQuantity() >= data.getMin_order_qty()).collect(com.annimon.stream.Collectors.toList()));
            if (mSelectedProductsList.isEmpty()) {
                Logger.toast(this, "Please Select at least one product.");
            } else {
                mOrderNowButt.setVisibility(View.GONE);
                int allQTYSum = Stream.of(mSelectedProductsList).collect(com.annimon.stream.Collectors.summingInt(ProductsResponse.Data::getQuantity));
                double allPriceSum = Stream.of(mSelectedProductsList).collect(com.annimon.stream.Collectors.summingDouble(ProductsResponse.Data::getTotalPrice));
//                double totalCgst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getCgst()) / 100);
//                double totalIgst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getIgst()) / 100);
                double totalGst = allPriceSum * (Double.valueOf(mSelectedProductsList.get(0).getGst()) / 100);

                mTotalQtyTV.setText("" + allQTYSum);
                mTotalPriceTV.setText("" + allPriceSum);
                mCgstTaxPercent.setText(mSelectedProductsList.get(0).getGst() + "%");
//                mCgstTaxPercent.setText(mSelectedProductsList.get(0).getCgst() + "%");
//                mIgstTaxPercent.setText(mSelectedProductsList.get(0).getIgst() + "%");
                mCgstTaxValue.setText("" + round(totalGst, 2));
//                mCgstTaxValue.setText("" + round(totalCgst, 2));
//                mIgstTaxValue.setText("" + round(totalIgst, 2));
                if (categoryId == CATEGORY_ID_WEIGHINGSCALES) {
                    mDeliveryChargesValue.setText("-");
                    mGrandTotalTextView.setText("" + (allPriceSum + round(totalGst, 2)));
                    setTotalAmount(allPriceSum + round(totalGst, 2));
                } else {
                    mDeliveryChargesValue.setText("" + postalCharge);
                    mGrandTotalTextView.setText("" + (allPriceSum + round(totalGst, 2) + Double.valueOf(postalCharge)));
                    setTotalAmount(allPriceSum + round(totalGst, 2) + Double.valueOf(postalCharge));
                }
//                setTotalAmount(allPriceSum);
//                mGrandTotalTextView.setText("" + (allPriceSum + round(totalCgst, 2) + round(totalIgst, 2) + Double.valueOf(postalCharge)));
                Log.e("SELECTED DATA", "" + mSelectedProductsList.size());
                setTotalAmount(allPriceSum);
                mOrderSummaryRecAdapter.notifyDataSetChanged();
                mToolbarTitle.setText("Order Summary");
                mFlipper.setDisplayedChild(2);
            }
        }


    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public CreateOrderRequest prepareCreatOrderRequest() {
        CreateOrderRequest mCreateOrderRequest = new CreateOrderRequest();
        mCreateOrderRequest.setApi_token(Utils.getString(this, Constant.KEY_TOKEN));
        List<CreateOrderRequest.Orderdetails> orderdetails;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            orderdetails = mProductList.stream().filter(data -> data.isAddToCart() == true).map(data -> new CreateOrderRequest.Orderdetails(data.getId(), data.getQuantity())).collect(Collectors.toList());
        } else {
            orderdetails = Stream.of(mProductList).filter(data -> data.isAddToCart() == true).map(data -> new CreateOrderRequest.Orderdetails(data.getId(), data.getQuantity())).collect(com.annimon.stream.Collectors.toList());
        }
        mCreateOrderRequest.setOrderdetails(orderdetails);
        return mCreateOrderRequest;
    }

    public SendOrderEnquiryReq prepareEnquiryRequest() {
        SendOrderEnquiryReq mCreateOrderRequest = new SendOrderEnquiryReq();
        mCreateOrderRequest.setApi_token(Utils.getString(this, Constant.KEY_TOKEN));
        List<SendOrderEnquiryReq.Products> orderdetails;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            orderdetails = mProductList.stream().filter(data -> data.isAddToCart() == true).map(data -> new SendOrderEnquiryReq.Products(data.getName(), data.getQuantity())).collect(Collectors.toList());
        } else {
            orderdetails = Stream.of(mProductList).filter(data -> data.isAddToCart() == true).map(data -> new SendOrderEnquiryReq.Products(data.getName(), data.getQuantity())).collect(com.annimon.stream.Collectors.toList());
        }
        mCreateOrderRequest.setProducts(orderdetails);
        return mCreateOrderRequest;
    }

    @Override
    public void onBackPressed() {
        if (mFlipper.getDisplayedChild() == 1) {
            if (!mProductList.isEmpty()) {
                mProductList.clear();
                mProductsAdapter.notifyDataSetChanged();
            }
            mOrderNowButt.setVisibility(View.GONE);
            mToolbarTitle.setText("Order Now");
            mFlipper.setDisplayedChild(0);
        } else if (mFlipper.getDisplayedChild() == 2) {
            mOrderNowButt.setVisibility(View.VISIBLE);
            mToolbarTitle.setText(selectedProduct);
            mFlipper.setDisplayedChild(1);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void callInstamojoSDK(String orderId) {
        MyAccountResponse.Hcf mHcf = new Gson().fromJson(Utils.getString(this, Constant.KEY_HCF_DATA), MyAccountResponse.Hcf.class);
        callInstamojoPay(mHcf.getEmail(), mHcf.getMobile(), "" + getTotalAmount(), "product buy", mHcf.getName(), orderId);
    }

    /**
     * INSTAMOJO METHODS
     **/
    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername, String orderId) {
        final Activity activity = this;
        instamojoPay = new InstamojoPay();
        filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        if (TextUtils.isEmpty(phone)){
            phone = Constant.DEFAULT_PHONE;
        }
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener(orderId);
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener(String orderId) {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
//                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
//                        .show();
                String[] responseData = response.split(":");
                for (int i = 0; i < responseData.length; i++) {
                    if (responseData[i].contains("paymentId")) {
                        String[] paymentDetails = responseData[i].split("=");
                        UpdateOrderRequest mUpdateOrderRequest = new UpdateOrderRequest();
                        mUpdateOrderRequest.setApi_token(Utils.getString(BrowseProductsActivity.this, Constant.KEY_TOKEN));
                        mUpdateOrderRequest.setOrderid(Integer.parseInt(orderId));
                        mUpdateOrderRequest.setPayment_ref_number(paymentDetails[1]);
                        mBrowseProductsActPresenter.updateOrder(mUpdateOrderRequest);
                    }
                }
                Log.e("PAYMENT RESPONSE", "" + response);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    @Override
    public void inflateStatesData(LocationDTO arg0) {
        mHcfData = new Gson().fromJson(Utils.getString(this, Constant.KEY_HCF_DATA), MyAccountResponse.Hcf.class);
        mLocationsModel = arg0;
        List<LocationDTO.LocationData> mLocationData = mLocationsModel.getLocationData();
        Collections.sort(mLocationData);

        for (LocationDTO.LocationData location : mLocationData) {
            if (location.getDistrictName().trim().equals(mHcfData.getCity().trim())) {
                postalCharge = location.getPostalCharges();
                Log.e("SEARCHED CITY MATCHED", location.getDistrictName());
            }
        }

        Log.e("SEARCHED POSTAL CHAR", "" + postalCharge);
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (instamojoPay!=null) {
//            unregisterReceiver(instamojoPay);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        if (instamojoPay!=null) {
//            unregisterReceiver(instamojoPay);
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (instamojoPay!=null){
//            registerReceiver(instamojoPay, filter);
//        }
//    }
}
