package nansat.com.safebio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.JsonObject;

import nansat.com.safebio.R;
import nansat.com.safebio.models.LoginResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppButton;
import retrofit2.Call;

public class CallSupportActivity extends BaseActivity {

    AppButton mCallButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_support);
        mCallButton = findViewById(R.id.button);

        mCallButton.setOnClickListener(v -> {
            callUs();
        });

        loginUserAndGetToken(Utils.getString(this, Constant.KEY_U), Utils.getString(this, Constant.KEY_P));
    }

    public void callUs() {
//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//        callIntent.setData(Uri.parse(String.format(Locale.getDefault(), "tel:+919035761000")));
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
//                        Constant.REQUEST_CODE_ASK_PERMISSIONS);
//            }
//            return;
//        }
//        startActivity(callIntent);
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:info@safebio.in"));
        startActivity(Intent.createChooser(intent, "Send Email"));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callUs();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void loginUserAndGetToken(String username, String password) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        JsonObject mJsonObject = new JsonObject();
        mJsonObject.addProperty("username", username.trim());
        mJsonObject.addProperty("password", password.trim());
        Call<LoginResponse> mLoginActPresenterCall = SafeBioClient.getInstance().getApiInterface().getToken(Constant.getTokenApi(), mJsonObject);
        mLoginActPresenterCall.enqueue(new RetrofitCallback<LoginResponse>(this, mProgressDialog, true) {
            @Override
            public void onSuccess(LoginResponse arg0) {
                if (arg0.getData() != null) {
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_U, username);
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_P, password);
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_TOKEN, arg0.getData().getApiToken());
//                    Utils.storeBoolean(CallSupportActivity.this, Constant.SUBSCRIPTION_TAKEN, arg0.getData().isHasSubscribed());//CHECK WITH DEEPAK
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_USER_ID, "" + arg0.getData().getUserId());
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_USER_ROLE, "" + arg0.getData().getRole());
                    Utils.storeInt(CallSupportActivity.this, Constant.KEY_USER_BELONGS_TO_HCF, arg0.getData().getRefId());
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_USER_EMAIL, arg0.getData().getEmail());
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_USER_PHONE, arg0.getData().getMobile());
                    Utils.storeBoolean(CallSupportActivity.this, Constant.KEY_ACTIVATION_STATUS, arg0.getData().getConfirmed());
                    Utils.storeString(CallSupportActivity.this, Constant.KEY_USER_NAME, !TextUtils.isEmpty(arg0.getData().getName()) ? arg0.getData().getName() : "");
                    if (arg0.getData().getConfirmed()) {
                        startNextActvity();
                    }
                }
            }
        });
    }

    private void startNextActvity() {
        if (Utils.getBoolean(this, Constant.KEY_ACTIVATION_STATUS)) {
            Intent in = new Intent(this, HomeActivity.class);
            navigateToNextActivity(in, true);
        }
    }
}
