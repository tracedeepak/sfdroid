package nansat.com.safebio.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import nansat.com.safebio.DeviceListAdapter;
import nansat.com.safebio.R;
import nansat.com.safebio.models.Device;
import nansat.com.safebio.utils.BaseActivity;

/**
 * Created by Phantasmist on 08/02/18.
 */

public class CustomBluetoothActivity extends BaseActivity {

    private static final String TAG = "CONNECTION THREAD";
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 23;
    private static final int REQUEST_ENABLE_BT = 222;
    BluetoothAdapter mBluetoothAdapter;
    ListView mLvNewDevices;
    List<Device> deviceList = new ArrayList<>();
    DeviceListAdapter mDeviceListAdapter;
    TextView weight;
    final int handlerState = 0;
    private StringBuilder recDataString = new StringBuilder();

    ConnectThread mConnectThread;
    ThreadConnected mThreadConnected;
    Handler bluetoothHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_scanning);
        mLvNewDevices = findViewById(R.id.lvNewDevices);
        weight=findViewById(R.id.weight);
        mDeviceListAdapter = new DeviceListAdapter(deviceList);
        mLvNewDevices.setAdapter(mDeviceListAdapter);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        fuckMarshMallow();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            getPrePairedDeviceList();
            registerDeviceForAnyNEwDeviceFound();
            //Broadcasts when bond state changes (ie:pairing)
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver4, filter);

        }


        mLvNewDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (deviceList.get(i).getUuid()!=null){
                    mConnectThread=new ConnectThread(deviceList.get(i).getmBluetoothDevice(), deviceList.get(i).getUuid());
                    mConnectThread.start();
                }else{
                    deviceList.get(i).getmBluetoothDevice().createBond();
                }
//
            }
        });

        bluetoothHandler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    recDataString.append(readMessage);
                    int endOfLineIndex = recDataString.indexOf("g");
                    endOfLineIndex+=1;
                    if (endOfLineIndex > 0) {
                        String dataInPrint = recDataString.substring(0, endOfLineIndex);
                        weight.setText(dataInPrint);
                        Log.e("MESSAGE",dataInPrint);
                        recDataString.delete(0, recDataString.length()); 					//clear all string data
                        // strIncom =" ";
                        dataInPrint = " ";

                    }
                }
            }
        };
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ENABLE_BT) {
                Log.e("BLUETOOTH ", "ENABLED");
                getPrePairedDeviceList();
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ENABLE_BT) {
                Log.e("BLUETOOTH ", "DISABLED");
            }
        }
    }


    public void getPrePairedDeviceList() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                ParcelUuid[] uuid = device.getUuids();
                UUID deviceUUID = device.getUuids()[0].getUuid();

                Device mDevice = new Device();
                mDevice.setDeviceName(deviceName);
                mDevice.setDeviceAddress(deviceHardwareAddress);
                mDevice.setUuid(deviceUUID.toString());
                mDevice.setmBluetoothDevice(device);
                Log.e("BOND STATE", "" + device.getBondState());
                Log.e("DEV NAME", "" + device.getName());
                deviceList.add(mDevice);
                mDeviceListAdapter.notifyDataSetChanged();
                Log.e("UUID", "" + deviceUUID);
            }
        }
    }


    public void registerDeviceForAnyNEwDeviceFound() {
        // Register for broadcasts when a device is discovered.
        mBluetoothAdapter.startDiscovery();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);
        Log.e("STATUS",""+
                mBluetoothAdapter.isDiscovering());
    }

    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED){
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    Log.e(TAG, "BroadcastReceiver: BOND_BONDED."+mDevice.getName());
                    mBluetoothAdapter.cancelDiscovery();
                    deviceList.clear();
                    mDeviceListAdapter.notifyDataSetChanged();
                    getPrePairedDeviceList();
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                    Log.e(TAG, "BroadcastReceiver: BOND_BONDING."+mDevice.getName());
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                    Log.e(TAG, "BroadcastReceiver: BOND_NONE."+mDevice.getName());
                }
            }
        }
    };



    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.e("BLUETOOTH RECEIVER STARTED","TRUE");
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                try {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    Device mDevice = new Device();
                    mDevice.setDeviceName(deviceName);
                    mDevice.setDeviceAddress(deviceHardwareAddress);

                    mDevice.setmBluetoothDevice(device);
                    Log.e("Discovered DEVICE", deviceName);
                    Log.e("BOND STATE", "" + device.getBondState());

                    ParcelUuid[] uuid = device.getUuids();
                    if (device.getUuids()!=null) {
                        UUID deviceUUID = device.getUuids()[0].getUuid();
                        mDevice.setUuid(deviceUUID.toString());
                        Log.e("UUID", "" + deviceUUID);
                    }
                    deviceList.add(mDevice);
                    mDeviceListAdapter.notifyDataSetChanged();

//                    if (!deviceName.equals(null)) {

//                    }
                } catch (Exception e) {
                    Log.e("EXCEPTION", e.getLocalizedMessage());
                }
//                Log.e("DISCOVERED DEVICE",deviceName);

            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Don't forget to unregister the ACTION_FOUND receiver.
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
        if (mBroadcastReceiver4 != null) {
            unregisterReceiver(mBroadcastReceiver4);
        }
    }


    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final String TAG = "CONNECT THREAD";

        public ConnectThread(BluetoothDevice device, String uuid) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(uuid));
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
                Log.e("DEVICE CONNECTED", "" + mmSocket.isConnected());
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    private void manageMyConnectedSocket(BluetoothSocket mmSocket) {
        mThreadConnected=new ThreadConnected(mmSocket);
        mThreadConnected.start();
    }

    private class ThreadConnected extends Thread {
        private final BluetoothSocket connectedBluetoothSocket;
        private final InputStream connectedInputStream;
        private final OutputStream connectedOutputStream;

        public ThreadConnected(BluetoothSocket socket) {
            connectedBluetoothSocket = socket;
            InputStream in = null;
            OutputStream out = null;
            Log.e("THEARD CONNECT","SOCKET INITIALISED"+connectedBluetoothSocket.isConnected());
            try {
                in = socket.getInputStream();
                out = socket.getOutputStream();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            connectedInputStream = in;
            connectedOutputStream = out;
        }

        @Override
        public void run() {
            Log.e("THEARD CONNECT","READING DATA");
            byte[] buffer = new byte[1024];
            int bytes;

            while (true) {
                try {
                    bytes = connectedInputStream.read(buffer);
                    String strReceived = new String(buffer, 0, bytes);
//                    String strReceived = new String(buffer,"UTF-8");
                    final String msgReceived = strReceived;
//                    setWeightText(msgReceived);
                    bluetoothHandler.obtainMessage(handlerState,bytes,-1,msgReceived).sendToTarget();

//                    runOnUiThread(new Runnable(){
//
//                        @Override
//                        public void run() {
////                            textStatus.setText(msgReceived);
//                        }});

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                    final String msgConnectionLost = "Connection lost:\n"
                            + e.getMessage();
                    runOnUiThread(new Runnable(){

                        @Override
                        public void run() {
                            Log.e("CONNECTION LOST","TRUE");
//                            textStatus.setText(msgConnectionLost);
                        }});
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                connectedOutputStream.write(buffer);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                connectedBluetoothSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    private void setWeightText(String msgReceived) {

        String text=msgReceived;
        Log.e("MESSAGE RECEiVED",text);
        Log.e("MESSAGE LENGTH",""+text.length());
        weight.setText(text);

//        runOnUiThread(()->weight.setText("UDIT"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);


                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

                        ) {
                    // All Permissions Granted

                    // Permission Denied
                    Toast.makeText(this, "All Permission GRANTED !! Thank You :)", Toast.LENGTH_SHORT)
                            .show();


                } else {
                    // Permission Denied
                    Toast.makeText(this, "One or More Permissions are DENIED Exiting App :(", Toast.LENGTH_SHORT)
                            .show();

                    finish();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void fuckMarshMallow() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Show Location");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {

                // Need Rationale
                String message = "App need access to " + permissionsNeeded.get(0);

                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        Toast.makeText(this, "No new Permission Required- Launching App .You are Awesome!!", Toast.LENGTH_SHORT)
                .show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {

        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

}



