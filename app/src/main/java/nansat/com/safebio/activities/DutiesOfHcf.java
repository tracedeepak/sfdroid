package nansat.com.safebio.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import nansat.com.safebio.R;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 11/05/18.
 */

public class DutiesOfHcf extends BaseActivity {
    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;
    private AppTextView mOpenLinkText;
    private AppTextView mDutiesText;

    private void assignViews() {
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mAccountToolbarTitle.setText("Duties of HCF");

        mOpenLinkText = (AppTextView) findViewById(R.id.openLinkText);
        mDutiesText = (AppTextView) findViewById(R.id.dutiesText);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duties);
        assignViews();
        mDutiesText.setText(Utils.loadJSONFromAsset(this, "duties.txt"));
        mOpenLinkText.setOnClickListener((v) -> {
            openWebPage("http://mpcb.gov.in/biomedical/pdf/BMW_Rules_2016.pdf");
        });
    }

    public void openWebPage(String url) {
        try {
            Uri webpage = Uri.parse(url);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request. Please install a web browser or check your URL.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        backAnimation();

    }
}
