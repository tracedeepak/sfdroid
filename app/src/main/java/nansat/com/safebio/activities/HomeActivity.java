package nansat.com.safebio.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import nansat.com.safebio.BuildConfig;
import nansat.com.safebio.R;
import nansat.com.safebio.contracts.HomeActContract;
import nansat.com.safebio.database.LocalStorage;
import nansat.com.safebio.database.models.Sku;
import nansat.com.safebio.database.viewmodels.SkuViewModel;
import nansat.com.safebio.models.OfflineRequestsDTO;
import nansat.com.safebio.models.SKUResponse;
import nansat.com.safebio.models.UpdateNotification;
import nansat.com.safebio.presenter.HomeActivityPresenter;
import nansat.com.safebio.services.UpdatePendingRequestsService;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 08/02/18.
 */

public class HomeActivity extends BaseActivity implements HomeActContract {

    HomeActivityPresenter mHomeActivityPresenter;
    SkuViewModel skuViewModel;
    CardView mPickupCard;
    CardView mHCFCard;
    CardView mDepositCard;
    CardView mOrderNowCard;
    CardView mMyAccountCard;
    CardView mMyOrdersCard;
    CardView mHCFReports;
    CardView mCPWTFReports;
    CardView mReportIssueCpwtf;
    CardView mReportIssueHCF;
    CardView mDutiesOfHCF;
    AppButton mLogoutButt;
    AppTextView mAppVersionTv;

    LinearLayout mHcfLayout;
    LinearLayout mCpwtfLayout;

    String userRole;
    CardView mMyAccountCardCPWTF;
    private CardView mOrderNowCardCBWTF;
    private LiveData<List<Sku>> mSkuList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_home);
        mPickupCard = findViewById(R.id.pickupCard);
        mDepositCard = findViewById(R.id.depositCard);
        mOrderNowCard = findViewById(R.id.orderNowCard);
        mOrderNowCardCBWTF = findViewById(R.id.orderNowCardCBWTF);
        mHCFReports = findViewById(R.id.hcfReports);
        mCPWTFReports = findViewById(R.id.cpwtfReports);
        mHCFCard = findViewById(R.id.hcfCard);
        mLogoutButt = findViewById(R.id.logoutButt);
        mMyAccountCard = findViewById(R.id.myAccountCard);
        mMyAccountCardCPWTF = findViewById(R.id.myAccountCardCPWTF);
        mMyOrdersCard = findViewById(R.id.myOrdersCard);
        mHcfLayout = findViewById(R.id.hcfLayout);
        mCpwtfLayout = findViewById(R.id.cpwtfLayout);
        mReportIssueCpwtf = findViewById(R.id.reportIssueCpwtf);
        mReportIssueHCF = findViewById(R.id.reportIssueHCF);
        mDutiesOfHCF = findViewById(R.id.dutiesOfHCF);
        mAppVersionTv = findViewById(R.id.appVersionTv);

        userRole = Utils.getString(this, Constant.KEY_USER_ROLE);
        Log.e("USER ROLE", userRole);
        mHomeActivityPresenter = new HomeActivityPresenter(this);
        skuViewModel = ViewModelProviders.of(this).get(SkuViewModel.class);
        mAppVersionTv.setText(BuildConfig.VERSION_NAME);

        try {
            powerFireBase();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        if (userRole.toUpperCase().equals(Constant.ROLE_HCF.toUpperCase())) {
            mHcfLayout.setVisibility(View.VISIBLE);
            mCpwtfLayout.setVisibility(View.GONE);
            if (TextUtils.isEmpty(Utils.getString(this, Constant.KEY_HCF_DATA))) {
                mHomeActivityPresenter.fetchDetails();
            }
        } else {

            mCpwtfLayout.setVisibility(View.VISIBLE);
            mHcfLayout.setVisibility(View.GONE);
        }


        Log.e("HCF_ID", "" + Utils.getInt(this, Constant.KEY_USER_BELONGS_TO_HCF));
        //deposit false
        mHCFCard.setOnClickListener((v) -> {
//            Intent in = new Intent(this, SelectHCFActivity.class);
//            in.putExtra("isDeposit", false);
//            navigateToNextActivity(in, false);
            Intent in = new Intent(HomeActivity.this, ScanOptionsActivity.class);
            in.putExtra("hospitalName", !TextUtils.isEmpty(Utils.getString(this, Constant.KEY_USER_NAME)) ? Utils.getString(this, Constant.KEY_USER_NAME) : "");
            in.putExtra("hospitalId", Utils.getInt(this, Constant.KEY_USER_BELONGS_TO_HCF));
            in.putExtra("isHCF", true);
            in.putExtra("isDeposit", false);
            navigateToNextActivity(in, false);
        });

        //deposit true
        mPickupCard.setOnClickListener((v) -> {
            Intent in = new Intent(this, ScanOptionsActivity.class);
            in.putExtra("isDeposit", true);
            in.putExtra("optionType", Constant.PICKUP);
            in.putExtra("isHCF", false);
            in.putExtra("hospitalId", 0);
            navigateToNextActivity(in, false);
        });

        mLogoutButt.setOnClickListener((v) -> {
            skuViewModel.deleteAllSkus(mDeletedAllSkusObserver);
        });
        //deposit false
        //this guy will directly go to scan page
        mDepositCard.setOnClickListener((v) -> {
            Intent in = new Intent(this, ScanOptionsActivity.class);
            in.putExtra("isDeposit", false);
            in.putExtra("optionType", Constant.DEPOSIT);
            in.putExtra("isHCF", false);
//            in.putExtra("enableWeightScan", true);
            in.putExtra("hospitalId", 0);
            navigateToNextActivity(in, false);
        });

        mOrderNowCard.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, BrowseProductsActivity.class);
            navigateToNextActivity(in, false);
        });

        mOrderNowCardCBWTF.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, BrowseProductsActivity.class);
            navigateToNextActivity(in, false);
        });

        mMyAccountCard.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, MyAccountActivity.class);
            navigateToNextActivity(in, false);
        });
        mMyAccountCardCPWTF.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, MyAccountActivity.class);
            navigateToNextActivity(in, false);
        });

        mMyOrdersCard.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, MyOrdersActivity.class);
            navigateToNextActivity(in, false);
        });

        mCPWTFReports.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, ReportsActivity.class);
            navigateToNextActivity(in, false);
        });
        mHCFReports.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, ReportsActivity.class);
            navigateToNextActivity(in, false);
        });

        mReportIssueHCF.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, ReportIssueActivity.class);
            navigateToNextActivity(in, false);
        });
        mReportIssueCpwtf.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, ReportIssueActivity.class);
            navigateToNextActivity(in, false);
        });
        mDutiesOfHCF.setOnClickListener((v) -> {
            Intent in = new Intent(HomeActivity.this, DutiesOfHcf.class);
            navigateToNextActivity(in, false);
        });


        mSkuList = skuViewModel.getAllSku();
        mSkuList.observe(this, (List<Sku> skuList) ->{
            mSkuList.removeObservers(this);
            if (mSkuList.hasObservers())return;
            if (skuList.isEmpty()) {
                Log.e("FETCHING SKU:", "SKU LIST IS EMPTY");
                mHomeActivityPresenter.fetchSKUs();
            } else {
                Log.e("FETCHING SKU:", "CHECKING FOR PENDING QUEUE" + new Gson().toJson(skuList));
                checkForPendingQueue();
            }
        });
    }


    private void checkForPendingQueue() {
        if (Utils.isConnectedToInternet(this)) {
            if (Utils.getString(this, Constant.PENDING_REQUESTS) == null || TextUtils.isEmpty(Utils.getString(this, Constant.PENDING_REQUESTS))) {
                LocalStorage.makePendingRequestsQueueEmpty(this);
            }
            if (Utils.getString(this, Constant.PENDING_REQUESTS) != null) {
                Log.e("PENDING QUEUE : ", new Gson().toJson(LocalStorage.getExistingPendingRequests(this)));
                OfflineRequestsDTO mOfflineRequestsDTO = LocalStorage.getExistingPendingRequests(this);

                if (!mOfflineRequestsDTO.getmPendingRequests().isEmpty()) {
                    Intent in = new Intent(this, UpdatePendingRequestsService.class);
                    startService(in);
                }


            }
        }
    }


    ProgressDialog mDeleteProgressDialog;
    CompletableObserver mDeletedAllSkusObserver = new CompletableObserver() {
        @Override
        public void onSubscribe(Disposable d) {
            mDeleteProgressDialog = Logger.showProgressDialog(HomeActivity.this);
        }

        @Override
        public void onComplete() {
            mDeleteProgressDialog.dismiss();
            logoutAndClearPreferences();
        }

        @Override
        public void onError(Throwable e) {

        }
    };

    android.arch.lifecycle.Observer<List<Sku>> mSkuListObserver = new android.arch.lifecycle.Observer<List<Sku>>() {
        @Override
        public void onChanged(@Nullable List<Sku> skus) {
            Log.e("SKU OBSERVER", "" + skuViewModel.getAllSku().hasActiveObservers());

        }

    };


    Observer<Long[]> mAddSkuObserver = new Observer<Long[]>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Long[] longs) {
            Log.e("Added to db", "" + longs);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {
            Log.e("Added to db", "" + new Gson().toJson(skuViewModel.getAllSku()));
        }
    };

    @Override
    public void saveSKUsToDb(List<SKUResponse.Data> data) {
        for (SKUResponse.Data mData : data) {
            skuViewModel.insertAllSkus(new Sku(mData.getSkuId(), mData.getSkuName(), mData.getActive()), mAddSkuObserver);
        }
        checkForPendingQueue();

    }

    @Override
    public Activity provideContext() {
        return this;
    }

    /**
     * AUTO UPDATE
     */


    DatabaseReference myRef;
    private Dialog updateDialog;

    public void powerFireBase() {
        myRef = FirebaseDatabase.getInstance().getReference("mandateUpdate");
        myRef.addValueEventListener(mValueEventListener);
    }

    ValueEventListener mValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            UpdateNotification updateData = dataSnapshot.getValue(UpdateNotification.class);
            Log.d("DataSnapshot", "onDataChange: " + dataSnapshot.toString());
            Log.d("Snapshot", "" + updateData.getUpdateVersionCode());
            Log.d("Snapshot", "" + updateData.getForceUpdateVersionCode());
            Log.d("BUILD", "" + BuildConfig.VERSION_CODE);
            String changes[] = updateData.getChangeLog().split(",");
            StringBuilder sb = new StringBuilder();
            sb.append("\n");
            for (int i = 0; i < changes.length; i++) {
                sb.append(changes[i]);
                sb.append("\n");
            }

            if (updateData.getUpdateVersionCode() >= BuildConfig.VERSION_CODE) {
                long lastCardShownTime = Utils.getLong(HomeActivity.this, Constant.UPDATE_CARD_TIME);
                if ((lastCardShownTime == 0) || (System.currentTimeMillis() > (lastCardShownTime + 4 * 60 * 60 * 1000))) {
                    updateDialog = new Dialog(HomeActivity.this);
                    updateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                    updateDialog.setContentView(R.layout.dialog_update);

                    AppButton btnNo = updateDialog.findViewById(R.id.btn_update_card_no);
                    AppButton btnYes = updateDialog.findViewById(R.id.btn_update_card_yes);

                    AppTextView tvMessage = updateDialog.findViewById(R.id.tv_update_card_message);
                    tvMessage.setText(updateData.getMessage() + "\n\nChange Log:" + sb.toString());

                    if (updateData.getForceUpdateVersionCode() >= BuildConfig.VERSION_CODE) {
                        btnNo.setVisibility(View.GONE);
                    } else {
                        btnNo.setVisibility(View.VISIBLE);
                    }
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (updateDialog != null) {
                                updateDialog.dismiss();
                            }
                            finish();
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=nansat.com.safebio"));
                            startActivity(intent);
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Utils.storeLong(HomeActivity.this, Constant.UPDATE_CARD_TIME, System.currentTimeMillis());
                            if (updateDialog != null) {
                                updateDialog.dismiss();
                                updateDialog = null;
                            }
                        }
                    });


                    updateDialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
                    updateDialog.setCancelable(false);
                    updateDialog.show();
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        if (myRef != null) {
            myRef.removeEventListener(mValueEventListener);
        }
        if (updateDialog != null) {
            updateDialog.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (updateDialog != null) {
            updateDialog.show();
        }
    }

    void logoutAndClearPreferences() {
        Utils.emptyPreferences(this);
        Intent in = new Intent(this, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        navigateToPreviousActivity(in, true);
    }
}
