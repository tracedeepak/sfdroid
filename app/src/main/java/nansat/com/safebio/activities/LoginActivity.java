package nansat.com.safebio.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import nansat.com.safebio.R;
import nansat.com.safebio.contracts.LoginActContract;
import nansat.com.safebio.databinding.ActivityLoginBinding;
import nansat.com.safebio.datamodels.LoginActViewModel;
import nansat.com.safebio.presenter.LoginActPresenter;
import nansat.com.safebio.utils.BaseActivity;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements LoginActContract {

    ActivityLoginBinding mLoginBinding;
    LoginActPresenter mPresenter;
    LoginActViewModel mInputData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mPresenter = new LoginActPresenter(this);
        mInputData = new LoginActViewModel();
        mLoginBinding.setInputData(mInputData);
        mLoginBinding.setPresenter(mPresenter);
    }

    @Override
    public void startNextActivity(boolean hasSubscribed, Boolean active) {
        if (hasSubscribed) {
            if (!active) {
                Intent in = new Intent(this, CallSupportActivity.class);
                navigateToNextActivity(in, true);
            } else {
                Intent in = new Intent(this, HomeActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                navigateToNextActivity(in, true);
            }
        } else {
            Intent in = new Intent(this, AvailablePlansActivity.class);
            navigateToNextActivity(in, true);
        }
    }

    @Override
    public void launchRegisterHCF() {
        Intent in = new Intent(this, RegisterHCFActivity.class);
        navigateToNextActivity(in, false);
    }

    @Override
    public Activity provideContext() {
        return this;
    }

    public void functionForTest() {
        Log.e("PRINT VALue", "TESTED");
    }
}