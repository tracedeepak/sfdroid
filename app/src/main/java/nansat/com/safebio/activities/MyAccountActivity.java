package nansat.com.safebio.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.gson.Gson;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.MyAccountCPWTFViewPagerAdapter;
import nansat.com.safebio.adapters.MyAccountViewPagerAdapter;
import nansat.com.safebio.models.CpwtfAccountResponse;
import nansat.com.safebio.models.MyAccountResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppTextView;
import nansat.com.safebio.widgets.SlidingTabLayout;
import retrofit2.Call;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyAccountActivity extends BaseActivity {


    private SlidingTabLayout mMyAccountTabs;
    private ViewPager mMyAccountViewPager;
    MyAccountViewPagerAdapter myAccountViewPagerAdapter;
    MyAccountCPWTFViewPagerAdapter myAccountCPWTFViewPagerAdapter;
    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;

    private void assignViews() {
        mMyAccountTabs = findViewById(R.id.myAccountTabs);
        mMyAccountViewPager = findViewById(R.id.myAccountViewPager);
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        mAccountToolbarTitle.setText("My Account");
        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtity_myaccount);
        assignViews();
        if (Utils.getString(this, Constant.KEY_USER_ROLE).toUpperCase().equals(Constant.ROLE_HCF.toUpperCase())) {
            fetchDetails();
        } else {
            fetchCPWTFDetails();
        }
    }

    public void fetchDetails() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        Call<MyAccountResponse> myAccountResponseCall = SafeBioClient.getInstance().getApiInterface().getMyAccountDetails(Constant.getMyAccountDetails(), Utils.getString(this, Constant.KEY_TOKEN));
        myAccountResponseCall.enqueue(new RetrofitCallback<MyAccountResponse>(this, mProgressDialog) {
            @Override
            public void onSuccess(MyAccountResponse arg0) {
                if (!arg0.getData().isEmpty()) {
                    inflateData(new Gson().toJson(arg0));
                }
            }
        });
    }

    public void inflateData(String data) {
        myAccountViewPagerAdapter = new MyAccountViewPagerAdapter(getSupportFragmentManager(), data);
        mMyAccountViewPager.setAdapter(myAccountViewPagerAdapter);
        mMyAccountTabs.setDistributeEvenly(true);
        mMyAccountTabs.setSelectedIndicatorColors(Color.parseColor("#F69831"));
        mMyAccountTabs.setViewPager(mMyAccountViewPager);
        mMyAccountViewPager.setOffscreenPageLimit(myAccountViewPagerAdapter.getCount());
    }

    public void fetchCPWTFDetails() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        Call<CpwtfAccountResponse> myAccountResponseCall = SafeBioClient.getInstance().getApiInterface().getCpwtfAccountDetails(Constant.getMyAccountDetails(), Utils.getString(this, Constant.KEY_TOKEN));
        myAccountResponseCall.enqueue(new RetrofitCallback<CpwtfAccountResponse>(this, mProgressDialog) {
            @Override
            public void onSuccess(CpwtfAccountResponse arg0) {

                inflateCPWTFData(new Gson().toJson(arg0));

            }
        });
    }

    public void inflateCPWTFData(String data) {
        myAccountCPWTFViewPagerAdapter = new MyAccountCPWTFViewPagerAdapter(getSupportFragmentManager(), data);
        mMyAccountViewPager.setAdapter(myAccountCPWTFViewPagerAdapter);
        mMyAccountTabs.setDistributeEvenly(true);
        mMyAccountTabs.setSelectedIndicatorColors(Color.parseColor("#F69831"));
        mMyAccountTabs.setViewPager(mMyAccountViewPager);
        mMyAccountViewPager.setOffscreenPageLimit(myAccountCPWTFViewPagerAdapter.getCount());
    }


}
