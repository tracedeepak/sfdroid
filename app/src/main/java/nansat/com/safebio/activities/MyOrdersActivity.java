package nansat.com.safebio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.MyOrdersAdapter;
import nansat.com.safebio.models.MyOrdersResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.RecyclerItemClickListener;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppTextView;
import retrofit2.Call;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyOrdersActivity extends BaseActivity {


    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;

    private RecyclerView mMyOrdersRecView;
    MyOrdersAdapter myOrdersAdapter;
    List<MyOrdersResponse.Data> mOrderList;

    private void assignViews() {
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mMyOrdersRecView = (RecyclerView) findViewById(R.id.myOrdersRecView);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        mAccountToolbarTitle.setText("My Orders");
        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mOrderList = new ArrayList<>();
        mMyOrdersRecView.setLayoutManager(new LinearLayoutManager(this));
        myOrdersAdapter = new MyOrdersAdapter(mOrderList);
        mMyOrdersRecView.setHasFixedSize(true);
        myOrdersAdapter.setHasStableIds(true);
        mMyOrdersRecView.setAdapter(myOrdersAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        assignViews();
        fetchMyOrders();
        mMyOrdersRecView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MyOrdersResponse.Data mData = myOrdersAdapter.getItemAtPosition(position);
                Intent in = new Intent(MyOrdersActivity.this, OrderDetailsActivity.class);
                in.putExtra("orderData", new Gson().toJson(mData));
                navigateToNextActivity(in, false);
            }
        }));
    }


    public void fetchMyOrders() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        Call<MyOrdersResponse> myOrdersResponseCall = SafeBioClient.getInstance().getApiInterface().getMyOrdersApi(Constant.getMyOrders(), Utils.getString(this, Constant.KEY_TOKEN));
        myOrdersResponseCall.enqueue(new RetrofitCallback<MyOrdersResponse>(this, mProgressDialog) {
            @Override
            public void onSuccess(MyOrdersResponse arg0) {
                if (!arg0.getData().isEmpty()) {
                    addDataToList(arg0.getData());
                }
            }
        });
    }

    private void addDataToList(List<MyOrdersResponse.Data> data) {
        if (!mOrderList.isEmpty()) {
            mOrderList.clear();
            myOrdersAdapter.notifyDataSetChanged();
        }
        mOrderList.addAll(data);
        myOrdersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backAnimation();
    }
}
