package nansat.com.safebio.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.MyOrderDetailsRecAdapter;
import nansat.com.safebio.databinding.ActivityOrderDetailsBinding;
import nansat.com.safebio.models.MyOrdersResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 26/03/18.
 */

public class OrderDetailsActivity extends BaseActivity {


    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;
    private RecyclerView mOrderContentsRecView;

    ActivityOrderDetailsBinding mActivityOrderDetailsBinding;
    String orderData;
    MyOrderDetailsRecAdapter myOrderDetailsRecAdapter;
    List<MyOrdersResponse.Order_details> mOrderDetailsList = new ArrayList<>();

    private void assignViews() {
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        mAccountToolbarTitle.setText("Order Details");
        mOrderContentsRecView = findViewById(R.id.orderContentsRecView);

        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityOrderDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_details);
        assignViews();
        orderData = getIntent().getStringExtra("orderData");
        MyOrdersResponse.Data mData = new Gson().fromJson(orderData, MyOrdersResponse.Data.class);

        switch (mData.getStatus()) {
            case Constant.ORDER_STATUS_UNCONFIRMED:
                mActivityOrderDetailsBinding.setOrderStatus("Un-Confirmed");
                break;
            case Constant.ORDER_STATUS_CONFIRMED:
                mActivityOrderDetailsBinding.setOrderStatus("Confirmed");
                break;
            case Constant.ORDER_STATUS_FULLFILLED:
                mActivityOrderDetailsBinding.setOrderStatus("Fulfilled");
                break;

        }
        mActivityOrderDetailsBinding.setData(mData);
        mOrderDetailsList.addAll(mData.getOrder_details());
        myOrderDetailsRecAdapter = new MyOrderDetailsRecAdapter(mOrderDetailsList);
        mOrderContentsRecView.setLayoutManager(new LinearLayoutManager(this));
        mOrderContentsRecView.setHasFixedSize(true);
        myOrderDetailsRecAdapter.setHasStableIds(true);
        mOrderContentsRecView.setAdapter(myOrderDetailsRecAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backAnimation();
    }
}
