package nansat.com.safebio.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.annimon.stream.Stream;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.HcfTypeAdapter;
import nansat.com.safebio.adapters.StateCityAdapter;
import nansat.com.safebio.contracts.RegisterActContract;
import nansat.com.safebio.databinding.ActivityRegisterBinding;
import nansat.com.safebio.datamodels.RegisterViewModel;
import nansat.com.safebio.models.HcfTypeModel;
import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.RegisterHCFRequest;
import nansat.com.safebio.models.StatesModel;
import nansat.com.safebio.presenter.RegisterActPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class RegisterHCFActivity extends BaseActivity implements RegisterActContract {

    ActivityRegisterBinding mActivityRegisterBinding;
    RegisterActPresenter mRegisterActPresenter;
    RegisterViewModel registerViewModel;
    Toolbar mRegisterToolBar;
    AppTextView mToolbarTitle;
    StateCityAdapter mStateAdapter;
    StateCityAdapter mCityAdapter;
    HcfTypeAdapter mHcfTypeAdapter;
    Spinner mStateSpinner;
    Spinner mCitySpinner;
    Spinner mHcfTypeSpinner;

    List<String> citiesList = new ArrayList<>();
    List<Integer> districtIds = new ArrayList<>();
    private LocationDTO mLocationsModel;
    private List<String> stateFilter;
    private List<LocationDTO.LocationData> allStateLocationList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        mRegisterToolBar = findViewById(R.id.registerToolBar);
        mStateSpinner = findViewById(R.id.stateSpinner);
        mCitySpinner = findViewById(R.id.citySpinner);
        mHcfTypeSpinner = findViewById(R.id.hcfTypeSpinner);


        mToolbarTitle = mRegisterToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("");
        mRegisterActPresenter = new RegisterActPresenter(this);
        registerViewModel = new RegisterViewModel();
        mActivityRegisterBinding.setPresenter(mRegisterActPresenter);
        mActivityRegisterBinding.setInputData(registerViewModel);
        mRegisterActPresenter.getStates();
        setSupportActionBar(mRegisterToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
//        StateCitesModel citesModel = new Gson().fromJson(Utils.loadJSONFromAsset(this, "state-ciites.json"), StateCitesModel.class);
        HcfTypeModel hcfTypeModel = new Gson().fromJson(Utils.loadJSONFromAsset(this, "hcf-type.json"), HcfTypeModel.class);
        StatesModel statesModel = new Gson().fromJson(Utils.loadJSONFromAsset(this, "states.json"), StatesModel.class);
//        LocationDTO mLocationsModel = new Gson().fromJson(Utils.loadJSONFromAsset(this, "location.json"), LocationDTO.class);

//        List<StatesModel.AllStates> allStatesList = statesModel.getAllStates();
//        Collections.sort(allStatesList);

//        List<LocationDTO.LocationData> allStateLocationList = mLocationsModel.getLocationData();
//        Collections.sort(allStateLocationList);

//        Set<String> uniqueStateNames = new HashSet<>();
//        for (StateCitesModel.Data state : citesModel.getData()) {
//            uniqueStateNames.add(state.getState());
//        }
//        Set<String> uniqueStateNames = new HashSet<>();
//        for (LocationDTO.LocationData state : allStateLocationList) {
//            uniqueStateNames.add(state.getStateName());
//        }

//        List<String> stateFilter = new ArrayList<String>(uniqueStateNames);
//        Collections.sort(stateFilter);


        mCityAdapter = new StateCityAdapter(citiesList);
        mCitySpinner.setAdapter(mCityAdapter);

        mHcfTypeAdapter = new HcfTypeAdapter(hcfTypeModel.getAllHcffTypes());
        mHcfTypeSpinner.setAdapter(mHcfTypeAdapter);


        mStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                StatesModel.AllStates mSearchedState = new StatesModel.AllStates();
//                mSearchedState.setState_name(stateFilter.get(i));
//                int indexOfStates = Collections.binarySearch(allStatesList, mSearchedState);
//                mActivityRegisterBinding.getInputData().setState("" + allStatesList.get(indexOfStates).getId() + ":" + allStatesList.get(indexOfStates).getState_code());
//                Log.e("STATE VALUE :", mActivityRegisterBinding.getInputData().getState());
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    refreshCityList(citesModel.getData().stream().filter(data -> data.getState().equals(stateFilter.get(i))).collect(Collectors.toList()));
//                } else {
//                    refreshCityList(Stream.of(citesModel.getData()).filter(data -> data.getState().equals(stateFilter.get(i))).collect(com.annimon.stream.Collectors.toList()));
//                }
                LocationDTO.LocationData mSearchedState = new LocationDTO.LocationData();
                mSearchedState.setStateName(stateFilter.get(i));
                int indexOfStates = Collections.binarySearch(allStateLocationList, mSearchedState);
                mActivityRegisterBinding.getInputData().setState("" + allStateLocationList.get(indexOfStates).getStateId() + ":" + allStateLocationList.get(indexOfStates).getStateCode());
                Log.e("STATE VALUE :", mActivityRegisterBinding.getInputData().getState());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    refreshCityList(allStateLocationList.stream().filter(data -> data.getStateName().equals(stateFilter.get(i))).collect(Collectors.toList()));
                } else {
                    refreshCityList(Stream.of(allStateLocationList).filter(data -> data.getStateName().equals(stateFilter.get(i))).collect(com.annimon.stream.Collectors.toList()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mCitySpinner.setOnItemSelectedListener(citiesListener);

        mHcfTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mActivityRegisterBinding.getInputData().setHcf_type("" + mHcfTypeAdapter.getItem(i).getId() + ":" + mHcfTypeAdapter.getItem(i).getHcf_type());
                Log.e("HCF TYPE", mActivityRegisterBinding.getInputData().getHcf_type());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    AdapterView.OnItemSelectedListener citiesListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            mActivityRegisterBinding.getInputData().setCity(mCityAdapter.getItem(i));
            mActivityRegisterBinding.getInputData().setDistrictId(districtIds.get(i));
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    void refreshCityList(List<LocationDTO.LocationData> mCitiesInState) {
//        mCitySpinner.setOnItemSelectedListener(null);
        if (!citiesList.isEmpty()) {
            citiesList.clear();
            mCityAdapter.notifyDataSetChanged();
        }
        if (!districtIds.isEmpty()) {
            districtIds.clear();
        }
        for (LocationDTO.LocationData mData : mCitiesInState) {
            citiesList.add(mData.getDistrictName());
            districtIds.add(mData.getDistrictId());
        }
        mCityAdapter.notifyDataSetChanged();
//        mCitySpinner.setOnItemSelectedListener(citiesListener);

        mCitySpinner.setSelection(0);
        mCitySpinner.setSelected(true);

        mActivityRegisterBinding.getInputData().setCity(citiesList.get(mCitySpinner.getSelectedItemPosition()));
        mActivityRegisterBinding.getInputData().setDistrictId(districtIds.get(mCitySpinner.getSelectedItemPosition()));

        Log.e("SELECTED Val", mCitySpinner.getSelectedItem().toString());
        Log.e("SELECTED Val1", mActivityRegisterBinding.getInputData().getCity());
        Log.e("SELECTED Val2", "" + mActivityRegisterBinding.getInputData().getDistrictId());

    }

    @Override
    public boolean checkValidation() {
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getName())) {
            Logger.toast(this, "Please provide hospital name.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getEmail())) {
            Logger.toast(this, "Please provide valid email.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getAddress())) {
            Logger.toast(this, "Please provide hospital address.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getPincode())) {
            Logger.toast(this, "Please provide pincode.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getContactperson())) {
            Logger.toast(this, "Please provide contact person's name.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getMobile())) {
            Logger.toast(this, "Please provide contact person's phone number.");
            return false;
        }

        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getBeds())) {
            Logger.toast(this, "Please provide number of beds in the hospital.");
            return false;
        }
        if (TextUtils.isEmpty(mActivityRegisterBinding.getInputData().getLandline())) {
            Logger.toast(this, "Please provide land line phone number.");
            return false;
        }
        return true;
    }

    @Override
    public Activity provideContext() {
        return this;
    }

    @Override
    public RegisterHCFRequest prepareRequest() {
        RegisterHCFRequest mRegisterHCFRequest = new RegisterHCFRequest();
        mRegisterHCFRequest.setName(mActivityRegisterBinding.getInputData().getName().trim());
        mRegisterHCFRequest.setEmail(mActivityRegisterBinding.getInputData().getEmail().trim());
        mRegisterHCFRequest.setAddress(mActivityRegisterBinding.getInputData().getAddress().trim());
        mRegisterHCFRequest.setContactperson(mActivityRegisterBinding.getInputData().getContactperson().trim());
        mRegisterHCFRequest.setMobile(mActivityRegisterBinding.getInputData().getMobile().trim());
        mRegisterHCFRequest.setLandline(mActivityRegisterBinding.getInputData().getLandline().trim());
        mRegisterHCFRequest.setBeds(mActivityRegisterBinding.getInputData().getBeds().trim());
        mRegisterHCFRequest.setCity(mActivityRegisterBinding.getInputData().getCity().trim());
        mRegisterHCFRequest.setDistrict(mActivityRegisterBinding.getInputData().getDistrictId());
        mRegisterHCFRequest.setState(mActivityRegisterBinding.getInputData().getState().trim());
        mRegisterHCFRequest.setHcf_type(mActivityRegisterBinding.getInputData().getHcf_type().trim());
        mRegisterHCFRequest.setPincode(mActivityRegisterBinding.getInputData().getPincode());
        mRegisterHCFRequest.setWebsite("dasda");
        mRegisterHCFRequest.setLatlong("");
        return mRegisterHCFRequest;
    }

    @Override
    public void startNextActivity() {
        Intent in = new Intent(this, AvailablePlansActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        navigateToNextActivity(in, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backAnimation();
    }

    @Override
    public void inflateStatesData(LocationDTO arg0) {
//         mLocationsModel = new Gson().fromJson(Utils.loadJSONFromAsset(this, "location.json"), LocationDTO.class);
        mLocationsModel = arg0;
        allStateLocationList = mLocationsModel.getLocationData();
        Collections.sort(allStateLocationList);
        Set<String> uniqueStateNames = new HashSet<>();
        for (LocationDTO.LocationData state : allStateLocationList) {
            uniqueStateNames.add(state.getStateName());
        }
        stateFilter = new ArrayList<String>(uniqueStateNames);
        Collections.sort(stateFilter);
        mStateAdapter = new StateCityAdapter(stateFilter);
        mStateSpinner.setAdapter(mStateAdapter);


    }
}
