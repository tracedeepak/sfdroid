package nansat.com.safebio.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import nansat.com.safebio.R;
import nansat.com.safebio.models.ReportIssueRequest;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.ResponseModel;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppEditText;
import nansat.com.safebio.widgets.AppTextView;
import retrofit2.Call;

/**
 * Created by Phantasmist on 28/03/18.
 */

public class ReportIssueActivity extends BaseActivity {
    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;
    private AppButton mSubmitButton;
    private AppEditText mIssueTitle;
    private AppEditText mIssueDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_issue);
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mSubmitButton = findViewById(R.id.submitButton);
        mIssueTitle = findViewById(R.id.issueTitle);
        mIssueDetails = findViewById(R.id.issueDetails);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        mAccountToolbarTitle.setText("Report Issue");
        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mSubmitButton.setOnClickListener((v) -> {
            makeRequest();
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backAnimation();
    }

    public boolean validateFeilds() {
        if (TextUtils.isEmpty(mIssueDetails.getText().toString())) {
            Logger.toast(this, "Please input issue details.");
            return false;
        }
        if (TextUtils.isEmpty(mIssueTitle.getText().toString())) {
            Logger.toast(this, "Please input issue title.");
            return false;
        }
        return true;
    }

    public void makeRequest() {
        if (validateFeilds()) {
            ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
            ReportIssueRequest mReportIssueRequest = new ReportIssueRequest();
            mReportIssueRequest.setApi_token(Utils.getString(this, Constant.KEY_TOKEN));
            mReportIssueRequest.setSubject(mIssueTitle.getText().toString());
            mReportIssueRequest.setMessage(mIssueDetails.getText().toString());
            Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().reportIssue(Constant.reportIssueReq(), mReportIssueRequest);
            modelCall.enqueue(new RetrofitCallback<ResponseModel>(this, mProgressDialog) {
                @Override
                public void onSuccess(ResponseModel arg0) {
                    Logger.showDialogAndCloseOnClick(ReportIssueActivity.this, "Success", "The issue has been logged successfully, you'll soon hear from us.");
                }
            });
        }
    }
}
