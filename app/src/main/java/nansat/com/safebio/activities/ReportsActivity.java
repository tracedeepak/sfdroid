package nansat.com.safebio.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.ReportsRecAdapter;
import nansat.com.safebio.adapters.StatusSpinnerAdapter;
import nansat.com.safebio.models.ReportsResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppTextView;
import retrofit2.Call;

/**
 * Created by Phantasmist on 28/03/18.
 */

public class ReportsActivity extends BaseActivity {

    private AppTextView mFromDate;
    private AppTextView mToDate;
    private Spinner mStatusSpinner;
    StatusSpinnerAdapter mStatusSpinnerAdapter;
    String fromDateText;
    String toDateText;
    String statusText;
    private AppTextView mAccountToolbarTitle;
    private Toolbar mAccountToolbar;
    private ViewFlipper mReportsViewFlipper;
    private AppButton mGetReportsButton;
    private RecyclerView mReportsRecView;

    List<ReportsResponse.Data> mReportsList = new ArrayList<>();
    ReportsRecAdapter mReportsRecAdapter;

    private void assignViews() {
        mAccountToolbar = findViewById(R.id.accountToolbarActivity);
        mAccountToolbarTitle = mAccountToolbar.findViewById(R.id.toolbarTitle);
        mReportsRecView = findViewById(R.id.reportsRecView);
        mAccountToolbarTitle.setText("Reports");
        setSupportActionBar(mAccountToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mFromDate = (AppTextView) findViewById(R.id.fromDate);
        mReportsViewFlipper = findViewById(R.id.reportsFlipper);
        mToDate = (AppTextView) findViewById(R.id.toDate);
        mStatusSpinner = (Spinner) findViewById(R.id.statusSpinner);
        mGetReportsButton = findViewById(R.id.getReportsButton);

        mStatusSpinnerAdapter = new StatusSpinnerAdapter();
        mStatusSpinner.setAdapter(mStatusSpinnerAdapter);

        if (isLessThanApi23()) {
            mStatusSpinner.setBackground(ContextCompat.getDrawable(this, R.drawable.spinner_bg));
        } else {
            mStatusSpinner.setBackground(ContextCompat.getDrawable(this, R.drawable.spinner_pressed));
        }

        mReportsRecAdapter = new ReportsRecAdapter(mReportsList);
        mReportsRecAdapter.setHasStableIds(true);
        mReportsRecView.setHasFixedSize(true);
        mReportsRecView.setLayoutManager(new LinearLayoutManager(this));
        mReportsRecView.setAdapter(mReportsRecAdapter);


    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        assignViews();
        mFromDate.setOnClickListener((v) -> {
            openDatePicker(1);
        });
        mToDate.setOnClickListener((v) -> {
            openDatePicker(2);
        });
        mStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                statusText = String.valueOf(i + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mGetReportsButton.setOnClickListener((v) -> {
            if (validateInputs()) {
                fetchReports();
            }
        });

    }


    public boolean validateInputs() {
        if (TextUtils.isEmpty(fromDateText)) {
            Logger.toast(this, "Please select a start date");
            return false;
        }
        if (TextUtils.isEmpty(toDateText)) {
            Logger.toast(this, "Please select a end date");
            return false;
        }
        return true;
    }

    public void openDatePicker(int flag) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (flag == 1) {
                            mFromDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            fromDateText = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        } else {
                            mToDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            toDateText = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }


                    }
                }, mYear, mMonth, mDay);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mReportsViewFlipper.getDisplayedChild() == 1) {
            mReportsViewFlipper.setDisplayedChild(0);
            mReportsList.clear();
            mReportsRecAdapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
            backAnimation();
        }
    }

    public static boolean isLessThanApi23() {
        if (Build.VERSION.SDK_INT < 23)
            return true;
        else
            return false;
    }


    void fetchReports() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        Call<ReportsResponse> mReportsResponseCall = SafeBioClient.getInstance().getApiInterface().getReports(Constant.getReports(), Utils.getString(this, Constant.KEY_TOKEN), fromDateText, toDateText, statusText);
        mReportsResponseCall.enqueue(new RetrofitCallback<ReportsResponse>(this, mProgressDialog) {
            @Override
            public void onSuccess(ReportsResponse arg0) {
                if (!arg0.getData().isEmpty()) {
                    mReportsViewFlipper.setDisplayedChild(1);
                    mReportsList.addAll(arg0.getData());
                    mReportsRecAdapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(ReportsActivity.this,"No reports were found.",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

}
