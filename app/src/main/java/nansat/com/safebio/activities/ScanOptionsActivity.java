package nansat.com.safebio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import nansat.com.safebio.R;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 23/04/18.
 */

public class ScanOptionsActivity extends BaseActivity {

    private Toolbar mSelectLotToolBar;
    private AppTextView mToolbarTitle;
    private AppButton mScanWithWeight;
    private AppButton mScanWithoutWeight;
    boolean isDeposit;
    boolean isHCF;
    int hospitalId = 0;
    int optionType = 0;//determinse whetehr it is pickup or deposit

    private void assignViews() {
        mScanWithWeight = (AppButton) findViewById(R.id.scanWithWeight);
        mScanWithoutWeight = (AppButton) findViewById(R.id.scanWithoutWeight);
        mSelectLotToolBar = findViewById(R.id.selectLotToolBar);
        mToolbarTitle = mSelectLotToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Scan Options");
        setSupportActionBar(mSelectLotToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_options);
        assignViews();
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);
        optionType = getIntent().getIntExtra("optionType", 0);
        isHCF = getIntent().getBooleanExtra("isHCF", false);
        hospitalId = getIntent().getIntExtra("hospitalId", 0);

//        if (Utils.getString(this, Constant.KEY_USER_ROLE).toUpperCase().equals(Constant.ROLE_HCF.toUpperCase())) {
//            MyAccountResponse.Hcf hcfData = new Gson().fromJson(Utils.getString(this, Constant.KEY_HCF_DATA), MyAccountResponse.Hcf.class);
//            try {
//                int noOfBeds = Integer.valueOf(hcfData.getBeds());
//                if (noOfBeds < 30) {
//                    mScanWithoutWeight.setVisibility(View.VISIBLE);
//                } else {
//                    mScanWithoutWeight.setVisibility(View.GONE);
//                }
//            } catch (Exception e) {
//
//            }
//        }

        mScanWithWeight.setOnClickListener((v) -> {
            Intent in = new Intent(ScanOptionsActivity.this, SelectBluetoothDevicesActivity.class);
            in.putExtra("isDeposit", isDeposit);
            in.putExtra("optionType", optionType);
            in.putExtra("enableWeightScan", true);
            in.putExtra("hospitalId", hospitalId);
            in.putExtra("isHCF",isHCF);
            startActivity(in);
            this.finish();
        });
        mScanWithoutWeight.setOnClickListener((v) -> {
            Intent in = new Intent(ScanOptionsActivity.this, WeightCaptureActivity.class);
            in.putExtra("isDeposit", isDeposit);
            in.putExtra("optionType", optionType);
            in.putExtra("enableWeightScan", false);
            in.putExtra("hospitalId", hospitalId);
            in.putExtra("isHCF",isHCF);
            startActivity(in);
            this.finish();
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
