package nansat.com.safebio.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.DeviceRecAdapter;
import nansat.com.safebio.contracts.SelectblueDeviceActContract;
import nansat.com.safebio.databinding.ActivitySelectBluetoothDeviceBinding;
import nansat.com.safebio.models.Device;
import nansat.com.safebio.presenter.SelectBluetoothDeviceActPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.RecyclerItemClickListener;
import nansat.com.safebio.widgets.AppTextView;


/**
 * Created by Phantasmist on 09/02/18.
 */

public class SelectBluetoothDevicesActivity extends BaseActivity implements SelectblueDeviceActContract {
    private static final int PERMISSION_STATUS = 2;
    private static final int REQUEST_ENABLE_BT = 222;
    private static final String TAG = "SELECT BT DEVICE";
    BluetoothAdapter mBluetoothAdapter;
    List<Device> pairedDeviceList = new ArrayList<>();
    List<Device> nearbyDeviceList = new ArrayList<>();
    RecyclerView mPrePairedRecView;
    SelectBluetoothDeviceActPresenter mPresenter;

    DeviceRecAdapter mDeviceRecAdapter;
    DeviceRecAdapter mNearbyDeviceRecAdapter;
    ActivitySelectBluetoothDeviceBinding mSelectBluetoothDeviceBinding;
    boolean isSearchReceiverRegistered = false;
    boolean isPairingReceiverRegistered = false;

    Toolbar mToolbar;
    AppTextView mToolbarTitle;

    String selectedHcfName;
    int selectedHcfId;
    private boolean isDeposit;
    private int lotId;
    int optionType = 0;
    private AlertDialog nearbyDevicesAlertDialog;
    boolean enableWeightScan = false;
    private boolean isHCF;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectBluetoothDeviceBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_bluetooth_device);
        mPrePairedRecView = findViewById(R.id.pairedDevicesList);
        mToolbar = findViewById(R.id.toolBar);
        mToolbarTitle = mToolbar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Select Device");
        Log.e("ID", "" + getIntent().getIntExtra("hospitalId", 0));
//        selectedHcfName = getIntent().getStringExtra("hospitalName");
        selectedHcfId = getIntent().getIntExtra("hospitalId", 0);
        optionType = getIntent().getIntExtra("optionType", 0);
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);
        enableWeightScan = getIntent().getBooleanExtra("enableWeightScan", false);
        isHCF = getIntent().getBooleanExtra("isHCF", false);

//        if (isDeposit && optionType == Constant.PICKUP) {
//            lotId = getIntent().getIntExtra("preSavedLotIdOnApi", 0);
//        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        mPresenter = new SelectBluetoothDeviceActPresenter(this);
        mSelectBluetoothDeviceBinding.setPresenter(mPresenter);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mDeviceRecAdapter = new DeviceRecAdapter(pairedDeviceList);
        mDeviceRecAdapter.setHasStableIds(true);
        mPrePairedRecView.setLayoutManager(new LinearLayoutManager(this));
        mPrePairedRecView.setHasFixedSize(true);
        mPrePairedRecView.setAdapter(mDeviceRecAdapter);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                getPrePairedDeviceList();
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
                registerReceiver(mPairingReceiver, filter);
                isPairingReceiverRegistered = true;
            }
        }

        mPrePairedRecView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent in = new Intent(SelectBluetoothDevicesActivity.this, WeightCaptureActivity.class);
//                in.putExtra("data", new Gson().toJson(mDeviceRecAdapter.getItemAtPosition(position)));
                in.putExtra("data", mDeviceRecAdapter.getItemAtPosition(position));
//                in.putExtra("hospitalName", selectedHcfName);
                in.putExtra("hospitalId", selectedHcfId);
                in.putExtra("isDeposit", isDeposit);
                in.putExtra("optionType", optionType);
                in.putExtra("enableWeightScan", enableWeightScan);
//                in.putExtra("enableWeightScan", false);
                in.putExtra("isHCF", isHCF);

//                if (isDeposit) {
//                    in.putExtra("preSavedLotIdOnApi", lotId);
//                }
                navigateToNextActivity(in, false);
            }
        }));
    }


    public void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                PERMISSION_STATUS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_STATUS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ENABLE_BT) {
                Log.e("BLUETOOTH ", "ENABLED");
                getPrePairedDeviceList();
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ENABLE_BT) {
                Log.e("BLUETOOTH ", "DISABLED");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getPrePairedDeviceList() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
//                Log.e("Device Renamed",""+renamePairedDevice(device,device.getName()+"_"+selectedHcfName+"_"+selectedHcfId));

            }
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                ParcelUuid[] uuid = new ParcelUuid[0];


                if (device.getUuids() != null) {
                    uuid = device.getUuids();
                    UUID deviceUUID = uuid[0].getUuid();
                    Log.e("Renamed", "" + getBluetoothDeviceName(device));
                    Device mDevice = new Device();
                    mDevice.setDeviceName(getBluetoothDeviceName(device));
                    mDevice.setDeviceAddress(deviceHardwareAddress);
                    mDevice.setUuid(deviceUUID.toString());
                    mDevice.setmBluetoothDevice(device);
                    pairedDeviceList.add(mDevice);
                    mDeviceRecAdapter.notifyDataSetChanged();
                    Log.e("UUID", "" + deviceUUID);
                    Log.e("BOND STATE", "" + device.getBondState());
                    Log.e("DEV NAME", "" + getBluetoothDeviceName(device));
                }
            }
        }
    }


    @Override
    public Activity provideContext() {
        return this;
    }

    @Override
    public void openNearbyDeviceDialog() {


        View alertLayout = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog_with_list, null);
        RecyclerView nearbyDeviceListRecView = alertLayout.findViewById(R.id.nearbyDeviceList);
        AppTextView cancelButton = alertLayout.findViewById(R.id.cancelButton);
        AppTextView refreshButton = alertLayout.findViewById(R.id.refreshButton);

        AlertDialog.Builder nearbyDevicesAlertDialogBuilder = new AlertDialog.Builder(this);
        nearbyDevicesAlertDialogBuilder.setView(alertLayout);
        nearbyDevicesAlertDialogBuilder.setCancelable(false);

        mNearbyDeviceRecAdapter = new DeviceRecAdapter(nearbyDeviceList);
        mNearbyDeviceRecAdapter.setHasStableIds(true);
        nearbyDeviceListRecView.setLayoutManager(new LinearLayoutManager(this));
        nearbyDeviceListRecView.setHasFixedSize(true);
        nearbyDeviceListRecView.setAdapter(mNearbyDeviceRecAdapter);

        nearbyDevicesAlertDialog = nearbyDevicesAlertDialogBuilder.create();
        nearbyDevicesAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        nearbyDevicesAlertDialog.show();

        registerDeviceForAnyNEwDeviceFound();

        nearbyDeviceListRecView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mNearbyDeviceRecAdapter.getItemAtPosition(position).getmBluetoothDevice().createBond();
                nearbyDevicesAlertDialog.dismiss();
            }
        }));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(nearbyDevicesAlertDialog.getWindow().getAttributes());

        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.7f);
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;

        // Apply the newly created layout parameters to the alert dialog window
        nearbyDevicesAlertDialog.getWindow().setAttributes(layoutParams);


        cancelButton.setOnClickListener((v) -> {
            if (nearbyDevicesAlertDialog != null) {
                nearbyDevicesAlertDialog.dismiss();
            }
            if (mSearchReceiver != null) {
                unregisterReceiver(mSearchReceiver);
            }
        });
        refreshButton.setOnClickListener((v) -> {
            nearbyDeviceList.clear();
            mNearbyDeviceRecAdapter.notifyDataSetChanged();

        });
    }

    public void registerDeviceForAnyNEwDeviceFound() {
        // Register for broadcasts when a device is discovered.
        mBluetoothAdapter.startDiscovery();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mSearchReceiver, filter);
        Log.e("STATUS", "" +
                mBluetoothAdapter.isDiscovering());
        isSearchReceiverRegistered = true;
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mSearchReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.e("SEARCH STARTED", "TRUE");
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                try {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    Device mDevice = new Device();
                    mDevice.setDeviceName(deviceName);
                    mDevice.setDeviceAddress(deviceHardwareAddress);
                    mDevice.setmBluetoothDevice(device);

                    Log.e("Discovered DEVICE", deviceName);
                    Log.e("BOND STATE", "" + device.getBondState());

                    nearbyDeviceList.add(mDevice);
                    mNearbyDeviceRecAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.e("EXCEPTION", e.getLocalizedMessage());
                }

            }
        }
    };


    private final BroadcastReceiver mPairingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    Log.e(TAG, "BroadcastReceiver: BOND_BONDED." + mDevice.getName());
                    mBluetoothAdapter.cancelDiscovery();
                    openRenameDeviceDialog(mDevice);
                }
                //case2: creating a bone
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                    Log.e(TAG, "BroadcastReceiver: BOND_BONDING." + mDevice.getName());
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                    Log.e(TAG, "BroadcastReceiver: BOND_NONE." + mDevice.getName());
                }
            }
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Don't forget to unregister the ACTION_FOUND receiver.
        if (isSearchReceiverRegistered) {
            if (mSearchReceiver != null) {
                try {
                    unregisterReceiver(mSearchReceiver);
                } catch (Exception e) {

                }
            }
        }
        if (isPairingReceiverRegistered) {
            if (mPairingReceiver != null) {
                try {
                    unregisterReceiver(mPairingReceiver);
                } catch (Exception e) {

                }
            }
        }
    }

    @SuppressLint("ResourceAsColor")
    public void openRenameDeviceDialog(BluetoothDevice mDevice) {
        final EditText txtUrl = new EditText(this);
        // Set the default text to a link of the Queen
        txtUrl.setHint("Unique Device Name");
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(30, 10, 30, 10);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(llp);
        layout.addView(txtUrl, llp);

        AlertDialog mAlertDialog = new AlertDialog.Builder(this)
                .setTitle("Add Device id")
                .setMessage("You have successfully paired with the device, we would like you to add a unique identifier to your device so that when you want to connect with same device again in future, you can remember it by its unique name.")
                .setView(layout)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                })
                .setCancelable(false).create();
        mAlertDialog.show();

        mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener((v) -> {
            String deviceName = txtUrl.getText().toString();
            Log.e("DEVICE NAME", deviceName);
            if (!TextUtils.isEmpty(deviceName)) {
                renamePairedDevice(mDevice, mDevice.getName() + "_" + deviceName);
                mAlertDialog.dismiss();
                pairedDeviceList.clear();
                mDeviceRecAdapter.notifyDataSetChanged();
                getPrePairedDeviceList();
                if (nearbyDevicesAlertDialog != null && nearbyDevicesAlertDialog.isShowing()) {
                    nearbyDevicesAlertDialog.dismiss();
                }
            } else {
                Toast.makeText(SelectBluetoothDevicesActivity.this, "Please provide a unique name.", Toast.LENGTH_LONG).show();
            }
        });

////        mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
//        txtUrl.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence.length() > 0) {
//                    mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
//                } else {
//                    mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });


    }

    public boolean renamePairedDevice(BluetoothDevice bluetoothDevice, String name) {
        try {
            Method m = bluetoothDevice.getClass().getMethod("setAlias", String.class);
            m.invoke(bluetoothDevice, name);
            Toast.makeText(this, "Your device has been renamed to : " + name, Toast.LENGTH_LONG).show();
            return true;
        } catch (Exception e) {
            Log.d(TAG, "error renaming device:" + e.getMessage());
            return false;
        }
    }

    public String getBluetoothDeviceName(BluetoothDevice bluetoothDevice) {
        String deviceAlias = bluetoothDevice.getName();
        try {
            Method method = bluetoothDevice.getClass().getMethod("getAliasName");
            if (method != null) {
                deviceAlias = (String) method.invoke(bluetoothDevice);
                return deviceAlias;
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return "";
    }


}