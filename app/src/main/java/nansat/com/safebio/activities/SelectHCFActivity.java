package nansat.com.safebio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.HCFListAdapter;
import nansat.com.safebio.models.HcfResponse;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import nansat.com.safebio.widgets.AppEditText;
import nansat.com.safebio.widgets.AppTextView;
import retrofit2.Call;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class SelectHCFActivity extends BaseActivity {
    private Toolbar mHcfToolBar;
    private ListView mHospitalList;
    AppTextView mToolbarRightText;
    HCFListAdapter mHcfListAdapter;
    List<HcfResponse.Data> mData = new ArrayList<>();
    AppEditText mSearchBar;
    private AppTextView mToolbarTitle;
    boolean isDeposit;
    int optionType = 0;//determinse whetehr it is pickup or deposit

    private void assignViews() {
        mHcfToolBar = findViewById(R.id.hcfToolBar);
        mHospitalList = findViewById(R.id.hospitalList);
        mSearchBar = findViewById(R.id.searchBar);
        mToolbarRightText = mHcfToolBar.findViewById(R.id.toolbarRightText);
        mToolbarTitle = mHcfToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Choose Hospital");
        mToolbarRightText.setVisibility(View.GONE);

        setSupportActionBar(mHcfToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_select_hcf);
        assignViews();
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);
        optionType = getIntent().getIntExtra("optionType", 0);

        mHcfListAdapter = new HCFListAdapter(mData);
        mHospitalList.setAdapter(mHcfListAdapter);

        mSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mHcfListAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        fetchHCFList();

        mHospitalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!isDeposit) {
                    Intent in = new Intent(SelectHCFActivity.this, SelectBluetoothDevicesActivity.class);
                    in.putExtra("hospitalName", mHcfListAdapter.getItem(i).getName());
                    in.putExtra("hospitalId", mHcfListAdapter.getItem(i).getHcfId());
                    in.putExtra("isDeposit", false);
                    navigateToNextActivity(in, false);
                } else {
                    Intent in = new Intent(SelectHCFActivity.this, SelectLotActivity.class);
                    in.putExtra("hospitalName", mHcfListAdapter.getItem(i).getName());
                    in.putExtra("hospitalId", mHcfListAdapter.getItem(i).getHcfId());
                    in.putExtra("isDeposit", true);
                    in.putExtra("optionType", optionType);
                    navigateToNextActivity(in, false);
                }
            }
        });

//        mToolbarRightText.setOnClickListener((v)->{
//            Intent in = new Intent(SelectHCFActivity.this, SelectBluetoothDevicesActivity.class);
//            in.putExtra("isDeposit", false);
//            in.putExtra("optionType", Constant.SKIP);
//            in.putExtra("hospitalName", "CPWTF");
//            in.putExtra("hospitalId", 0);
//            navigateToNextActivity(in, false);
//        });
    }

    public void fetchHCFList() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        Call<HcfResponse> mHcfResponseCall = SafeBioClient.getInstance().getApiInterface().getHCFListApi(Constant.getHCFList(), Utils.getString(this, Constant.KEY_TOKEN));
        mHcfResponseCall.enqueue(new RetrofitCallback<HcfResponse>(this, mProgressDialog) {
            @Override
            public void onSuccess(HcfResponse arg0) {
                mData.addAll(arg0.getData());
                mHcfListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
