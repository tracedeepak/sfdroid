package nansat.com.safebio.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.SelectLotRecAdapter;
import nansat.com.safebio.contracts.SelectLotActContract;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.presenter.SelectLotActPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.RecyclerItemClickListener;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 19/02/18.
 */

public class SelectLotActivity extends BaseActivity implements SelectLotActContract {

    private RecyclerView mLotListRecView;
    private Toolbar mSelectLotToolBar;
    private AppTextView mToolbarTitle;
    SelectLotActPresenter mSelectLotActPresenter;
    SelectLotRecAdapter mSelectLotRecAdapter;
    List<HCFLotResponse.Data> mLotList;

    int selectedLotId;
    boolean isDeposit;
    private String selectedHcfName;
    private int selectedHcfId;
    int optionType = 0;

    private void assignViews() {
        mLotListRecView = findViewById(R.id.lotList);
        mSelectLotToolBar = findViewById(R.id.selectLotToolBar);
        mToolbarTitle = mSelectLotToolBar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Select Lot");
        setSupportActionBar(mSelectLotToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mLotList = new ArrayList<>();
        mLotListRecView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_select_lot);
        assignViews();
        selectedHcfName = getIntent().getStringExtra("hospitalName");
        selectedHcfId = getIntent().getIntExtra("hospitalId", 0);
        optionType = getIntent().getIntExtra("optionType", 0);
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);

        mSelectLotActPresenter = new SelectLotActPresenter(this);
        mSelectLotRecAdapter = new SelectLotRecAdapter(mLotList);
        mLotListRecView.setHasFixedSize(true);
        mSelectLotRecAdapter.setHasStableIds(true);
        mLotListRecView.setAdapter(mSelectLotRecAdapter);

        mLotListRecView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent in = new Intent(SelectLotActivity.this, SelectBluetoothDevicesActivity.class);
                in.putExtra("hospitalName", selectedHcfName);
                in.putExtra("hospitalId", selectedHcfId);
                in.putExtra("isDeposit", true);
                in.putExtra("optionType", optionType);
                in.putExtra("preSavedLotIdOnApi", mSelectLotRecAdapter.getItemAtPosition(position).getLotId());
                navigateToNextActivity(in, false);

            }
        }));

        mSelectLotActPresenter.fetchHcfLotList(selectedHcfId);
    }

    @Override
    public Activity provideContext() {
        return this;
    }

    @Override
    public void addLotstoRecyclerView(List<HCFLotResponse.Data> data) {
        if (!data.isEmpty()) {
            mLotList.addAll(data);
            mSelectLotRecAdapter.notifyDataSetChanged();
        } else {
            Intent in = new Intent(SelectLotActivity.this, SelectBluetoothDevicesActivity.class);
            in.putExtra("hospitalName", selectedHcfName);
            in.putExtra("hospitalId", selectedHcfId);
            in.putExtra("isDeposit", true);
            in.putExtra("optionType", Constant.SKIP);
            navigateToNextActivity(in, false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
