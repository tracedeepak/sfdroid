package nansat.com.safebio.activities;

import android.content.Intent;
import android.util.Log;

import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Utils;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected void onStart() {
        super.onStart();
        if (!Utils.getString(this, Constant.KEY_TOKEN).isEmpty()) {
            Log.e("DATA", Utils.getString(this, Constant.KEY_TOKEN));
            Log.e("DATA SUBS", ""+Utils.getBoolean(this, Constant.SUBSCRIPTION_TAKEN));
            if (Utils.getBoolean(this, Constant.SUBSCRIPTION_TAKEN)) {
                if (!Utils.getBoolean(this, Constant.KEY_ACTIVATION_STATUS)) {
                    Intent in = new Intent(this, CallSupportActivity.class);
                    navigateToNextActivity(in, true);
                } else {
                    Intent in = new Intent(this, HomeActivity.class);
                    navigateToNextActivity(in, true);
                }
            } else {
                Intent in = new Intent(this, AvailablePlansActivity.class);
                navigateToNextActivity(in, true);
            }
        } else {
            Intent in = new Intent(this, LoginActivity.class);
            navigateToNextActivity(in, true);
        }
    }
}
