package nansat.com.safebio.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.annimon.stream.Stream;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nansat.com.safebio.R;
import nansat.com.safebio.adapters.CollectedBagsAdapter;
import nansat.com.safebio.contracts.ViewLotContract;
import nansat.com.safebio.database.models.Bag;
import nansat.com.safebio.database.models.Lot;
import nansat.com.safebio.database.viewmodels.BagViewModel;
import nansat.com.safebio.database.viewmodels.LotViewModel;
import nansat.com.safebio.databinding.ActivityViewLotBinding;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.models.SaveLotRequest;
import nansat.com.safebio.presenter.ViewLotActPresenter;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.LocationHelper;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.widgets.AppButton;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 18/02/18.
 */

public class ViewLotActivity extends BaseActivity implements ViewLotContract, OnSuccessListener<Void>, OnFailureListener {

    private static final int PERMISSION_STATUS = 21;
    private Toolbar mViewLotToolbar;
    private RecyclerView mCollectedBagssList;
    private AppButton mSaveButt;

    ActivityViewLotBinding mActivityViewLotBinding;
    private LotViewModel mLotViewModel;
    CollectedBagsAdapter mCollectedBagsAdapter;
    List<Bag> mCollectedBagsList;
    List<SaveLotRequest.Bag> mExistingBagList;
    ViewLotActPresenter mViewLotActPresenter;

    private BagViewModel mBagViewModel;
    private boolean isDeposit;
    private int preSavedLotIdOnApi;
    private String preFetchedLot;
    int optionType = 0;
    private AppTextView mToolbarTitle;


    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    List<HCFLotResponse.Lotdetails> preFetchedLotList = new ArrayList<HCFLotResponse.Lotdetails>();

    private void assignViews() {
        mViewLotToolbar = (Toolbar) findViewById(R.id.viewLotToolbar);
        mCollectedBagssList = (RecyclerView) findViewById(R.id.collectedBagssList);
        mSaveButt = (AppButton) findViewById(R.id.saveButt);

        mToolbarTitle = mViewLotToolbar.findViewById(R.id.toolbarTitle);
        if (optionType == 0 || optionType == Constant.SKIP) {
            mToolbarTitle.setText("Review Lot");
        } else if (optionType == Constant.PICKUP) {
            mToolbarTitle.setText("Review Pickup");
        } else if (optionType == Constant.DEPOSIT) {
            mToolbarTitle.setText("Review Deposit");
        }
        setSupportActionBar(mViewLotToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    long lotId;
    int hospitalId;
    String totalWeight;
    int totalBags;
    Lot mCurrentLot;
    private String provider;

    double latitude = 0.0;
    double longitude = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityViewLotBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_lot);
        lotId = getIntent().getLongExtra("lot_id", 0);
        hospitalId = getIntent().getIntExtra("hospitalId", -1);
        optionType = getIntent().getIntExtra("optionType", 0);
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);
        preSavedLotIdOnApi = getIntent().getIntExtra("preSavedLotIdOnApi", 0);
        preFetchedLot = getIntent().getStringExtra("preSavedLots");

        //LOCATION STUFF
        mLocationRequest = LocationHelper.createLocationRequest();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (LocationHelper.checkLocationPermissions(this)) {
            startGps();
        } else {
            requestPermissions();
        }

        Log.d("-----HCFID----", "" + hospitalId);
        assignViews();
        mViewLotActPresenter = new ViewLotActPresenter(this);
        mCollectedBagsList = new ArrayList<>();
        mExistingBagList = new ArrayList<>();
        GridLayoutManager manager = new GridLayoutManager(this, 1);
        mCollectedBagssList.setLayoutManager(manager);

        mCollectedBagsAdapter = new CollectedBagsAdapter(mCollectedBagsList, null, true);
        mCollectedBagsAdapter.setHasStableIds(true);
        mCollectedBagssList.setHasFixedSize(true);
        mCollectedBagssList.setAdapter(mCollectedBagsAdapter);

        mLotViewModel = ViewModelProviders.of(this).get(LotViewModel.class);
        mBagViewModel = ViewModelProviders.of(this).get(BagViewModel.class);

        mBagViewModel.getAllBagsThatBelongToLotWithId(lotId).observe(this, bagsListObserver);


        mLotViewModel.getLotByID(lotId).observe(this, new Observer<Lot>() {
            @Override
            public void onChanged(@Nullable Lot lot) {
                if (lot != null) {
                    totalBags = lot.getTotalBags();
                    totalWeight = "" + lot.getTotalWeight();
                    mCurrentLot = lot;
                    if (totalBags != 0) {
                        mSaveButt.setVisibility(View.VISIBLE);
                    } else {
                        mSaveButt.setVisibility(View.GONE);
                    }

                }
            }
        });

        mSaveButt.setOnClickListener((v) -> {
//            Log.e("PREPARED REQ", new Gson().toJson(prepareRequest()));
            if (isDeposit) {
                //PICKUP GUY WILL LAND HERE
                if (optionType == Constant.PICKUP) {
//                    if (preSavedLotIdOnApi!=0) {
                    mViewLotActPresenter.saveLotAndConfirm(prepareRequest());
//                    }else{
//                        mViewLotActPresenter.saveLot(prepareRequest());
//                    }
                }
            } else {

                if (optionType == Constant.DEPOSIT) {
                    //DEPOSIT GUY WILL LAND HERE
                    mViewLotActPresenter.depositLot(prepareRequest());
                } else {
                    //HCF GUY WILL LAND HERE
                    mViewLotActPresenter.saveLot(prepareRequest());
                }
            }
        });

        if (!TextUtils.isEmpty(preFetchedLot)) {
            preFetchedLotList = new Gson().fromJson(preFetchedLot, HCFLotResponse.class).data.get(0).getLotdetails();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * OBSERVER TO OBSERVE ANY CHANGE LIKE ADDITION IN BAGS LIST
     * AND UPDATE THE CUMULATIVE DATA LIKE TOTAL WEIGHT AND BAGS
     * TO THE LOTS DATA IN LOT TABLE
     */
    Observer<List<Bag>> bagsListObserver = new Observer<List<Bag>>() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onChanged(@Nullable List<Bag> bags) {
            Log.e("BAGS LIST OBS", new Gson().toJson(bags));
            mCollectedBagsList.clear();
            mCollectedBagsAdapter.notifyDataSetChanged();
            mCollectedBagsList.addAll(bags);
            mCollectedBagsAdapter.notifyDataSetChanged();
        }
    };

    SaveLotRequest prepareRequest() {
        SaveLotRequest mSaveLotRequest = new SaveLotRequest();
        mSaveLotRequest.setApiToken(Utils.getString(this, Constant.KEY_TOKEN));
        mSaveLotRequest.setHcfId(hospitalId);
        mSaveLotRequest.setTotalBags(totalBags);
        mSaveLotRequest.setTotalWeight(totalWeight);

        //is deposti is true in case of pickup only && send lot id in case of PICKUP
        if (isDeposit) {
            List<SaveLotRequest.Bag> mBagsList = new ArrayList<>();

            for (Bag mBag : mCollectedBagsList) {
                SaveLotRequest.Bag bag = new SaveLotRequest.Bag();
                bag.setScanCode(mBag.getBarCode());
                bag.setSkuId(mBag.getSkuId());
                bag.setWeight("" + mBag.getWeight());
                mBagsList.add(bag);
            }
            mSaveLotRequest.setLotId(preSavedLotIdOnApi);
            if (!preFetchedLotList.isEmpty()) {
                for (HCFLotResponse.Lotdetails data : preFetchedLotList) {
                    SaveLotRequest.Bag bag = new SaveLotRequest.Bag();
                    bag.setScanCode(data.getScanCode());
                    bag.setSkuId(data.getSkuId());
                    bag.setWeight("" + data.getWeight());
                    mExistingBagList.add(bag);
                }
            }

            List<SaveLotRequest.Bag> duplicates = new ArrayList<>();

            Log.e("EXISTING SIZE", "" + mExistingBagList.size());
            for (SaveLotRequest.Bag b : mExistingBagList) {
                for (SaveLotRequest.Bag o : mBagsList) {
                    if (o.getScanCode().equals(b.getScanCode())) {
                        Log.e("ORIG LIST HAS ", o.getScanCode());
                        duplicates.add(b);
                        Log.e("EXISTING SIZE R", "" + mExistingBagList.size());
                    }
                }
            }
            mExistingBagList.removeAll(duplicates);
            mBagsList.addAll(mExistingBagList);

            double allWeightSum;
            //RECALCULATE WEIGHT IN THIS CASE
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                allWeightSum = mBagsList.stream().collect(Collectors.summingDouble(n -> Double.parseDouble(n.getWeight())));
            } else {
                allWeightSum = Stream.of(mBagsList).collect(com.annimon.stream.Collectors.summingDouble(n -> Double.parseDouble(n.getWeight())));
            }
            mSaveLotRequest.setTotalBags(mBagsList.size());
            mSaveLotRequest.setTotalWeight("" + allWeightSum);
            mSaveLotRequest.setLotdetails(mBagsList);

        }else{
            List<SaveLotRequest.Bag> mBagsList = new ArrayList<>();

            for (Bag mBag : mCollectedBagsList) {
                SaveLotRequest.Bag bag = new SaveLotRequest.Bag();
                bag.setScanCode(mBag.getBarCode());
                bag.setSkuId(mBag.getSkuId());
                bag.setWeight("" + mBag.getWeight());
                mBagsList.add(bag);
            }
            mSaveLotRequest.setLotdetails(mBagsList);
        }


        mSaveLotRequest.setLat("" + latitude);
        mSaveLotRequest.setLng("" + longitude);
        Log.e("PREPARED REQ", new Gson().toJson(mSaveLotRequest));

        return mSaveLotRequest;
    }


    @Override
    public void deleteDataAndCloseActivity() {
        mLotViewModel.deleteLot(mCurrentLot);

        if (isDeposit) {
            Logger.showDialogSetResultOkAndCloseOnClick(this, "Success", "Lot saved and confirmed successfully.", null);
        } else {
            Logger.showDialogSetResultOkAndCloseOnClick(this, "Success", "Lot saved successfully.", null);
        }
    }

    @Override
    public Activity provideContext() {
        return this;
    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                PERMISSION_STATUS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_STATUS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startGps();

                } else {
                }

                break;
        }
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            //Here is your location result
            Log.e("LATITUDE", "" + latitude);
            Log.e("LONGITUDE", "" + longitude);
            latitude = locationResult.getLastLocation().getLatitude();
            longitude = locationResult.getLastLocation().getLongitude();
            Log.e("LATITUDE", "" + latitude);
            Log.e("LONGITUDE", "" + longitude);

        }
    };


    @SuppressLint("MissingPermission")
    private void startGps() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null).addOnSuccessListener(this).addOnFailureListener(this);

    }

    @Override
    public void onSuccess(Void aVoid) {
        Log.e("LOCATION SUCCESS", "TRUE");
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        Log.e("LOCATION SUCCESS", e.getMessage());
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startGps();
    }
}
