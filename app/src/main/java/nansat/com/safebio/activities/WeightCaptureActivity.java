package nansat.com.safebio.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.gson.Gson;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import io.reactivex.disposables.Disposable;
import nansat.com.safebio.R;
import nansat.com.safebio.adapters.CollectedBagsAdapter;
import nansat.com.safebio.contracts.WeightCaptureActContract;
import nansat.com.safebio.database.models.Bag;
import nansat.com.safebio.database.models.Lot;
import nansat.com.safebio.database.models.Sku;
import nansat.com.safebio.database.viewmodels.BagViewModel;
import nansat.com.safebio.database.viewmodels.LotViewModel;
import nansat.com.safebio.database.viewmodels.SkuViewModel;
import nansat.com.safebio.databinding.ActivityWeightCaptureBinding;
import nansat.com.safebio.datamodels.WeightCaptureViewModel;
import nansat.com.safebio.models.Device;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.presenter.WeightCaptureActPresenter;
import nansat.com.safebio.threads.BTDataTransferThread;
import nansat.com.safebio.threads.ConnectThread;
import nansat.com.safebio.utils.BaseActivity;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.widgets.AppEditText;
import nansat.com.safebio.widgets.AppTextView;

/**
 * Created by Phantasmist on 10/02/18.
 */

public class WeightCaptureActivity extends BaseActivity implements ConnectThread.OnDeviceConnectedListener, BTDataTransferThread.BTDataTransferListener, WeightCaptureActContract {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 23;
    ActivityWeightCaptureBinding mActivityWeightCaptureBinding;
    WeightCaptureViewModel weightCaptureViewModel;
    Device mDeviceData;
    ConnectThread mConnectThread;
    BTDataTransferThread mDataTransferThread;

    BluetoothAdapter mBluetoothAdapter;
    Handler mDataHandler;
    final int handlerState = 0;
    private StringBuilder recDataString = new StringBuilder();

    Toolbar mToolbar;
    private AppTextView mToolbarTitle;
    private AppCompatImageView mToolbarIcon;
    private ViewFlipper mBarCodeFlipper;
    private RecyclerView mBagsRecView;

    WeightCaptureActPresenter mWeightCaptureActPresenter;
    /**
     * BARCODE STUFF
     */
    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    private String lastText;

    private Animation animationFlipIn;
    private Animation animationFlipOut;
    DrawerLayout mDrawerLayout;

    String selctedHospitalName;
    int selctedHospitalid;
    boolean isDeposit;
    int preSavedLotIdOnApi;
    int optionType;
    /***
     * DATA STORAGE STUFF
     * */
    LotViewModel mLotViewModel;
    BagViewModel mBagViewModel;
    SkuViewModel mSkuModel;
    List<Bag> mCollectedBagsList;
    CollectedBagsAdapter mCollectedBagsAdapter;
    ArrayList<String> listOfAllScannedQrCodes = new ArrayList<>();
    String preFetchedLotList;

    boolean isFirstBagScanned = false;


    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {


            if (result.getText() != null || lastText.equals(result.getText())) {
                // Prevent duplicate scans
                Log.e("BArcode", result.getText());
                Collections.sort(listOfAllScannedQrCodes);
                int index = Collections.binarySearch(listOfAllScannedQrCodes, result.getText());
                if (index > -1) {
                    //allready scanned qrCode
                    Logger.toast(WeightCaptureActivity.this, "Allready scanned.");

                } else {
                    //add qr code to list

                    try {

                        if (enableWeightScan) {
//                            if (mActivityWeightCaptureBinding.getInputData().getWeightCaptured().contains("U") || mActivityWeightCaptureBinding.getInputData().getWeightCaptured().length() < 10) {
                            if (mActivityWeightCaptureBinding.getInputData().getWeightCaptured().length() < 7) {
                                Logger.toast(WeightCaptureActivity.this, "Stablize weight first.");
                            } else {
                                pause();

                                Double valueForSetting=0.0d;
                                if(mActivityWeightCaptureBinding.getInputData().getWeightCaptured().length()>7){
                                    valueForSetting=Double.parseDouble(mActivityWeightCaptureBinding.getInputData().getWeightCaptured().substring(4, 11));
                                }
                                else{
                                     valueForSetting=Double.parseDouble(mActivityWeightCaptureBinding.getInputData().getWeightCaptured());

                                }
                                saveToData(result.getText(),valueForSetting);
                                barcodeView.setStatusText(result.getText());
                                beepManager.playBeepSoundAndVibrate();
                            }
                        } else {
                            pause();
                            beepManager.playBeepSoundAndVibrate();
                            barcodeView.setStatusText(result.getText());
//                            saveToData(result.getText(), 0d);
                            openWeightInputDialog(result.getText(), 0D);
                        }
                    } catch (Exception e) {
                        Logger.toast(WeightCaptureActivity.this, "Please connect to weight capture device.");
                    }
                }
            } else {

            }
            lastText = result.getText();

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };
    private ValueAnimator anim;
    private List<Sku> mAllSkusList = new ArrayList<>();
    private boolean enableWeightScan;
    private boolean isHCF;
    private String previousValue="";
    private BluetoothSocket mBluetoothSocket;

    public void assignViews() {
        mToolbar = findViewById(R.id.toolBar);
        mToolbarIcon = findViewById(R.id.toolbarRightIcon);
        mBarCodeFlipper = findViewById(R.id.barcodeFlipper);
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mBagsRecView = findViewById(R.id.bagsRecView);
        animationFlipIn = AnimationUtils.loadAnimation(this, R.anim.flip_in);
        animationFlipOut = AnimationUtils.loadAnimation(this, R.anim.flip_out);
        mBarCodeFlipper.setInAnimation(animationFlipIn);
        mBarCodeFlipper.setOutAnimation(animationFlipOut);
        mToolbar = findViewById(R.id.toolBar);
        mToolbarIcon = findViewById(R.id.toolbarRightIcon);
        mBarCodeFlipper = findViewById(R.id.barcodeFlipper);
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mBagsRecView = findViewById(R.id.bagsRecView);
        animationFlipIn = AnimationUtils.loadAnimation(this, R.anim.flip_in);
        animationFlipOut = AnimationUtils.loadAnimation(this, R.anim.flip_out);
        mBarCodeFlipper.setInAnimation(animationFlipIn);
        mBarCodeFlipper.setOutAnimation(animationFlipOut);


        mCollectedBagsList = new ArrayList<>();
        GridLayoutManager manager = new GridLayoutManager(this, 1);
        mBagsRecView.setLayoutManager(manager);


        if (ContextCompat.checkSelfPermission(WeightCaptureActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        } else {
            instantiateBarCodeView();
        }
    }

    public void instantiateBarCodeView() {
        Log.e("BARCODE VIEW", "Instantiated");
        barcodeView = (DecoratedBarcodeView) findViewById(R.id.barcode_scanner);
        beepManager = new BeepManager(this);
        barcodeView.decodeContinuous(callback);
        barcodeView.resume();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    instantiateBarCodeView();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                }
                return;
        }
    }

    long lotId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityWeightCaptureBinding = DataBindingUtil.setContentView(this, R.layout.activity_weight_capture);
//        mDeviceData = new Gson().fromJson(getIntent().getStringExtra("data"), Device.class);
        mDeviceData = (Device) getIntent().getExtras().getParcelable("data");
//        selctedHospitalName = getIntent().getStringExtra("hospitalName");
        selctedHospitalid = getIntent().getIntExtra("hospitalId", 0);
        optionType = getIntent().getIntExtra("optionType", 0);
        isDeposit = getIntent().getBooleanExtra("isDeposit", false);
        enableWeightScan = getIntent().getBooleanExtra("enableWeightScan", false);
        isHCF = getIntent().getBooleanExtra("isHCF", false);

//        if (isDeposit) {
//            preSavedLotIdOnApi = getIntent().getIntExtra("preSavedLotIdOnApi", 0);
//        }
        assignViews();

        mLotViewModel = ViewModelProviders.of(this).get(LotViewModel.class);
        mBagViewModel = ViewModelProviders.of(this).get(BagViewModel.class);
        mSkuModel = ViewModelProviders.of(this).get(SkuViewModel.class);

        mLotViewModel.addALot(new Lot(0, 0d), mLotIDObserver);
        mSkuModel.getAllSku().observe(this, new Observer<List<Sku>>() {
            @Override
            public void onChanged(@Nullable List<Sku> skus) {
                mAllSkusList.addAll(skus);
            }
        });

        mToolbarTitle = mToolbar.findViewById(R.id.toolbarTitle);
        mToolbarTitle.setText("Capturing Weight");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mWeightCaptureActPresenter = new WeightCaptureActPresenter(this);
        mActivityWeightCaptureBinding.setPresenter(mWeightCaptureActPresenter);

        mCollectedBagsAdapter = new CollectedBagsAdapter(mCollectedBagsList, mWeightCaptureActPresenter, false);
        mCollectedBagsAdapter.setHasStableIds(true);
        mBagsRecView.setHasFixedSize(true);
        mBagsRecView.setAdapter(mCollectedBagsAdapter);

        if (enableWeightScan) {
            weightCaptureViewModel = new WeightCaptureViewModel();
            weightCaptureViewModel.setDeviceName(mDeviceData.getDeviceName());
            weightCaptureViewModel.setConnectionStatus("Connecting To : ");
            BluetoothAdapter.getDefaultAdapter();

            mActivityWeightCaptureBinding.setInputData(weightCaptureViewModel);


            mConnectThread = new ConnectThread(mDeviceData.getmBluetoothDevice(), mDeviceData.getUuid(), BluetoothAdapter.getDefaultAdapter(), this);
            mConnectThread.start();

            anim = new ValueAnimator();
            anim.setIntValues(ContextCompat.getColor(this, R.color.white), ContextCompat.getColor(this, R.color.transparent));
            anim.setEvaluator(new ArgbEvaluator());
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    mToolbarIcon.setColorFilter((Integer) valueAnimator.getAnimatedValue());
                }
            });
            anim.setDuration(300);
            anim.setRepeatMode(ValueAnimator.RESTART);
            anim.setRepeatCount(1000);
            anim.start();
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mToolbarIcon.setColorFilter(ContextCompat.getColor(WeightCaptureActivity.this, R.color.transparent));
                }
            });

            //to listen to incoming dat ffrom device
            mDataHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if (msg.what == handlerState) {
                        String readMessage = (String) msg.obj;
                        recDataString.append(readMessage);

                        int endOfLineIndex = recDataString.indexOf("g");
                        endOfLineIndex += 1;
                        if (endOfLineIndex > 0) {
                            String dataInPrint = recDataString.substring(0, endOfLineIndex);
//                            Normalizer.normalize(dataInPrint.trim(), Normalizer.Form.valueOf("UTF-8"))
                            mActivityWeightCaptureBinding.getInputData().setWeightCaptured(dataInPrint.trim());
                            Log.e("Weight", dataInPrint);
                            //clear all string data
                            recDataString.delete(0, recDataString.length());
                            dataInPrint = " ";

                        } else {
                            try {
                                if (recDataString.toString().contains("\n")) {



                                    String value = recDataString.subSequence(0, 8).toString().replace("", "");

                                    mActivityWeightCaptureBinding.getInputData().setWeightCaptured(value.trim());



                                    if(!TextUtils.isEmpty(previousValue)){

                                        if(previousValue.equals(value.trim())){

                                            mActivityWeightCaptureBinding.setIsStableValue(true);

                                            mActivityWeightCaptureBinding.executePendingBindings();

                                        }
                                        else{
                                            mActivityWeightCaptureBinding.setIsStableValue(false);
                                            mActivityWeightCaptureBinding.executePendingBindings();


                                        }




                                    }


                                    previousValue=new String(value.trim());
                                    recDataString.delete(0, recDataString.length());

                                }

                            }
                            catch (Exception exception ){
                                exception.printStackTrace();

                            }

//
//                           String[]arr= weightInString.split("\r\n" );
//
//                            if (arr.length!=0) {
//                                String dataInPrint = arr[0];
////                            Normalizer.normalize(dataInPrint.trim(), Normalizer.Form.valueOf("UTF-8"))
//                                mActivityWeightCaptureBinding.getInputData().setWeightCaptured(dataInPrint.trim());
//                                Log.e("Weight", dataInPrint);
//                                //clear all string data
//                                recDataString.delete(0, recDataString.length());
//                                dataInPrint = " ";
//
//                            }


                        }
                    }
                }
            };
        } else {
            mToolbarIcon.setVisibility(View.INVISIBLE);
        }

//        checkForBarCodeValidity("19-2-5ac756a8620286731");
        mLotViewModel.getAllLots().observe(this, lotsListObserver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                backAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 55) {
                Log.e("REQUEST CODE", "" + requestCode);
                clearAllThreads();
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onDeviceConnected(BluetoothSocket mBluetoothSocket) {
//        mActivityWeightCaptureBinding.getInputData().setConnectionStatus("Connected To : ");
        Log.e("BLUETOOTH CONNECTED", "" + mBluetoothSocket.isConnected());
        runOnUiThread(() -> {
            anim.end();
        });

        this.mBluetoothSocket=mBluetoothSocket;
        mDataTransferThread = new BTDataTransferThread(mBluetoothSocket, this);
        mDataTransferThread.start();

    }

    @Override
    public void dataReceivedUponConnection(String dataReceived, int bytes) {
//        Log.e("DATA",dataReceived);
        mDataHandler.obtainMessage(handlerState, bytes, -1, dataReceived).sendToTarget();
    }

    public void clearAllThreads() {
        if (mDataTransferThread != null) {
            Log.e("CLOSING WINDOW", "" + mDataTransferThread.isAlive());
            mDataHandler.removeMessages(0);
            mDataTransferThread.interrupt();
            mDataTransferThread.cancel();
        }

        if (mConnectThread != null) {
            mConnectThread.interrupt();
            mConnectThread.cancel();
            Log.e("CLOSING CONNECTION", "" + mConnectThread.isAlive());
        }
    }

    @Override
    public void onBackPressed() {

        clearAllThreads();
        mLotViewModel.getLotByID(lotId).observe(this, new Observer<Lot>() {
            @Override
            public void onChanged(@Nullable Lot lot) {
                if (lot != null) {
                    mLotViewModel.deleteLot(lot);
                }
            }
        });
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (barcodeView != null && inputWeightDialog == null) {
            barcodeView.resume();
        }
        try {
            if (mDataTransferThread != null) {
                if (!mDataTransferThread.isAlive()) {

                    if(mBluetoothSocket!=null &&mBluetoothSocket.isConnected()){
                        mDataTransferThread = new BTDataTransferThread(mBluetoothSocket, this);
                        mDataTransferThread.start();
                    }

                }
            }
        }
        catch (Exception exception){
            exception.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (barcodeView != null) {
            barcodeView.pause();
        }
        if (mDataTransferThread != null) {
            if (mDataTransferThread.isAlive()) {
                mDataTransferThread.interrupt();
            }
        }
    }

    /**
     * BAR CODE RELATED METHODS
     **/
    public void pause() {
        barcodeView.pause();
    }

    public void resume() {
        barcodeView.resume();
    }

    public void triggerScan(View view) {
        barcodeView.decodeSingle(callback);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }


    /**
     * UI METHODS
     */
    @Override
    public void scanAnother() {
        resume();
        mBarCodeFlipper.setDisplayedChild(0);
    }

    @Override
    public void onNextPressed() {
        Intent in = new Intent(WeightCaptureActivity.this, ViewLotActivity.class);
        in.putExtra("lot_id", lotId);
        in.putExtra("hospitalId", selctedHospitalid);
        in.putExtra("isDeposit", isDeposit);
        in.putExtra("optionType", optionType);
        if (preSavedLotIdOnApi != 0) {
            in.putExtra("preSavedLotIdOnApi", preSavedLotIdOnApi);
            in.putExtra("preSavedLots", preFetchedLotList);
        }
        navigateToNextActivityForResult(in, false, 55);
    }

    @Override
    public void setLotDetails(List<HCFLotResponse.Data> data, HCFLotResponse arg0, String firstScannedBarcode) {
        if (!data.isEmpty()) {
            preSavedLotIdOnApi = data.get(0).getLotId();
            preFetchedLotList = new Gson().toJson(arg0);
            Logger.showDialogAndPerformOnClickustomText(this, "Success", "Saved lots fetched successfully. Do you want to add them to the list", (di, i) -> {
                addPreScannedBagsToListView(data, firstScannedBarcode);
            }, true, (di, i) -> {
                di.dismiss();
            }, "Ok", "No");
        }
        isFirstBagScanned = true;
    }

    private void addPreScannedBagsToListView(List<HCFLotResponse.Data> responseData, String firstScannedBarcode) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(this);
        for (HCFLotResponse.Lotdetails lotDetails : responseData.get(0).getLotdetails()) {
            //to avoid duplicacy of first bag following condition
            if (!lotDetails.getScanCode().equals(firstScannedBarcode)) {
                mBagViewModel.addNewBag(new Bag(lotId, Double.parseDouble(lotDetails.getWeight()), lotDetails.getScanCode(), lotDetails.getSkuId()), isBagAddedObserver);
            }
        }
        mProgressDialog.dismiss();
    }

    @Override
    public void deleteBagAtPosition(int position) {
        try {
            int index = Collections.binarySearch(listOfAllScannedQrCodes, mCollectedBagsAdapter.getBagAtPosition(position).getBarCode());
            listOfAllScannedQrCodes.remove(index);
            mBagViewModel.deleteBag(mCollectedBagsList.get(position));
        } catch (Exception e) {
            Log.e("EXCEPTION AY DELETE", e.getLocalizedMessage());
        }
    }

    public void saveToData(String barcode, Double mWeight) {
        if (checkForBarCodeValidity(barcode)) {
            String[] barcodeParts = barcode.split("-");
            String hcfId = barcodeParts[0].trim();
            String skuId = barcodeParts[1].trim();
            int index = Collections.binarySearch(mAllSkusList, Integer.valueOf(skuId));
            Log.e("INDEX", "" + index);
            Log.e("SLECTED HCF ID", "" + selctedHospitalid);
            Log.e("Exctracted iD", hcfId);
            if (!isFirstBagScanned && !isHCF) {
//                mWeightCaptureActPresenter.fetchHcfLotList(Integer.parseInt(hcfId), barcode);
                isFirstBagScanned = true;
                selctedHospitalid = Integer.parseInt(hcfId);
            }

//            if (hcfId.equals("" + selctedHospitalid) && index > -1 && optionType == Constant.PICKUP && isDeposit) {
//                //FOR PICKUP ONLY
//                listOfAllScannedQrCodes.add(barcode);
//                mBagViewModel.addNewBag(new Bag(lotId, mWeight, barcode, Integer.parseInt(skuId)), isBagAddedObserver);
//            } else
            if (index > -1 && optionType == Constant.PICKUP && isDeposit) {
                //FOR PICKUP ONLY without lot
                listOfAllScannedQrCodes.add(barcode);
                mBagViewModel.addNewBag(new Bag(lotId, mWeight, barcode, Integer.parseInt(skuId)), isBagAddedObserver);
            } else if (index > -1 && optionType == Constant.DEPOSIT && !isDeposit) {
                //FOR DEPOSIT ONLY
                listOfAllScannedQrCodes.add(barcode);
                mBagViewModel.addNewBag(new Bag(lotId, mWeight, barcode, Integer.parseInt(skuId)), isBagAddedObserver);
            } else if (hcfId.equals("" + selctedHospitalid) && index > -1 && !isDeposit) {
//                FOR HCF ONLY
                listOfAllScannedQrCodes.add(barcode);
                mBagViewModel.addNewBag(new Bag(lotId, mWeight, barcode, Integer.parseInt(skuId)), isBagAddedObserver);
            } else {
                runOnUiThread(() -> {
                    resume();
                    mBarCodeFlipper.setDisplayedChild(0);
                });
                Logger.toast(this, "This barcode not belongs to you.");
            }
        } else {
            runOnUiThread(() -> {
                resume();
                mBarCodeFlipper.setDisplayedChild(0);
            });
            Logger.toast(this, "Invalid barcode.");
        }

    }

    public boolean checkForBarCodeValidity(String barcode) {
        Log.e("CHECKING BARCODE", barcode);
        if (barcode.contains("-")) {
            String[] barcodeParts = barcode.split("-");
            if (barcodeParts.length == 3) {
                Log.e("BARCODE has LEngth", "true");
                String hcfId = barcodeParts[0].trim();
                String skuId = barcodeParts[1].trim();
//                if (skuId.length() == 1 && hcfId.length() <=5) {
//                    Log.e("VALID 1",""+(skuId.length() == 1));
//                    Log.e("VALID 2",""+(hcfId.length() <= 5));
//                    return true;
//                } else {
//
//                    Log.e("VALID 1 FALSE",""+(skuId.length() == 1));
//                    Log.e("VALID 2 FALSE",""+(hcfId.length() <= 5));
//                    return false;
//                }
                if (!TextUtils.isEmpty(skuId) && !TextUtils.isEmpty(hcfId)) {
                    Log.e("VALID 1", "" + !TextUtils.isEmpty(skuId));
                    Log.e("VALID 2", "" + !TextUtils.isEmpty(hcfId));
                    return true;
                } else {

                    Log.e("VALID 1", "" + !TextUtils.isEmpty(skuId));
                    Log.e("VALID 2", "" + !TextUtils.isEmpty(hcfId));
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }


    @Override
    public Activity provideContext() {
        return this;
    }


    /**
     * LOT ID OBSERVER TO ENSURE THAT LOT HAS BEEN CREATED AND AFTER THAT USER
     * CAN NOW ADD OR DELETE BAGS TO THAT LOT
     **/

    io.reactivex.Observer<Long> mLotIDObserver = new io.reactivex.Observer<Long>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Long aLong) {
            lotId = aLong;
            Log.e("LOT ID CAPTURED", "" + lotId);
            enableObservableForBagsAddedToList();
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    };


    /**
     * OBSERVER TO OBSERVE WHETHER THE BAG IS ADDED TO THE DB
     **/

    io.reactivex.Observer<Long> isBagAddedObserver = new io.reactivex.Observer<Long>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Long aLong) {

            runOnUiThread(() -> {
                resume();
                mBarCodeFlipper.setDisplayedChild(0);
            });
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    };


    /**
     * OBSERVER TO OBSERVE ANY UPDATES WHILE UPDATING LOT DATA
     */
    io.reactivex.Observer<Integer> updateLotDetails = new io.reactivex.Observer<Integer>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Integer integer) {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    };


    /**
     * OBSERVER TO OBSERVE ANY CHANGE LIKE ADDITION IN BAGS LIST
     * AND UPDATE THE CUMULATIVE DATA LIKE TOTAL WEIGHT AND BAGS
     * TO THE LOTS DATA IN LOT TABLE
     */
    Observer<List<Bag>> bagsListObserver = new Observer<List<Bag>>() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onChanged(@Nullable List<Bag> bags) {
            Log.e("BAGS LIST OBS", new Gson().toJson(bags));
            mCollectedBagsList.clear();
            mCollectedBagsAdapter.notifyDataSetChanged();
            mCollectedBagsList.addAll(bags);
            mCollectedBagsAdapter.notifyDataSetChanged();
            /**UPDATE LOT DETAIL*/
            double allWeightSum = bags.stream().collect(Collectors.summingDouble(Bag::getWeight));
            Lot updatedLot = new Lot(0, 0d);
            updatedLot.setLotId(lotId);
            updatedLot.setTotalBags(bags.size());
            updatedLot.setTotalWeight(allWeightSum);
            mLotViewModel.updateLotDetails(updatedLot, updateLotDetails);
            if (bags.size() == 0) {
                isFirstBagScanned = false;
            }
        }
    };

    /**
     * THIS OBSERVER WILL KEEP TRACK OF ALL LOTS AND CHECK THAT A RELEVANT LOT IS BEING UPDATED
     */
    Observer<List<Lot>> lotsListObserver = new Observer<List<Lot>>() {
        @Override
        public void onChanged(@Nullable List<Lot> lots) {
            Log.e("LOT OBSERVER", new Gson().toJson(lots));

        }
    };

    /**
     * THIS METHOD WILL INITILISE OBSERVER ON BAGS LIST THAT BELONG TO PARTICULAR LOT ID
     **/
    public void enableObservableForBagsAddedToList() {
        Log.e("ENABLED READ WRITE", "TRUE");
        mBagViewModel.getAllBagsThatBelongToLotWithId(lotId).observe(this, bagsListObserver);
    }

    /**
     * THIS METHOD TO INFLATE CUSTOM DIALOG AND TAKE USER INPUT
     */
    private AlertDialog inputWeightDialog;


    public void openWeightInputDialog(String text, double weight) {
        View alertLayout = LayoutInflater.from(this).inflate(R.layout.custom_alert_input_weight, null);
        AppTextView cancelButton = alertLayout.findViewById(R.id.cancelButton);
        AppTextView saveButton = alertLayout.findViewById(R.id.refreshButton);
        AppEditText weightET = alertLayout.findViewById(R.id.weightET);

        AlertDialog.Builder nearbyDevicesAlertDialogBuilder = new AlertDialog.Builder(this);
        nearbyDevicesAlertDialogBuilder.setView(alertLayout);
        nearbyDevicesAlertDialogBuilder.setCancelable(false);

        inputWeightDialog = nearbyDevicesAlertDialogBuilder.create();
        inputWeightDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        inputWeightDialog.show();
        pause();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(inputWeightDialog.getWindow().getAttributes());

        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.5f);
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;

        // Apply the newly created layout parameters to the alert dialog window
        inputWeightDialog.getWindow().setAttributes(layoutParams);
        inputWeightDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    inputWeightDialog.dismiss();
                    runOnUiThread(() -> {
                        resume();
                        mBarCodeFlipper.setDisplayedChild(0);
                    });
                    inputWeightDialog = null;
                }
                return false;
            }
        });

        cancelButton.setOnClickListener((v) -> {
            inputWeightDialog.dismiss();
            runOnUiThread(() -> {
                resume();
                mBarCodeFlipper.setDisplayedChild(0);
            });
            inputWeightDialog = null;
        });
        saveButton.setOnClickListener((v) -> {
            if (!TextUtils.isEmpty(weightET.getText().toString())) {
                inputWeightDialog.dismiss();
                saveToData(text, Double.parseDouble(weightET.getText().toString()));
                inputWeightDialog = null;
            } else {
                Logger.toast(this, "Please input weight.");
            }
        });


    }
}
