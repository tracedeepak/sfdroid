package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemPlanBinding;
import nansat.com.safebio.models.AvailablePlansResponse;
import nansat.com.safebio.presenter.AvailablePlansPresenter;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class AvailablePlansAdapter extends RecyclerView.Adapter<AvailablePlansAdapter.AvailablePlansViewHolder> {
    List<AvailablePlansResponse.Data> mAvailPlanList;
    AvailablePlansPresenter mAvailablePlansPresenter;

    public AvailablePlansAdapter(List<AvailablePlansResponse.Data> mAvailPlanList, AvailablePlansPresenter mAvailablePlansPresenter) {
        this.mAvailPlanList = mAvailPlanList;
        this.mAvailablePlansPresenter = mAvailablePlansPresenter;
    }

    @Override
    public AvailablePlansViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPlanBinding mItemPlanBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_plan, parent, false);
        return new AvailablePlansViewHolder(mItemPlanBinding);
    }

    @Override
    public void onBindViewHolder(AvailablePlansViewHolder holder, int position) {
        holder.bindData(mAvailPlanList.get(position), mAvailablePlansPresenter);
    }

    @Override
    public int getItemCount() {
        return mAvailPlanList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class AvailablePlansViewHolder extends RecyclerView.ViewHolder {

        ItemPlanBinding mItemPlanBinding;

        public AvailablePlansViewHolder(ItemPlanBinding mItemPlanBinding) {
            super(mItemPlanBinding.getRoot());
            this.mItemPlanBinding = mItemPlanBinding;
        }

        public void bindData(AvailablePlansResponse.Data mData, AvailablePlansPresenter mAvailablePlansPresenter) {
            mItemPlanBinding.setData(mData);
            mItemPlanBinding.setPresenter(mAvailablePlansPresenter);
        }

    }
}
