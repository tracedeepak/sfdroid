package nansat.com.safebio.adapters;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemCategoryBinding;
import nansat.com.safebio.models.ProductCategoryResponse;
import nansat.com.safebio.widgets.AppImageView;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>{
    List<ProductCategoryResponse.Data> mCategoryList;

    public CategoriesAdapter(List<ProductCategoryResponse.Data> mCategoryList) {
        this.mCategoryList = mCategoryList;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public ProductCategoryResponse.Data getItemAtPosition(int pos){
        return mCategoryList.get(pos);
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCategoryBinding mItemCategoryBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_category,parent,false);
        return new CategoriesViewHolder(mItemCategoryBinding);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, int position) {
        holder.bindData(mCategoryList.get(position));
    }

    @BindingAdapter("android:src")
    public static void setImageUrl(AppImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            url.replaceAll("\'","");
            Log.e("URLS",url);
            Picasso.with(view.getContext()).load(url).into(view);
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder{
        ItemCategoryBinding mItemCategoryBinding;
        public CategoriesViewHolder(ItemCategoryBinding mItemCategoryBinding) {
            super(mItemCategoryBinding.getRoot());
            this.mItemCategoryBinding=mItemCategoryBinding;
        }


        public void bindData(ProductCategoryResponse.Data data) {
            mItemCategoryBinding.setData(data);
        }
    }
}
