package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.database.models.Bag;
import nansat.com.safebio.databinding.BagItemBinding;
import nansat.com.safebio.presenter.WeightCaptureActPresenter;

/**
 * Created by Phantasmist on 14/02/18.
 */

public class CollectedBagsAdapter extends RecyclerView.Adapter<CollectedBagsAdapter.CollectedBagViewHolder> {

    List<Bag> mBagList;
    WeightCaptureActPresenter mWeightCaptureActPresenter;
    boolean hideDeleteButton=false;
    public CollectedBagsAdapter(List<Bag> mBagList, WeightCaptureActPresenter mWeightCaptureActPresenter, boolean hideDeleteButton) {
        this.mBagList = mBagList;
        this.mWeightCaptureActPresenter=mWeightCaptureActPresenter;
        this.hideDeleteButton=hideDeleteButton;
    }

    @Override
    public CollectedBagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BagItemBinding mBagItemBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.bag_item,parent,false);
        return new CollectedBagViewHolder(mBagItemBinding);
    }

    @Override
    public void onBindViewHolder(CollectedBagsAdapter.CollectedBagViewHolder holder, int position) {
        holder.bindData(mBagList.get(position),position,mWeightCaptureActPresenter);
    }

    @Override
    public long getItemId(int position) {
        return mBagList.get(position).getBagId();
    }

    public Bag getBagAtPosition(int pos){
        return mBagList.get(pos);
    }

    @Override
    public int getItemCount() {
        return mBagList.size();
    }

    class CollectedBagViewHolder extends RecyclerView.ViewHolder{
        BagItemBinding mBagItemBinding;

        public CollectedBagViewHolder(BagItemBinding mBagItemBinding) {
            super(mBagItemBinding.getRoot());
            this.mBagItemBinding=mBagItemBinding;
        }

        public void bindData(Bag mBag, int position, WeightCaptureActPresenter presenter){
            mBagItemBinding.setBagData(mBag);
            mBagItemBinding.setPosition(position);
            mBagItemBinding.setPresenter(presenter);
            mBagItemBinding.setHideDelButt(hideDeleteButton);
        }

    }
}
