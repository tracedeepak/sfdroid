package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemDeviceBinding;
import nansat.com.safebio.models.Device;

/**
 * Created by Phantasmist on 09/02/18.
 */

public class DeviceRecAdapter extends RecyclerView.Adapter<DeviceRecAdapter.DeviceRecViewHolder>{

    List<Device> pairedDeviceList ;

    public DeviceRecAdapter(List<Device> pairedDeviceList) {
        this.pairedDeviceList = pairedDeviceList;
    }

    @Override
    public DeviceRecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemDeviceBinding mItemDeviceBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_device,parent,false);
        return new DeviceRecViewHolder(mItemDeviceBinding);
    }

    @Override
    public void onBindViewHolder(DeviceRecViewHolder holder, int position) {
        holder.bindData(pairedDeviceList.get(position));
    }

    @Override
    public int getItemCount() {
        return pairedDeviceList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public Device getItemAtPosition(int pos){
        return pairedDeviceList.get(pos);
    }

    public class DeviceRecViewHolder extends RecyclerView.ViewHolder{
        ItemDeviceBinding mItemDeviceBinding;
        public DeviceRecViewHolder(ItemDeviceBinding itemView) {
            super(itemView.getRoot());
            this.mItemDeviceBinding=itemView;
        }

        public  void bindData(Device data){
            mItemDeviceBinding.setData(data);
        }
    }
}
