package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemHcfBinding;
import nansat.com.safebio.models.HcfResponse;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class HCFListAdapter extends BaseAdapter implements Filterable {
    List<HcfResponse.Data> mDataList;
    List<HcfResponse.Data> mOrignalDataList;

    public HCFListAdapter(List<HcfResponse.Data> mDataList) {
        this.mDataList = mDataList;
        this.mOrignalDataList=mDataList;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public HcfResponse.Data getItem(int i) {
        return mDataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ItemHcfBinding mItemHcfBinding= DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_hcf,viewGroup,false);
        mItemHcfBinding.setData(mDataList.get(i));
        return mItemHcfBinding.getRoot();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDataList = (List<HcfResponse.Data>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<HcfResponse.Data> FilteredArrayNames = new ArrayList<HcfResponse.Data>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < mOrignalDataList.size(); i++) {
                    String dataNames = mOrignalDataList.get(i).getName();
                    if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                        FilteredArrayNames.add(mOrignalDataList.get(i));
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }
}
