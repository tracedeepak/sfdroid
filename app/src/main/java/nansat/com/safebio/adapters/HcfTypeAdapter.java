package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.SpinnerItemBinding;
import nansat.com.safebio.models.HcfTypeModel;

public class HcfTypeAdapter extends BaseAdapter {
    List<HcfTypeModel.AllHcffTypes> mStringList;

    public HcfTypeAdapter(List<HcfTypeModel.AllHcffTypes> mStringList) {
        this.mStringList = mStringList;
    }

    @Override
    public int getCount() {
        return mStringList.size();
    }

    @Override
    public HcfTypeModel.AllHcffTypes getItem(int i) {
        return mStringList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SpinnerItemBinding mSpinnerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.spinner_item, viewGroup, false);
        mSpinnerItemBinding.setStatus(mStringList.get(i).hcf_type_desc);
        return mSpinnerItemBinding.getRoot();
    }
}
