package nansat.com.safebio.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import nansat.com.safebio.fragments.CPWTFProfileFragment;

/**
 * Created by Phantasmist on 02/04/18.
 */

public class MyAccountCPWTFViewPagerAdapter extends FragmentPagerAdapter {

    String[] tabTitles = {"Profile Details"};
    Fragment mFragment;
    String apiData;
    CPWTFProfileFragment mCpwtfProfileFragment;

    public MyAccountCPWTFViewPagerAdapter(FragmentManager fm, String data) {
        super(fm);
        this.mCpwtfProfileFragment = new CPWTFProfileFragment();
        this.apiData = data;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle data = new Bundle();
        data.putString("data", apiData);
        switch (position) {
            case 0:
                mFragment = this.mCpwtfProfileFragment;
                mFragment.setArguments(data);
                break;
        }

        return mFragment;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
