package nansat.com.safebio.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import nansat.com.safebio.fragments.HCFDetailsFragment;
import nansat.com.safebio.fragments.MySubscriptionFragment;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyAccountViewPagerAdapter extends FragmentPagerAdapter {

    String[] tabTitles = {"Subscription", "Profile Details"};
    MySubscriptionFragment mySubscriptionFragment;
    HCFDetailsFragment hcfDetailsFragment;
    Fragment mFragment;
    String apiData;

    public MyAccountViewPagerAdapter(FragmentManager fm, String data) {
        super(fm);
        this.hcfDetailsFragment = new HCFDetailsFragment();
        this.mySubscriptionFragment = new MySubscriptionFragment();
        this.apiData = data;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle data = new Bundle();
        data.putString("data", apiData);
        switch (position) {
            case 0:
                mFragment = this.mySubscriptionFragment;
                mFragment.setArguments(data);
                break;
            case 1:
                mFragment = this.hcfDetailsFragment;
                mFragment.setArguments(data);
                break;
        }

        return mFragment;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
