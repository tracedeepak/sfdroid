package nansat.com.safebio.adapters;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemOrderContentsBinding;
import nansat.com.safebio.models.MyOrdersResponse;
import nansat.com.safebio.widgets.AppImageView;

/**
 * Created by Phantasmist on 26/03/18.
 */

public class MyOrderDetailsRecAdapter extends RecyclerView.Adapter<MyOrderDetailsRecAdapter.MyOrderDetails> {

    List<MyOrdersResponse.Order_details> mOrder_detailsList;

    public MyOrderDetailsRecAdapter(List<MyOrdersResponse.Order_details> mOrder_detailsList) {
        this.mOrder_detailsList = mOrder_detailsList;
    }

    @Override
    public MyOrderDetails onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderContentsBinding mItemOrderContentsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_order_contents, parent, false);

        return new MyOrderDetails(mItemOrderContentsBinding);
    }

    @Override
    public void onBindViewHolder(MyOrderDetails holder, int position) {
        holder.bindData(mOrder_detailsList.get(position));
    }

    @Override
    public int getItemCount() {
        return mOrder_detailsList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    @BindingAdapter("android:src")
    public static void setImageUrl(AppImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext()).load(url).into(view);
        }
    }

    public class MyOrderDetails extends RecyclerView.ViewHolder {

        ItemOrderContentsBinding mItemOrderContentsBinding;

        public MyOrderDetails(ItemOrderContentsBinding itemView) {
            super(itemView.getRoot());
            this.mItemOrderContentsBinding = itemView;
        }


        public void bindData(MyOrdersResponse.Order_details order_details) {
            mItemOrderContentsBinding.setOrderDetails(order_details);
            mItemOrderContentsBinding.setProductDetails(order_details.getProduct());
        }
    }


}
