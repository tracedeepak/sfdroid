package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemMyOrderBinding;
import nansat.com.safebio.models.MyOrdersResponse;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyOrderViewHolder> {

    List<MyOrdersResponse.Data> mOrderList;

    public MyOrdersAdapter(List<MyOrdersResponse.Data> mOrderList) {
        this.mOrderList = mOrderList;
    }

    @Override
    public MyOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMyOrderBinding myOrderBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_my_order,parent,false);
        return new MyOrderViewHolder(myOrderBinding);
    }

    @Override
    public void onBindViewHolder(MyOrderViewHolder holder, int position) {
        holder.bindData(mOrderList.get(position));
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }

    public MyOrdersResponse.Data getItemAtPosition(int position){
        return mOrderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class MyOrderViewHolder extends RecyclerView.ViewHolder {

        ItemMyOrderBinding myOrderBinding;
        public MyOrderViewHolder(ItemMyOrderBinding itemView) {
            super(itemView.getRoot());
            this.myOrderBinding=itemView;
        }


        public void bindData(MyOrdersResponse.Data data) {
            myOrderBinding.setData(data);
        }
    }
}
