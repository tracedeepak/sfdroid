package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemOrderBinding;
import nansat.com.safebio.models.ProductsResponse;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class OrderSummaryRecAdapter extends RecyclerView.Adapter<OrderSummaryRecAdapter.OrderSummaryViewHolder> {
    List<ProductsResponse.Data> mProductList;

    public OrderSummaryRecAdapter(List<ProductsResponse.Data> mProductList) {
        this.mProductList = mProductList;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public ProductsResponse.Data getItemAtPosition(int pos) {
        return mProductList.get(pos);
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }


    @Override
    public OrderSummaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderBinding mItemOrderBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_order, parent, false);
        return new OrderSummaryViewHolder(mItemOrderBinding);
    }

    @Override
    public void onBindViewHolder(OrderSummaryViewHolder holder, int position) {
        holder.bindData(mProductList.get(position), position);
    }


    public class OrderSummaryViewHolder extends RecyclerView.ViewHolder {

        ItemOrderBinding mItemOrderBinding;

        public OrderSummaryViewHolder(ItemOrderBinding mItemOrderBinding) {
            super(mItemOrderBinding.getRoot());
            this.mItemOrderBinding = mItemOrderBinding;
        }

        public void bindData(ProductsResponse.Data data, int position) {
            mItemOrderBinding.setData(data);
            mItemOrderBinding.setPosition(position + 1);
        }
    }
}
