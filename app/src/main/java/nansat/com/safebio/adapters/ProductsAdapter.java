package nansat.com.safebio.adapters;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemProductBinding;
import nansat.com.safebio.models.ProductsResponse;
import nansat.com.safebio.presenter.BrowseProductsActPresenter;
import nansat.com.safebio.widgets.AppImageView;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    List<ProductsResponse.Data> mProductList;
    BrowseProductsActPresenter mBrowseProductsActPresenter;

    public ProductsAdapter(List<ProductsResponse.Data> mProductList, BrowseProductsActPresenter mBrowseProductsActPresenter) {
        this.mProductList = mProductList;
        this.mBrowseProductsActPresenter = mBrowseProductsActPresenter;
    }

    @BindingAdapter("android:src")
    public static void setImageUrl(AppImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            url.replaceAll("\'", "");
            Log.e("URLS", url);
            Picasso.with(view.getContext()).load(url).into(view);
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public ProductsResponse.Data getItemAtPosition(int pos) {
        return mProductList.get(pos);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductBinding mItemProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product, parent, false);
        return new ProductsViewHolder(mItemProductBinding);
    }

    @Override
    public void onBindViewHolder(ProductsViewHolder holder, int position) {
        holder.bindData(mProductList.get(position), mBrowseProductsActPresenter, position);
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder {
        ItemProductBinding mItemProductBinding;

        public ProductsViewHolder(ItemProductBinding mItemProductBinding) {
            super(mItemProductBinding.getRoot());
            this.mItemProductBinding = mItemProductBinding;
        }

        public void bindData(ProductsResponse.Data data, BrowseProductsActPresenter mBrowseProductsActPresenter, int position) {
            data.setQuantity(data.getMin_order_qty());
            mItemProductBinding.setData(data);
            mItemProductBinding.setPresenter(mBrowseProductsActPresenter);
            mItemProductBinding.setPosition(position);
//            mItemProductBinding.setAddToCart(false);
        }
    }
}
