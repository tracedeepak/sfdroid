package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemReportBinding;
import nansat.com.safebio.models.ReportsResponse;

/**
 * Created by Phantasmist on 02/04/18.
 */

public class ReportsRecAdapter extends RecyclerView.Adapter<ReportsRecAdapter.ReportsViewHolder> {
    List<ReportsResponse.Data> mDataList;

    public ReportsRecAdapter(List<ReportsResponse.Data> mDataList) {
        this.mDataList = mDataList;
    }

    @Override
    public ReportsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemReportBinding mItemReportBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_report,parent,false);

        return new ReportsViewHolder(mItemReportBinding);
    }

    @Override
    public void onBindViewHolder(ReportsViewHolder holder, int position) {
        holder.bindData(mDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ReportsViewHolder extends RecyclerView.ViewHolder {

        ItemReportBinding mItemReportBinding;
        public ReportsViewHolder(ItemReportBinding itemView) {
            super(itemView.getRoot());
            this.mItemReportBinding=itemView;
        }


        public void bindData(ReportsResponse.Data data) {
            mItemReportBinding.setData(data);
        }
    }
}
