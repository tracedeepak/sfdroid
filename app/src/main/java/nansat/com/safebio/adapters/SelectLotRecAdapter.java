package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.ItemLotBinding;
import nansat.com.safebio.models.HCFLotResponse;

/**
 * Created by Phantasmist on 19/02/18.
 */

public class SelectLotRecAdapter extends RecyclerView.Adapter<SelectLotRecAdapter.SelectLotViewHolder>{

    List<HCFLotResponse.Data> mLotList;

    public SelectLotRecAdapter(List<HCFLotResponse.Data> mLotList) {
        this.mLotList = mLotList;
    }

    @Override
    public SelectLotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemLotBinding mItemLotBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_lot,parent,false);
        return new SelectLotViewHolder(mItemLotBinding);
    }

    @Override
    public void onBindViewHolder(SelectLotViewHolder holder, int position) {
        holder.bindData(mLotList.get(position));
    }

    @Override
    public int getItemCount() {
        return mLotList.size();
    }

    public HCFLotResponse.Data getItemAtPosition(int position){
        return mLotList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class SelectLotViewHolder extends RecyclerView.ViewHolder{

        ItemLotBinding mItemLotBinding;
        public SelectLotViewHolder(ItemLotBinding mItemLotBinding) {
            super(mItemLotBinding.getRoot());
            this.mItemLotBinding=mItemLotBinding;
        }

        void bindData(HCFLotResponse.Data mData){
            mItemLotBinding.setData(mData);
        }
    }
}
