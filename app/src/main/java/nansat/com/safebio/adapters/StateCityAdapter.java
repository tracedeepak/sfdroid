package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.SpinnerItemBinding;

/**
 * Created by Phantasmist on 11/05/18.
 */

public class StateCityAdapter extends BaseAdapter {
    List<String> mStringList;

    public StateCityAdapter(List<String> mStringList) {
        this.mStringList = mStringList;
    }

    @Override
    public int getCount() {
        return mStringList.size();
    }

    @Override
    public String getItem(int i) {
        return mStringList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SpinnerItemBinding mSpinnerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.spinner_item, viewGroup, false);
        mSpinnerItemBinding.setStatus(mStringList.get(i));
        return mSpinnerItemBinding.getRoot();
    }
}
