package nansat.com.safebio.adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.SpinnerItemBinding;

/**
 * Created by Phantasmist on 28/03/18.
 */

public class StatusSpinnerAdapter extends BaseAdapter {
    String[] statusList = {"Picked", "Partially Received", "Fully Received"};

    public StatusSpinnerAdapter() {
    }

    @Override
    public int getCount() {
        return statusList.length;
    }

    @Override
    public String getItem(int i) {
        return statusList[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SpinnerItemBinding mSpinnerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.spinner_item, viewGroup, false);
        mSpinnerItemBinding.setStatus(statusList[i]);
        return mSpinnerItemBinding.getRoot();
    }
}
