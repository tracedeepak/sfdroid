package nansat.com.safebio.contracts;

import java.util.List;

import nansat.com.safebio.models.AvailablePlansResponse;
import nansat.com.safebio.models.SubscribePlanRequest;

/**
 * Created by Phantasmist on 08/03/18.
 */

public interface AvailablePlansActContract extends CommonInterface{
    void inflateAvailablePlansToGrid(List<AvailablePlansResponse.Data> data);

    void proceedToPaymentOptions(AvailablePlansResponse.Data mData);

    boolean validateReferenceNum();

    SubscribePlanRequest prepareSubscribePlanRequest();

    void startNextActivity();

    void callInstaMojoPay();
}
