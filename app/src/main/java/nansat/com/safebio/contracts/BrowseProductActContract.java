package nansat.com.safebio.contracts;

import java.util.List;

import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.ProductCategoryResponse;
import nansat.com.safebio.models.ProductsResponse;

/**
 * Created by Phantasmist on 27/02/18.
 */

public interface BrowseProductActContract extends CommonInterface {
    void populateCategoriesRecView(List<ProductCategoryResponse.Data> data);

    void populateProductsRecView(List<ProductsResponse.Data> data);

    void incrementValByFactor(int val, int factor, int position, int min_order);

    void decrementValByFactor(int val, int factor, int position, int min_order);

    void callInstamojoSDK(String orderId);

    void changeCartValueOfProduct(int position,boolean addToCart);

    void inflateStatesData(LocationDTO arg0);
}
