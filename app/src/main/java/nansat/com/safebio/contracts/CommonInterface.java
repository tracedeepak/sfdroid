package nansat.com.safebio.contracts;

import android.app.Activity;

/**
 * Created by Phantasmist on 10/02/18.
 */

public interface CommonInterface {
    Activity provideContext();
}
