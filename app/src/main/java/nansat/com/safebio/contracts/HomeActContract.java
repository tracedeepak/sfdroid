package nansat.com.safebio.contracts;

import java.util.List;

import nansat.com.safebio.models.SKUResponse;

/**
 * Created by Phantasmist on 17/02/18.
 */

public interface HomeActContract extends CommonInterface{
    void saveSKUsToDb(List<SKUResponse.Data> data);
}
