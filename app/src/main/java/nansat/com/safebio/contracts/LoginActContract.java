package nansat.com.safebio.contracts;

/**
 * Created by Phantasmist on 17/02/18.
 */

public interface LoginActContract extends CommonInterface {

    void startNextActivity(boolean hasSubscribed, Boolean active);

    void launchRegisterHCF();
}
