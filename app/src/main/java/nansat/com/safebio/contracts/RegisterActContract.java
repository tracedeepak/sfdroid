package nansat.com.safebio.contracts;

import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.RegisterHCFRequest;

/**
 * Created by Phantasmist on 07/03/18.
 */

public interface RegisterActContract extends CommonInterface{
    boolean checkValidation();

    RegisterHCFRequest prepareRequest();

    void startNextActivity();

    void inflateStatesData(LocationDTO arg0);
}
