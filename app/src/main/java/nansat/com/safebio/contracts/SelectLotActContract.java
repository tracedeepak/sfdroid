package nansat.com.safebio.contracts;

import java.util.List;

import nansat.com.safebio.models.HCFLotResponse;

/**
 * Created by Phantasmist on 19/02/18.
 */

public interface SelectLotActContract extends CommonInterface{

    void addLotstoRecyclerView(List<HCFLotResponse.Data> data);
}
