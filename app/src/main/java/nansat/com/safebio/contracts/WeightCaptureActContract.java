package nansat.com.safebio.contracts;

import java.util.List;

import nansat.com.safebio.models.HCFLotResponse;

/**
 * Created by Phantasmist on 13/02/18.
 */

public interface WeightCaptureActContract extends CommonInterface {
    void scanAnother();



    void deleteBagAtPosition(int position);

    void onNextPressed();

    void setLotDetails(List<HCFLotResponse.Data> data, HCFLotResponse arg0, String firstScannedBarcode);
}
