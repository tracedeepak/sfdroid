package nansat.com.safebio.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import nansat.com.safebio.database.dao.BagDao;
import nansat.com.safebio.database.dao.LotDao;
import nansat.com.safebio.database.dao.SkuDao;
import nansat.com.safebio.database.models.Bag;
import nansat.com.safebio.database.models.Lot;
import nansat.com.safebio.database.models.Sku;

/**
 * Created by Phantasmist on 13/02/18.
 */

@Database(entities = {Lot.class, Bag.class, Sku.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract LotDao lotDao();

    public abstract BagDao bagDao();

    public abstract SkuDao skuDao();

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "safebio_db").fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

}
