package nansat.com.safebio.database;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nansat.com.safebio.models.OfflineRequestsDTO;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Utils;

public class LocalStorage {

    public static OfflineRequestsDTO getExistingPendingRequests(Context mContext) {
        OfflineRequestsDTO mOfflineRequestsDTO = new Gson().fromJson(Utils.getString(mContext, Constant.PENDING_REQUESTS), OfflineRequestsDTO.class);
        return mOfflineRequestsDTO;
    }


    public static void addPendingRequestsToQueue(Context mContext, String requestKey, String requestData, long timestamp) {

        if (!Utils.getString(mContext, Constant.PENDING_REQUESTS).isEmpty() && !Utils.getString(mContext, Constant.PENDING_REQUESTS).equals(null)) {
            //EXISTING REQUEST DTO
            OfflineRequestsDTO mExistingOfflineRequestsDTO = getExistingPendingRequests(mContext);
            //EXISTING PENDING REQUESTS
            List<OfflineRequestsDTO.PendingRequest> mExistingPendingRequests = mExistingOfflineRequestsDTO.getmPendingRequests();
            //CREATE A NEW REQEST
            OfflineRequestsDTO.PendingRequest mPendingRequest = new OfflineRequestsDTO().new PendingRequest();
            mPendingRequest.setRequestType(requestKey);
            mPendingRequest.setRequestData(requestData);
            mPendingRequest.setTimeStamp(timestamp);
            //ADD NEWLY CREATED REQUEST TO EXITING QUEUE
            mExistingPendingRequests.add(mPendingRequest);
            //ADD NEW QUEUE TO EXISTING OBJECT
            mExistingOfflineRequestsDTO.setmPendingRequests(mExistingPendingRequests);
            Utils.storeString(mContext, Constant.PENDING_REQUESTS, new Gson().toJson(mExistingOfflineRequestsDTO));
        } else {
            List<OfflineRequestsDTO.PendingRequest> mPendingRequestsQueue = new ArrayList<>();
            //CREATE A NEW QUEUE
            OfflineRequestsDTO.PendingRequest mPendingRequest = new OfflineRequestsDTO().new PendingRequest();
            mPendingRequest.setRequestType(requestKey);
            mPendingRequest.setRequestData(requestData);
            mPendingRequest.setTimeStamp(timestamp);
            //ADD NEWLY CREATED REQUEST TO NEW QUEUE
            mPendingRequestsQueue.add(mPendingRequest);
            //ADD NEW QUEUE TO NEW OBJECT
            OfflineRequestsDTO mOfflineRequestsDTO = new OfflineRequestsDTO();
            mOfflineRequestsDTO.setmPendingRequests(mPendingRequestsQueue);
            //STORE THE NEW QUEUE INTO STORAGE
            Utils.storeString(mContext, Constant.PENDING_REQUESTS, new Gson().toJson(mOfflineRequestsDTO));
        }
    }

    public static void updatePendingRequestStatus(Context mContext, OfflineRequestsDTO.PendingRequest mCompletedPendingRequest) {
        if (!Utils.getString(mContext, Constant.PENDING_REQUESTS).isEmpty() && !Utils.getString(mContext, Constant.PENDING_REQUESTS).equals(null)) {
            OfflineRequestsDTO mExistingOfflineRequestsDTO = getExistingPendingRequests(mContext);
            List<OfflineRequestsDTO.PendingRequest> mPendingRequests = mExistingOfflineRequestsDTO.getmPendingRequests();
            Collections.sort(mPendingRequests);
            int index = Collections.binarySearch(mPendingRequests, mCompletedPendingRequest);
            try {
                mPendingRequests.remove(index);
                mExistingOfflineRequestsDTO.setmPendingRequests(mPendingRequests);
                Utils.storeString(mContext, Constant.PENDING_REQUESTS, new Gson().toJson(mExistingOfflineRequestsDTO));
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

        }
    }

    public static void makePendingRequestsQueueEmpty(Context mContext) {
//        if (!Utils.getString(mContext, Constant.PENDING_REQUESTS).isEmpty() && Utils.getString(mContext, Constant.PENDING_REQUESTS).equals(null)) {
            OfflineRequestsDTO mExistingOfflineRequestsDTO = new OfflineRequestsDTO();
            List<OfflineRequestsDTO.PendingRequest> mPendingRequests = new ArrayList<>();
            mExistingOfflineRequestsDTO.setmPendingRequests(mPendingRequests);
            Utils.storeString(mContext, Constant.PENDING_REQUESTS, new Gson().toJson(mExistingOfflineRequestsDTO));
//        }
    }
}