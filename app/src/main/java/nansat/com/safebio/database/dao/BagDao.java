package nansat.com.safebio.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import nansat.com.safebio.database.models.Bag;

/**
 * Created by Phantasmist on 14/02/18.
 */

@Dao
public interface BagDao {

    @Query("SELECT * FROM Bag")
    LiveData<List<Bag>> getAllBags();

    @Query("SELECT * FROM Bag WHERE bag_id=:bagId")
    LiveData<Bag> getBagDetailsById(int bagId);

    @Query("SELECT * FROM Bag WHERE bar_code=:barCode")
    Bag getBagDetailsByBarCode(String barCode);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long createNewBag(Bag mBag);

    @Query("SELECT * FROM Bag WHERE lot_id=:lotId")
    LiveData<List<Bag>> getAllBagsFromLot(long lotId);

    @Delete
    int deleteBag(Bag mBag);

}
