package nansat.com.safebio.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import nansat.com.safebio.database.models.Lot;

/**
 * Created by Phantasmist on 13/02/18.
 */

@Dao
public interface LotDao {
    @Query("SELECT * FROM Lot")
    LiveData<List<Lot>> getLots();

    @Query("SELECT * FROM Lot WHERE lot_id=:lotId")
    LiveData<Lot> getLotById(long lotId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long createNewLot(Lot mLot);

    @Delete
    int deleteLot(Lot lotToBeDeleted);

    @Update
    int updateLot(Lot mLot);

}
