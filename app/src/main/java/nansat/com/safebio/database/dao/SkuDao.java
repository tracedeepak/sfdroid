package nansat.com.safebio.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import nansat.com.safebio.database.models.Sku;

/**
 * Created by Phantasmist on 17/02/18.
 */
@Dao
public interface SkuDao {
    @Query("SELECT * FROM Sku")
    LiveData<List<Sku>> getSkuList();

    @Query("SELECT * FROM Sku WHERE sku_id=:skuId")
    Sku getSkuById(long skuId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long createNewSKU(Sku mSku);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAllSku(Sku... mSkus);

    @Query("DELETE FROM sku")
    void deleteAllSku();

    @Delete
    int deleteSku(Sku mSku);

    @Update
    int updateSku(Sku mSku);

}
