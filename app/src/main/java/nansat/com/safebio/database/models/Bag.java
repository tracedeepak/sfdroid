package nansat.com.safebio.database.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import nansat.com.safebio.BR;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Phantasmist on 14/02/18.
 */

@Entity(foreignKeys = @ForeignKey(entity = Lot.class, parentColumns = "lot_id", childColumns = "lot_id", onDelete = CASCADE), indices = {@Index("lot_id")})
public class Bag extends BaseObservable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "bag_id")
    long bagId;
    @ColumnInfo(name = "lot_id")
    long lotId;
    @ColumnInfo(name = "weight")
    Double weight;
    @ColumnInfo(name = "bar_code")
    String barCode;
    @ColumnInfo(name = "sku_id")
    long skuId;

    public Bag(long lotId, Double weight, String barCode, long skuId) {
        this.lotId = lotId;
        this.weight = weight;
        this.barCode = barCode;
        this.skuId = skuId;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    @Bindable
    public long getBagId() {
        return bagId;
    }

    public void setBagId(long bagId) {
        this.bagId = bagId;
        notifyPropertyChanged(BR.bagId);
    }

    @Bindable
    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
        notifyPropertyChanged(BR.lotId);
    }

    @Bindable
    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
        notifyPropertyChanged(BR.weight);
    }

    @Bindable
    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
        notifyPropertyChanged(BR.barCode);
    }

}
