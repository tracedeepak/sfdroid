package nansat.com.safebio.database.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Phantasmist on 13/02/18.
 */

@Entity
public class Lot {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "lot_id")
    long lotId;
    @ColumnInfo(name = "total_bags")
    int totalBags;
    @ColumnInfo(name = "total_weight")
    Double totalWeight;

    public Lot(int totalBags, Double totalWeight) {
        this.totalBags = totalBags;
        this.totalWeight = totalWeight;
    }

    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public int getTotalBags() {
        return totalBags;
    }

    public void setTotalBags(int totalBags) {
        this.totalBags = totalBags;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }
}
