package nansat.com.safebio.database.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Phantasmist on 17/02/18.
 */

@Entity
public class Sku implements Comparable<Integer> {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "sku_id")
    long skuId;
    @ColumnInfo(name = "sku_name")
    String skuName;
    @ColumnInfo(name = "active")
    int active;

    public Sku(long skuId, String skuName, int active) {
        this.skuId = skuId;
        this.skuName = skuName;
        this.active = active;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

//    @Override
//    public int compareTo(@NonNull Sku sku) {
//        return Integer.valueOf("" + skuId).compareTo(Integer.valueOf("" + sku.getSkuId()));
//    }

    @Override
    public int compareTo(@NonNull Integer integer) {
        return Integer.valueOf("" + skuId).compareTo(integer);
    }
}
