package nansat.com.safebio.database.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.schedulers.Schedulers;
import nansat.com.safebio.database.AppDatabase;
import nansat.com.safebio.database.models.Bag;

/**
 * Created by Phantasmist on 14/02/18.
 */

public class BagViewModel extends AndroidViewModel {
    private AppDatabase mAppDatabase;

    public BagViewModel(Application mApplication) {
        super(mApplication);
        mAppDatabase = AppDatabase.getDatabase(mApplication);
    }

    public LiveData<List<Bag>> getAllBagsThatBelongToLotWithId(long lotId) {
        return mAppDatabase.bagDao().getAllBagsFromLot(lotId);
    }

    public void deleteBag(Bag mBag) {
        Observable.fromCallable(() -> mAppDatabase.bagDao().deleteBag(mBag)).subscribeOn(Schedulers.io()).subscribe();
    }

    public void addNewBag(Bag mBag, Observer<Long> isBagAddedObserver) {
        Observable.fromCallable(() -> mAppDatabase.bagDao().createNewBag(mBag)).subscribeOn(Schedulers.io()).subscribe(isBagAddedObserver);
    }

    public void checkWhetherBarCodeIsValid(String barcode, Observer<Bag> bagExistObserver) {
        Observable.fromCallable(() -> mAppDatabase.bagDao().getBagDetailsByBarCode(barcode)).subscribeOn(Schedulers.io()).subscribe(bagExistObserver);
    }

}
