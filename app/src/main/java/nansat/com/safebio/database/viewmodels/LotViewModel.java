package nansat.com.safebio.database.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.schedulers.Schedulers;
import nansat.com.safebio.database.AppDatabase;
import nansat.com.safebio.database.models.Lot;

/**
 * Created by Phantasmist on 13/02/18.
 */

public class LotViewModel extends AndroidViewModel {
    private AppDatabase mAppDatabase;


    public LotViewModel(Application mApplication) {
        super(mApplication);
        this.mAppDatabase = AppDatabase.getDatabase(mApplication);
    }

    public LiveData<List<Lot>> getAllLots() {
        return mAppDatabase.lotDao().getLots();
    }

    public LiveData<Lot> getLotByID(long id) {
        return mAppDatabase.lotDao().getLotById(id);
    }

    public void deleteLot(Lot mLot) {
        Observable.fromCallable(() -> mAppDatabase.lotDao().deleteLot(mLot)).subscribeOn(Schedulers.io()).subscribe();
    }

    public void updateLotDetails(Lot mLot, Observer<Integer> updateLotDetails) {
        Observable.fromCallable(() -> mAppDatabase.lotDao().updateLot(mLot)).subscribeOn(Schedulers.io()).subscribe(updateLotDetails);
    }

    Long lotId;

    public void addALot(Lot mLot, Observer<Long> mLotIDObserver) {
        Observable.fromCallable(() -> mAppDatabase.lotDao().createNewLot(mLot)).subscribeOn(Schedulers.io()).subscribe(mLotIDObserver);
    }


}
