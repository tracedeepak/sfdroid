package nansat.com.safebio.database.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.schedulers.Schedulers;
import nansat.com.safebio.database.AppDatabase;
import nansat.com.safebio.database.models.Sku;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class SkuViewModel extends AndroidViewModel {
    private AppDatabase mAppDatabase;

    public SkuViewModel(@NonNull Application application) {
        super(application);
        this.mAppDatabase = AppDatabase.getDatabase(application);
    }


    public LiveData<List<Sku>> getAllSku() {
        return mAppDatabase.skuDao().getSkuList();
    }


    public Sku getSkuById(long id) {
        return mAppDatabase.skuDao().getSkuById(id);
    }

    public void addSku(Sku mSku, Observer<Long> mSkuIDObserver) {
        Observable.fromCallable(() -> mAppDatabase.skuDao().createNewSKU(mSku)).subscribeOn(Schedulers.io()).subscribe(mSkuIDObserver);
    }

    public void insertAllSkus(Sku mSku, Observer<Long[]> mSkuIDObserver) {
        Observable.fromCallable(() -> mAppDatabase.skuDao().insertAllSku(mSku)).subscribeOn(Schedulers.io()).subscribe(mSkuIDObserver);
    }

    public void deleteAllSkus(CompletableObserver mDeleteSkusObserver){
        Completable.fromAction(()->mAppDatabase.skuDao().deleteAllSku()).subscribeOn(Schedulers.io()).subscribe(mDeleteSkusObserver);
    }

    public void deleteSku(Sku mSku) {
        Observable.fromCallable(() -> mAppDatabase.skuDao().deleteSku(mSku)).subscribeOn(Schedulers.io()).subscribe();
    }

    public void updateSkuDetails(Sku mSku, Observer<Integer> updateLotDetails) {
        Observable.fromCallable(() -> mAppDatabase.skuDao().updateSku(mSku)).subscribeOn(Schedulers.io()).subscribe(updateLotDetails);
    }
}
