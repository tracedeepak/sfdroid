package nansat.com.safebio.datamodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 08/03/18.
 */

public class AvailablePlansViewModel extends BaseObservable {

    String referenceNumber;
    boolean paymentMode=false;

    @Bindable
    public boolean isPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(boolean paymentMode) {
        this.paymentMode = paymentMode;
        notifyPropertyChanged(BR.paymentMode);
    }

    @Bindable
    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        notifyPropertyChanged(BR.referenceNumber);
    }
}
