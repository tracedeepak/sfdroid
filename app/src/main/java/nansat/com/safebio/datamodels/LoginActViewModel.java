package nansat.com.safebio.datamodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class LoginActViewModel extends BaseObservable{
    public String username;
    public String password;

    @Bindable
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }
}
