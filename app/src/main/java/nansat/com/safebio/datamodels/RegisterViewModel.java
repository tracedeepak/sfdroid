package nansat.com.safebio.datamodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;

import com.google.gson.annotations.SerializedName;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class RegisterViewModel extends BaseObservable implements Observable {

    @SerializedName("name")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("address")
    public String address;
    @SerializedName("contactperson")
    public String contactperson;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("landline")
    public String landline;
    @SerializedName("beds")
    public String beds;
    @SerializedName("latlong")
    public String latlong;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("hcf_type")
    public String hcf_type;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("district")
    public int districtId;

    @Bindable
    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
        notifyPropertyChanged(BR.districtId);
    }

    @Bindable
    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
        notifyPropertyChanged(BR.pincode);
    }

    @Bindable
    public String getHcf_type() {
        return hcf_type;
    }

    public void setHcf_type(String hcf_type) {
        this.hcf_type = hcf_type;
        notifyPropertyChanged(BR.hcf_type);
    }


    @Bindable
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        notifyPropertyChanged(BR.city);
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    @Bindable
    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
        notifyPropertyChanged(BR.contactperson);
    }

    @Bindable
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
        notifyPropertyChanged(BR.mobile);
    }

    @Bindable
    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
        notifyPropertyChanged(BR.landline);
    }

    @Bindable
    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
        notifyPropertyChanged(BR.beds);
    }

    @Bindable
    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
        notifyPropertyChanged(BR.latlong);
    }

}
