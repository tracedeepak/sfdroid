package nansat.com.safebio.datamodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 10/02/18.
 */

public class WeightCaptureViewModel extends BaseObservable {
    public String connectionStatus;
    public String deviceName;
    public String weightCaptured;
   
    @Bindable
    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
        notifyPropertyChanged(BR.connectionStatus);
    }

    @Bindable
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        notifyPropertyChanged(BR.deviceName);
    }

    @Bindable
    public String getWeightCaptured() {
        return weightCaptured;
    }

    public void setWeightCaptured(String weightCaptured) {
        this.weightCaptured = weightCaptured;
        notifyPropertyChanged(BR.weightCaptured);
    }
}
