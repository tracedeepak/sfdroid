package nansat.com.safebio.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.FragmentCpwtfProfileBinding;
import nansat.com.safebio.models.CpwtfAccountResponse;

/**
 * Created by Phantasmist on 02/04/18.
 */

public class CPWTFProfileFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentCpwtfProfileBinding mFragmentHcfdetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.fragment_cpwtf_profile, container, false);
        Bundle mBundle = getArguments();
        String data = mBundle.getString("data", "");
        CpwtfAccountResponse myAccountResponse = new Gson().fromJson(data, CpwtfAccountResponse.class);
        mFragmentHcfdetailsBinding.setInflateData(myAccountResponse.getData());
        return mFragmentHcfdetailsBinding.getRoot();
    }
}

