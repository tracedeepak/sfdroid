package nansat.com.safebio.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import nansat.com.safebio.R;
import nansat.com.safebio.databinding.FragmentHcfdetailsBinding;
import nansat.com.safebio.models.MyAccountResponse;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class HCFDetailsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentHcfdetailsBinding mFragmentHcfdetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(container.getContext()), R.layout.fragment_hcfdetails, container, false);
        Bundle mBundle = getArguments();
        String data = mBundle.getString("data", "");
        MyAccountResponse myAccountResponse = new Gson().fromJson(data, MyAccountResponse.class);
        mFragmentHcfdetailsBinding.setInflateData(myAccountResponse.getData().get(0).getHcf());
        return mFragmentHcfdetailsBinding.getRoot();
    }
}
