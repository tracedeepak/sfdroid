package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class AvailablePlansResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }


    public static class Data extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("plan_scheme")
        public String plan_scheme;
        @SerializedName("duration")
        public String duration;
        @SerializedName("base_cost")
        public String base_cost;
        @SerializedName("per_bed_cost")
        public String per_bed_cost;
        @SerializedName("desc_text1")
        public String desc_text1;
        @SerializedName("desc_text2")
        public String desc_text2;
        @SerializedName("desc_text3")
        public String desc_text3;
        @SerializedName("desc_text4")
        public String desc_text4;
        @SerializedName("active")
        public int active;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;


        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getPlan_scheme() {
            return plan_scheme;
        }

        public void setPlan_scheme(String plan_scheme) {
            this.plan_scheme = plan_scheme;
            notifyPropertyChanged(BR.plan_scheme);
        }

        @Bindable
        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
            notifyPropertyChanged(BR.duration);
        }

        @Bindable
        public String getBase_cost() {
            return base_cost;
        }

        public void setBase_cost(String base_cost) {
            this.base_cost = base_cost;
            notifyPropertyChanged(BR.base_cost);
        }

        @Bindable
        public String getPer_bed_cost() {
            return per_bed_cost;
        }

        public void setPer_bed_cost(String per_bed_cost) {
            this.per_bed_cost = per_bed_cost;
            notifyPropertyChanged(BR.per_bed_cost);
        }

        @Bindable
        public String getDesc_text1() {
            return desc_text1;
        }

        public void setDesc_text1(String desc_text1) {
            this.desc_text1 = desc_text1;
            notifyPropertyChanged(BR.desc_text1);
        }

        @Bindable
        public String getDesc_text2() {
            return desc_text2;
        }

        public void setDesc_text2(String desc_text2) {
            this.desc_text2 = desc_text2;
            notifyPropertyChanged(BR.desc_text2);
        }

        @Bindable
        public String getDesc_text3() {
            return desc_text3;
        }

        public void setDesc_text3(String desc_text3) {
            this.desc_text3 = desc_text3;
            notifyPropertyChanged(BR.desc_text3);
        }

        @Bindable
        public String getDesc_text4() {
            return desc_text4;
        }

        public void setDesc_text4(String desc_text4) {
            this.desc_text4 = desc_text4;
            notifyPropertyChanged(BR.desc_text4);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }


    }
}
