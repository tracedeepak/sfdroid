package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 02/04/18.
 */

public class CpwtfAccountResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }




    public static class Data extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("uuid")
        public String uuid;
        @SerializedName("first_name")
        public String first_name;
        @SerializedName("last_name")
        public String last_name;
        @SerializedName("email")
        public String email;
        @SerializedName("avatar_type")
        public String avatar_type;
        @SerializedName("avatar_location")
        public String avatar_location;
        @SerializedName("password_changed_at")
        public String password_changed_at;
        @SerializedName("active")
        public int active;
        @SerializedName("confirmation_code")
        public String confirmation_code;
        @SerializedName("confirmed")
        public int confirmed;
        @SerializedName("timezone")
        public String timezone;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("deleted_at")
        public String deleted_at;
        @SerializedName("api_token")
        public String api_token;
        @SerializedName("ref_type")
        public String ref_type;
        @SerializedName("ref_id")
        public String ref_id;
        @SerializedName("full_name")
        public String full_name;


        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
            notifyPropertyChanged(BR.uuid);
        }

        @Bindable
        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
            notifyPropertyChanged(BR.first_name);
        }

        @Bindable
        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
            notifyPropertyChanged(BR.last_name);
        }

        @Bindable
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
            notifyPropertyChanged(BR.email);
        }

        @Bindable
        public String getAvatar_type() {
            return avatar_type;
        }

        public void setAvatar_type(String avatar_type) {
            this.avatar_type = avatar_type;
            notifyPropertyChanged(BR.avatar_type);
        }

        @Bindable
        public String getAvatar_location() {
            return avatar_location;
        }

        public void setAvatar_location(String avatar_location) {
            this.avatar_location = avatar_location;
            notifyPropertyChanged(BR.avatar_location);
        }

        @Bindable
        public String getPassword_changed_at() {
            return password_changed_at;
        }

        public void setPassword_changed_at(String password_changed_at) {
            this.password_changed_at = password_changed_at;
            notifyPropertyChanged(BR.password_changed_at);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getConfirmation_code() {
            return confirmation_code;
        }

        public void setConfirmation_code(String confirmation_code) {
            this.confirmation_code = confirmation_code;
            notifyPropertyChanged(BR.confirmation_code);
        }

        @Bindable
        public int getConfirmed() {
            return confirmed;
        }

        public void setConfirmed(int confirmed) {
            this.confirmed = confirmed;
            notifyPropertyChanged(BR.confirmed);
        }

        @Bindable
        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
            notifyPropertyChanged(BR.timezone);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public String getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(String deleted_at) {
            this.deleted_at = deleted_at;
            notifyPropertyChanged(BR.deleted_at);
        }

        @Bindable
        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
            notifyPropertyChanged(BR.api_token);
        }

        @Bindable
        public String getRef_type() {
            return ref_type;
        }

        public void setRef_type(String ref_type) {
            this.ref_type = ref_type;
            notifyPropertyChanged(BR.ref_type);
        }

        @Bindable
        public String getRef_id() {
            return ref_id;
        }

        public void setRef_id(String ref_id) {
            this.ref_id = ref_id;
            notifyPropertyChanged(BR.ref_id);
        }

        @Bindable
        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
            notifyPropertyChanged(BR.full_name);
        }
        
    }
}
