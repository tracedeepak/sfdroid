package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phantasmist on 28/02/18.
 */

public class CreateOrderRequest {

    @SerializedName("api_token")
    public String api_token;
    @SerializedName("orderDetails")
    public List<Orderdetails> orderdetails;

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public List<Orderdetails> getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(List<Orderdetails> orderdetails) {
        this.orderdetails = orderdetails;
    }

    public static class Orderdetails {
        @SerializedName("productId")
        public int productId;
        @SerializedName("qty")
        public int qty;

        public Orderdetails(int productId, int qty) {
            this.productId = productId;
            this.qty = qty;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }
    }
}
