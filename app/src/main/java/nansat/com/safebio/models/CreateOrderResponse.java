package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 20/05/18.
 */

public class CreateOrderResponse {
    @SerializedName("error")
    public int error;
    @SerializedName("data")
    public Data data;
    @SerializedName("message")
    private String message;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("order_id")
        public int order_id;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }
    }
}
