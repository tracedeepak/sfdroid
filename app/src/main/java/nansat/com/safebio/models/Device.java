package nansat.com.safebio.models;

import android.bluetooth.BluetoothDevice;
import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Phantasmist on 08/02/18.
 */

public class Device extends BaseObservable implements Parcelable {
    public String deviceName;
    public String deviceAddress;
    public BluetoothDevice mBluetoothDevice;
    public String  uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public BluetoothDevice getmBluetoothDevice() {
        return mBluetoothDevice;
    }

    public void setmBluetoothDevice(BluetoothDevice mBluetoothDevice) {
        this.mBluetoothDevice = mBluetoothDevice;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.deviceName);
        dest.writeString(this.deviceAddress);
        dest.writeParcelable(this.mBluetoothDevice, 0);
        dest.writeString(this.uuid);
    }

    public Device() {
    }

    protected Device(Parcel in) {
        this.deviceName = in.readString();
        this.deviceAddress = in.readString();
        this.mBluetoothDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
        this.uuid = in.readString();
    }

    public static final Creator<Device> CREATOR = new Creator<Device>() {
        public Device createFromParcel(Parcel source) {
            return new Device(source);
        }

        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
}
