package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 19/02/18.
 */

public class HCFLotResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;
    private transient PropertyChangeRegistry propertyChangeRegistry = new PropertyChangeRegistry();

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }


    public static class Lotdetails extends BaseObservable {
        @SerializedName("lotDetailId")
        public int lotDetailId;
        @SerializedName("lotId")
        public int lotId;
        @SerializedName("weight")
        public String weight;
        @SerializedName("scanCode")
        public String scanCode;
        @SerializedName("skuId")
        public int skuId;
        @SerializedName("status")
        public int status;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    
        @Bindable
        public int getLotDetailId() {
            return lotDetailId;
        }

        public void setLotDetailId(int lotDetailId) {
            this.lotDetailId = lotDetailId;
            notifyPropertyChanged(BR.lotDetailId);
        }

        @Bindable
        public int getLotId() {
            return lotId;
        }

        public void setLotId(int lotId) {
            this.lotId = lotId;
            notifyPropertyChanged(BR.lotId);
        }

        @Bindable
        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
            notifyPropertyChanged(BR.weight);
        }

        @Bindable
        public String getScanCode() {
            return scanCode;
        }

        public void setScanCode(String scanCode) {
            this.scanCode = scanCode;
            notifyPropertyChanged(BR.scanCode);
        }

        @Bindable
        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
            notifyPropertyChanged(BR.skuId);
        }

        @Bindable
        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
            notifyPropertyChanged(BR.status);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

      
    }

    public static class Data extends BaseObservable {
        @SerializedName("lotId")
        public int lotId;
        @SerializedName("totalBags")
        public int totalBags;
        @SerializedName("totalWeight")
        public String totalWeight;
        @SerializedName("hcfId")
        public int hcfId;
        @SerializedName("lotstatus")
        public String lotstatus;
        @SerializedName("isConfirmed")
        public int isConfirmed;
        @SerializedName("createdBy")
        public String createdBy;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("lotdetails")
        public List<Lotdetails> lotdetails;
    
        @Bindable
        public int getLotId() {
            return lotId;
        }

        public void setLotId(int lotId) {
            this.lotId = lotId;
            notifyPropertyChanged(BR.lotId);
        }

        @Bindable
        public int getTotalBags() {
            return totalBags;
        }

        public void setTotalBags(int totalBags) {
            this.totalBags = totalBags;
            notifyPropertyChanged(BR.totalBags);
        }

        @Bindable
        public String getTotalWeight() {
            return totalWeight;
        }

        public void setTotalWeight(String totalWeight) {
            this.totalWeight = totalWeight;
            notifyPropertyChanged(BR.totalWeight);
        }

        @Bindable
        public int getHcfId() {
            return hcfId;
        }

        public void setHcfId(int hcfId) {
            this.hcfId = hcfId;
            notifyPropertyChanged(BR.hcfId);
        }

        @Bindable
        public String getLotstatus() {
            return lotstatus;
        }

        public void setLotstatus(String lotstatus) {
            this.lotstatus = lotstatus;
            notifyPropertyChanged(BR.lotstatus);
        }

        @Bindable
        public int getIsConfirmed() {
            return isConfirmed;
        }

        public void setIsConfirmed(int isConfirmed) {
            this.isConfirmed = isConfirmed;
            notifyPropertyChanged(BR.isConfirmed);
        }

        @Bindable
        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            notifyPropertyChanged(BR.createdBy);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public List<Lotdetails> getLotdetails() {
            return lotdetails;
        }

        public void setLotdetails(List<Lotdetails> lotdetails) {
            this.lotdetails = lotdetails;
            notifyPropertyChanged(BR.lotdetails);
        }

       
    }
}
