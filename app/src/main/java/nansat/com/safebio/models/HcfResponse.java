package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class HcfResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }

    public static class Data extends BaseObservable {
        @SerializedName("id")
        public int hcfId;
        @SerializedName("name")
        public String name;
        @SerializedName("email")
        public String email;
        @SerializedName("address")
        public String address;
        @SerializedName("createBy")
        public int createBy;
        @SerializedName("createdOn")
        public String createdOn;
        @SerializedName("active")
        public int active;


        @Bindable
        public int getHcfId() {
            return hcfId;
        }

        public void setHcfId(int hcfId) {
            this.hcfId = hcfId;
            notifyPropertyChanged(BR.hcfId);
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

        @Bindable
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
            notifyPropertyChanged(BR.email);
        }

        @Bindable
        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
            notifyPropertyChanged(BR.address);
        }

        @Bindable
        public int getCreateBy() {
            return createBy;
        }

        public void setCreateBy(int createBy) {
            this.createBy = createBy;
            notifyPropertyChanged(BR.createBy);
        }

        @Bindable
        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
            notifyPropertyChanged(BR.createdOn);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }


    }
}
