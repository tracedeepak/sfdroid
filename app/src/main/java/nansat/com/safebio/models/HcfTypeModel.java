package nansat.com.safebio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HcfTypeModel {

    @Expose
    @SerializedName("allHcffTypes")
    public List<AllHcffTypes> allHcffTypes;

    public List<AllHcffTypes> getAllHcffTypes() {
        return allHcffTypes;
    }

    public void setAllHcffTypes(List<AllHcffTypes> allHcffTypes) {
        this.allHcffTypes = allHcffTypes;
    }

    public static class AllHcffTypes {
        @Expose
        @SerializedName("updated_at")
        public String updated_at;
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("hcf_type_desc")
        public String hcf_type_desc;
        @Expose
        @SerializedName("hcf_type")
        public String hcf_type;
        @Expose
        @SerializedName("id")
        public int id;

        public String getHcf_type_desc() {
            return hcf_type_desc;
        }

        public void setHcf_type_desc(String hcf_type_desc) {
            this.hcf_type_desc = hcf_type_desc;
        }

        public String getHcf_type() {
            return hcf_type;
        }

        public void setHcf_type(String hcf_type) {
            this.hcf_type = hcf_type;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
