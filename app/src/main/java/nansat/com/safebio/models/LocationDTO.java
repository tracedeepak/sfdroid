package nansat.com.safebio.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationDTO {
    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;

    @Expose
    @SerializedName("data")
    private List<LocationData> locationData;

    public List<LocationData> getLocationData() {
        return locationData;
    }

    public void setLocationData(List<LocationData> locationData) {
        this.locationData = locationData;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class LocationData implements Comparable<LocationData> {
        @Expose
        @SerializedName("postalCharges")
        private int postalCharges;
        @Expose
        @SerializedName("districtId")
        private int districtId;
        @Expose
        @SerializedName("districtName")
        private String districtName;
        @Expose
        @SerializedName("stateId")
        private int stateId;
        @Expose
        @SerializedName("stateCode")
        private String stateCode;
        @Expose
        @SerializedName("stateName")
        private String stateName;

        public int getPostalCharges() {
            return postalCharges;
        }

        public void setPostalCharges(int postalCharges) {
            this.postalCharges = postalCharges;
        }

        public int getDistrictId() {
            return districtId;
        }

        public void setDistrictId(int districtId) {
            this.districtId = districtId;
        }

        public String getDistrictName() {
            return districtName;
        }

        public void setDistrictName(String districtName) {
            this.districtName = districtName;
        }

        public int getStateId() {
            return stateId;
        }

        public void setStateId(int stateId) {
            this.stateId = stateId;
        }

        public String getStateCode() {
            return stateCode;
        }

        public void setStateCode(String stateCode) {
            this.stateCode = stateCode;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        @Override
        public int compareTo(@NonNull LocationData locationData) {
            return stateName.compareTo(locationData.stateName);
        }
    }
}
