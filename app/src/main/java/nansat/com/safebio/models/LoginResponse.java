package nansat.com.safebio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class LoginResponse {

    @Expose
    @SerializedName("user_meta")
    private User_meta user_meta;
    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String Message;
    @SerializedName("data")
    public Data data;

    public static class User_meta {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("active")
        private int active;
        @Expose
        @SerializedName("pincode")
        private int pincode;
        @Expose
        @SerializedName("hcf_code")
        private String hcf_code;
        @Expose
        @SerializedName("hcf_type_id")
        private int hcf_type_id;
        @Expose
        @SerializedName("district_id")
        private int district_id;
        @Expose
        @SerializedName("state_id")
        private int state_id;
        @Expose
        @SerializedName("landline")
        private String landline;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("latlong")
        private String latlong;
        @Expose
        @SerializedName("website")
        private String website;
        @Expose
        @SerializedName("beds")
        private String beds;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("contactperson")
        private String contactperson;
        @Expose
        @SerializedName("createBy")
        private String createBy;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getHcf_code() {
            return hcf_code;
        }

        public void setHcf_code(String hcf_code) {
            this.hcf_code = hcf_code;
        }

        public int getHcf_type_id() {
            return hcf_type_id;
        }

        public void setHcf_type_id(int hcf_type_id) {
            this.hcf_type_id = hcf_type_id;
        }

        public int getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(int district_id) {
            this.district_id = district_id;
        }

        public int getState_id() {
            return state_id;
        }

        public void setState_id(int state_id) {
            this.state_id = state_id;
        }

        public String getLandline() {
            return landline;
        }

        public void setLandline(String landline) {
            this.landline = landline;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatlong() {
            return latlong;
        }

        public void setLatlong(String latlong) {
            this.latlong = latlong;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getBeds() {
            return beds;
        }

        public void setBeds(String beds) {
            this.beds = beds;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getContactperson() {
            return contactperson;
        }

        public void setContactperson(String contactperson) {
            this.contactperson = contactperson;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("userId")
        public int userId;
        @SerializedName("confirmed")
        public Boolean confirmed;
        @SerializedName("name")
        public String name;
        @SerializedName("apiToken")
        public String apiToken;
        @SerializedName("role")
        public String role;
        @SerializedName("refId")
        public int refId;
        @SerializedName("hasSubscribed")
        public boolean hasSubscribed;
        @SerializedName("email")
        public String email;
        @SerializedName("mobile")
        public String mobile;
        @SerializedName("user_meta")
        public User_meta hospitalData;

        public User_meta getHospitalData() {
            return hospitalData;
        }

        public void setHospitalData(User_meta hospitalData) {
            this.hospitalData = hospitalData;
        }

        public Boolean getConfirmed() {
            return confirmed;
        }

        public void setConfirmed(Boolean confirmed) {
            this.confirmed = confirmed;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public int getRefId() {
            return refId;
        }

        public void setRefId(int refId) {
            this.refId = refId;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public boolean isHasSubscribed() {
            return hasSubscribed;
        }

        public void setHasSubscribed(boolean hasSubscribed) {
            this.hasSubscribed = hasSubscribed;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getApiToken() {
            return apiToken;
        }

        public void setApiToken(String apiToken) {
            this.apiToken = apiToken;
        }
    }

}
