package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyAccountResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }


    public static class Plan extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("plan_scheme")
        public String plan_scheme;
        @SerializedName("duration")
        public String duration;
        @SerializedName("base_cost")
        public String base_cost;
        @SerializedName("per_bed_cost")
        public String per_bed_cost;
        @SerializedName("desc_text1")
        public String desc_text1;
        @SerializedName("desc_text2")
        public String desc_text2;
        @SerializedName("desc_text3")
        public String desc_text3;
        @SerializedName("desc_text4")
        public String desc_text4;
        @SerializedName("active")
        public int active;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;


        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getPlan_scheme() {
            return plan_scheme;
        }

        public void setPlan_scheme(String plan_scheme) {
            this.plan_scheme = plan_scheme;
            notifyPropertyChanged(BR.plan_scheme);
        }

        @Bindable
        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
            notifyPropertyChanged(BR.duration);
        }

        @Bindable
        public String getBase_cost() {
            return base_cost;
        }

        public void setBase_cost(String base_cost) {
            this.base_cost = base_cost;
            notifyPropertyChanged(BR.base_cost);
        }

        @Bindable
        public String getPer_bed_cost() {
            return per_bed_cost;
        }

        public void setPer_bed_cost(String per_bed_cost) {
            this.per_bed_cost = per_bed_cost;
            notifyPropertyChanged(BR.per_bed_cost);
        }

        @Bindable
        public String getDesc_text1() {
            return desc_text1;
        }

        public void setDesc_text1(String desc_text1) {
            this.desc_text1 = desc_text1;
            notifyPropertyChanged(BR.desc_text1);
        }

        @Bindable
        public String getDesc_text2() {
            return desc_text2;
        }

        public void setDesc_text2(String desc_text2) {
            this.desc_text2 = desc_text2;
            notifyPropertyChanged(BR.desc_text2);
        }

        @Bindable
        public String getDesc_text3() {
            return desc_text3;
        }

        public void setDesc_text3(String desc_text3) {
            this.desc_text3 = desc_text3;
            notifyPropertyChanged(BR.desc_text3);
        }

        @Bindable
        public String getDesc_text4() {
            return desc_text4;
        }

        public void setDesc_text4(String desc_text4) {
            this.desc_text4 = desc_text4;
            notifyPropertyChanged(BR.desc_text4);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }


    }

    public static class Hcf extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("email")
        public String email;
        @SerializedName("address")
        public String address;
        @SerializedName("createBy")
        public String createBy;
        @SerializedName("contactperson")
        public String contactperson;
        @SerializedName("mobile")
        public String mobile;
        @SerializedName("beds")
        public String beds;
        @SerializedName("website")
        public String website;
        @SerializedName("latlong")
        public String latlong;
        @SerializedName("city")
        public String city;
        @SerializedName("landline")
        public String landline;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("active")
        public int active;
        @SerializedName("state_id")
        public int state_id;
        @SerializedName("district_id")
        public int district_id;
        @SerializedName("hcf_type_id")
        public int hcf_type_id;
        @SerializedName("hcf_code")
        public String hcf_code;
        @SerializedName("pin_code")
        public String pin_code;

        @Bindable
        public int getState_id() {
            return state_id;
        }

        public void setState_id(int state_id) {
            this.state_id = state_id;
            notifyPropertyChanged(BR.state_id);
        }

        @Bindable
        public int getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(int district_id) {
            this.district_id = district_id;
            notifyPropertyChanged(BR.district_id);
        }

        @Bindable
        public int getHcf_type_id() {
            return hcf_type_id;
        }

        public void setHcf_type_id(int hcf_type_id) {
            this.hcf_type_id = hcf_type_id;
            notifyPropertyChanged(BR.hcf_type_id);
        }

        @Bindable
        public String getHcf_code() {
            return hcf_code;
        }

        public void setHcf_code(String hcf_code) {
            this.hcf_code = hcf_code;
            notifyPropertyChanged(BR.hcf_code);
        }

        @Bindable
        public String getPin_code() {
            return pin_code;
        }

        public void setPin_code(String pin_code) {
            this.pin_code = pin_code;
            notifyPropertyChanged(BR.pin_code);
        }

        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

        @Bindable
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
            notifyPropertyChanged(BR.email);
        }

        @Bindable
        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
            notifyPropertyChanged(BR.address);
        }

        @Bindable
        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
            notifyPropertyChanged(BR.createBy);
        }

        @Bindable
        public String getContactperson() {
            return contactperson;
        }

        public void setContactperson(String contactperson) {
            this.contactperson = contactperson;
            notifyPropertyChanged(BR.contactperson);
        }

        @Bindable
        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
            notifyPropertyChanged(BR.mobile);
        }

        @Bindable
        public String getBeds() {
            return beds;
        }

        public void setBeds(String beds) {
            this.beds = beds;
            notifyPropertyChanged(BR.beds);
        }

        @Bindable
        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
            notifyPropertyChanged(BR.website);
        }

        @Bindable
        public String getLatlong() {
            return latlong;
        }

        public void setLatlong(String latlong) {
            this.latlong = latlong;
            notifyPropertyChanged(BR.latlong);
        }

        @Bindable
        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
            notifyPropertyChanged(BR.city);
        }

        @Bindable
        public String getLandline() {
            return landline;
        }

        public void setLandline(String landline) {
            this.landline = landline;
            notifyPropertyChanged(BR.landline);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }


    }

    public static class Data extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("hcf_id")
        public int hcf_id;
        @SerializedName("plan_id")
        public int plan_id;
        @SerializedName("start_date")
        public String start_date;
        @SerializedName("end_date")
        public String end_date;
        @SerializedName("is_paid")
        public String is_paid;
        @SerializedName("payment_mode")
        public String payment_mode;
        @SerializedName("payment_ref_number")
        public String payment_ref_number;
        @SerializedName("active")
        public int active;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("plan")
        public Plan plan;
        @SerializedName("hcf")
        public Hcf hcf;


        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public int getHcf_id() {
            return hcf_id;
        }

        public void setHcf_id(int hcf_id) {
            this.hcf_id = hcf_id;
            notifyPropertyChanged(BR.hcf_id);
        }

        @Bindable
        public int getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(int plan_id) {
            this.plan_id = plan_id;
            notifyPropertyChanged(BR.plan_id);
        }

        @Bindable
        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
            notifyPropertyChanged(BR.start_date);
        }

        @Bindable
        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
            notifyPropertyChanged(BR.end_date);
        }

        @Bindable
        public String getIs_paid() {
            return is_paid;
        }

        public void setIs_paid(String is_paid) {
            this.is_paid = is_paid;
            notifyPropertyChanged(BR.is_paid);
        }

        @Bindable
        public String getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
            notifyPropertyChanged(BR.payment_mode);
        }

        @Bindable
        public String getPayment_ref_number() {
            return payment_ref_number;
        }

        public void setPayment_ref_number(String payment_ref_number) {
            this.payment_ref_number = payment_ref_number;
            notifyPropertyChanged(BR.payment_ref_number);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public Plan getPlan() {
            return plan;
        }

        public void setPlan(Plan plan) {
            this.plan = plan;
            notifyPropertyChanged(BR.plan);
        }

        @Bindable
        public Hcf getHcf() {
            return hcf;
        }

        public void setHcf(Hcf hcf) {
            this.hcf = hcf;
            notifyPropertyChanged(BR.hcf);
        }


    }
}
