package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 10/03/18.
 */

public class MyOrdersResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }


    public static class Product extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("categoryId")
        public int categoryId;
        @SerializedName("name")
        public String name;
        @SerializedName("image")
        public String image;
        @SerializedName("icon")
        public String icon;
        @SerializedName("price")
        public String price;
        @SerializedName("description")
        public String description;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("active")
        public int active;
        @SerializedName("igst")
        public String igst;
        @SerializedName("cgst")
        public String cgst;



        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
            notifyPropertyChanged(BR.categoryId);
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

        @Bindable
        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
            notifyPropertyChanged(BR.image);
        }

        @Bindable
        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
            notifyPropertyChanged(BR.icon);
        }

        @Bindable
        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
            notifyPropertyChanged(BR.price);
        }

        @Bindable
        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
            notifyPropertyChanged(BR.description);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

        @Bindable
        public String getIgst() {
            return igst;
        }

        public void setIgst(String igst) {
            this.igst = igst;
            notifyPropertyChanged(BR.igst);
        }

        @Bindable
        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
            notifyPropertyChanged(BR.cgst);
        }


    }

    public static class Order_details extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("orderId")
        public int orderId;
        @SerializedName("productId")
        public int productId;
        @SerializedName("price")
        public String price;
        @SerializedName("qty")
        public int qty;
        @SerializedName("igst")
        public String igst;
        @SerializedName("gst")
        public String gst;
        @SerializedName("cgst")
        public String cgst;
        @SerializedName("amount")
        public String amount;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("product")
        public Product product;

        @Bindable
        public String getGst() {
            return gst;
        }

        public void setGst(String gst) {
            this.gst = gst;
            notifyPropertyChanged(BR.gst);
        }

        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
            notifyPropertyChanged(BR.orderId);
        }

        @Bindable
        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
            notifyPropertyChanged(BR.productId);
        }

        @Bindable
        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
            notifyPropertyChanged(BR.price);
        }

        @Bindable
        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
            notifyPropertyChanged(BR.qty);
        }

        @Bindable
        public String getIgst() {
            return igst;
        }

        public void setIgst(String igst) {
            this.igst = igst;
            notifyPropertyChanged(BR.igst);
        }

        @Bindable
        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
            notifyPropertyChanged(BR.cgst);
        }

        @Bindable
        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
            notifyPropertyChanged(BR.amount);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
            notifyPropertyChanged(BR.product);
        }


    }

    public static class Data extends BaseObservable {
        @SerializedName("id")
        public int id;
        @SerializedName("orderNumber")
        public String orderNumber;
        @SerializedName("subTotal")
        public String subTotal;
        @SerializedName("total")
        public String total;
        @SerializedName("hcfId")
        public int hcfId;
        @SerializedName("status")
        public int status;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("order_details")
        public List<Order_details> order_details;


        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
            notifyPropertyChanged(BR.orderNumber);
        }

        @Bindable
        public String getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(String subTotal) {
            this.subTotal = subTotal;
            notifyPropertyChanged(BR.subTotal);
        }

        @Bindable
        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
            notifyPropertyChanged(BR.total);
        }

        @Bindable
        public int getHcfId() {
            return hcfId;
        }

        public void setHcfId(int hcfId) {
            this.hcfId = hcfId;
            notifyPropertyChanged(BR.hcfId);
        }

        @Bindable
        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
            notifyPropertyChanged(BR.status);
        }

        @Bindable
        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
            notifyPropertyChanged(BR.created_at);
        }

        @Bindable
        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
            notifyPropertyChanged(BR.updated_at);
        }

        @Bindable
        public List<Order_details> getOrder_details() {
            return order_details;
        }

        public void setOrder_details(List<Order_details> order_details) {
            this.order_details = order_details;
            notifyPropertyChanged(BR.order_details);
        }


    }
}
