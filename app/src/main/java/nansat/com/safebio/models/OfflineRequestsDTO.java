package nansat.com.safebio.models;

import android.support.annotation.NonNull;

import java.util.List;

public class OfflineRequestsDTO {
    List<PendingRequest> mPendingRequests;

    public List<PendingRequest> getmPendingRequests() {
        return mPendingRequests;
    }

    public void setmPendingRequests(List<PendingRequest> mPendingRequests) {
        this.mPendingRequests = mPendingRequests;
    }

    public class PendingRequest implements Comparable<PendingRequest> {
        String requestType;

        String requestData;
        long timeStamp;
        boolean requestCompleted=false;

        public boolean isRequestCompleted() {
            return requestCompleted;
        }

        public void setRequestCompleted(boolean requestCompleted) {
            this.requestCompleted = requestCompleted;
        }

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public String getRequestData() {
            return requestData;
        }

        public void setRequestData(String requestData) {
            this.requestData = requestData;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public void setTimeStamp(long timeStamp) {
            this.timeStamp = timeStamp;
        }

        @Override
        public int compareTo(@NonNull PendingRequest pendingRequest) {
            return Long.valueOf(pendingRequest.getTimeStamp()).compareTo(timeStamp);
        }
    }

}
