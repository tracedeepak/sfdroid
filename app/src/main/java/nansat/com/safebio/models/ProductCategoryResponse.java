package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class ProductCategoryResponse extends BaseObservable  {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }


    public static class Data extends BaseObservable  {
        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("icon")
        public String icon;

        @Bindable
        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
            notifyPropertyChanged(BR.icon);
        }

        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

    }
}
