package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class ProductsResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }



    public static class Data extends BaseObservable  {
        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("icon")
        public String icon;
        @SerializedName("image")
        public String image;
        @SerializedName("price")
        public String price;
        @SerializedName("description")
        public String description;
        @SerializedName("cgst")
        public String cgst;
        @SerializedName("igst")
        public String igst;
        @SerializedName("gst")
        public String gst;
        @SerializedName("external_link")
        public String external_link;
        @SerializedName("quantity")
        public int quantity=0;
        @SerializedName("totalPrice")
        public double totalPrice=0;
        @SerializedName("min_order_qty")
        public int min_order_qty=0;
        @SerializedName("batch_qty")
        public int batch_qty=0;

        public boolean addToCart=false;

        @Bindable
        public String getGst() {
            return gst;
        }

        public void setGst(String gst) {
            this.gst = gst;
            notifyPropertyChanged(BR.gst);
        }

        @Bindable
        public String getExternal_link() {
            return external_link;
        }

        public void setExternal_link(String external_link) {
            this.external_link = external_link;
            notifyPropertyChanged(BR.external_link);
        }

        @Bindable
        public int getBatch_qty() {
            return batch_qty;
        }

        public void setBatch_qty(int batch_qty) {
            this.batch_qty = batch_qty;
            notifyPropertyChanged(BR.batch_qty);
        }

        @Bindable
        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
            notifyPropertyChanged(BR.cgst);
        }

        @Bindable
        public String getIgst() {
            return igst;
        }

        public void setIgst(String igst) {
            this.igst = igst;
            notifyPropertyChanged(BR.igst);
        }

        @Bindable
        public boolean isAddToCart() {
            return addToCart;
        }

        public void setAddToCart(boolean addToCart) {
            this.addToCart = addToCart;
            notifyPropertyChanged(BR.addToCart);
        }

        @Bindable
        public int getMin_order_qty() {
            return min_order_qty;
        }

        public void setMin_order_qty(int min_order_qty) {
            this.min_order_qty = min_order_qty;
            notifyPropertyChanged(BR.min_order_qty);
        }

        @Bindable
        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
            notifyPropertyChanged(BR.totalPrice);
        }



        @Bindable
        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
            notifyPropertyChanged(BR.quantity);
        }

        @Bindable
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
            notifyPropertyChanged(BR.id);
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

        @Bindable
        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
            notifyPropertyChanged(BR.icon);
        }

        @Bindable
        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
            notifyPropertyChanged(BR.image);
        }

        @Bindable
        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
            notifyPropertyChanged(BR.price);
        }

        @Bindable
        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
            notifyPropertyChanged(BR.description);
        }


    }
}
