package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class RegisterHCFRequest {
    @SerializedName("name")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("address")
    public String address;
    @SerializedName("contactperson")
    public String contactperson;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("landline") /// to be renamed as website
    public String landline;
    @SerializedName("beds")
    public String beds;
    @SerializedName("latlong")
    public String latlong;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("website")
    public String website;
    @SerializedName("hcf_type")
    public String hcf_type;
    @SerializedName("pincode")
    public String pincode;
    @SerializedName("district")
    public int district;

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getHcf_type() {
        return hcf_type;
    }

    public void setHcf_type(String hcf_type) {
        this.hcf_type = hcf_type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }
}
