package nansat.com.safebio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportIssueRequest {

    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("subject")
    private String subject;
    @Expose
    @SerializedName("api_token")
    private String api_token;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }
}
