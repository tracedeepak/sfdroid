package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 02/04/18.
 */

public class ReportsResponse extends BaseObservable {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String Message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }

    public class Data extends BaseObservable {
        @SerializedName("scanCode")
        public String scanCode;
        @SerializedName("skuName")
        public String skuName;
        @SerializedName("hcfName")
        public String hcfName;
        @SerializedName("cpwtfName")
        public String cpwtfName;
        @SerializedName("picked_on")
        public String picked_on;
        @SerializedName("weight_picked")
        public String weight_picked;
        @SerializedName("received_on")
        public String received_on;
        @SerializedName("weight_received")
        public String weight_received;

        @Bindable
        public String getScanCode() {
            return scanCode;
        }

        public void setScanCode(String scanCode) {
            this.scanCode = scanCode;
            notifyPropertyChanged(BR.scanCode);
        }

        @Bindable
        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
            notifyPropertyChanged(BR.skuName);
        }

        @Bindable
        public String getHcfName() {
            return hcfName;
        }

        public void setHcfName(String hcfName) {
            this.hcfName = hcfName;
            notifyPropertyChanged(BR.weight_received);
        }

        @Bindable
        public String getCpwtfName() {
            return cpwtfName;
        }

        public void setCpwtfName(String cpwtfName) {
            this.cpwtfName = cpwtfName;
            notifyPropertyChanged(BR.cpwtfName);
        }

        @Bindable
        public String getPicked_on() {
            return picked_on;
        }

        public void setPicked_on(String picked_on) {
            this.picked_on = picked_on;
            notifyPropertyChanged(BR.picked_on);
        }

        @Bindable
        public String getWeight_picked() {
            return weight_picked;
        }

        public void setWeight_picked(String weight_picked) {
            this.weight_picked = weight_picked;
            notifyPropertyChanged(BR.weight_picked);
        }

        @Bindable
        public String getReceived_on() {
            return received_on;
        }

        public void setReceived_on(String received_on) {
            this.received_on = received_on;
            notifyPropertyChanged(BR.received_on);
        }

        @Bindable
        public String getWeight_received() {
            return weight_received;
        }

        public void setWeight_received(String weight_received) {
            this.weight_received = weight_received;
            notifyPropertyChanged(BR.weight_received);
        }
    }
}
