package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import nansat.com.safebio.BR;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class SKUResponse extends BaseObservable{

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    @Bindable
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    @Bindable
    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
        notifyPropertyChanged(BR.data);
    }

    public static class Data extends BaseObservable{
        @SerializedName("id")
        public int skuId;
        @SerializedName("skuName")
        public String skuName;
        @SerializedName("active")
        public int active;

        @Bindable
        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
            notifyPropertyChanged(BR.skuId);
        }

        @Bindable
        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
            notifyPropertyChanged(BR.skuName);
        }

        @Bindable
        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
            notifyPropertyChanged(BR.active);
        }

    }
}
