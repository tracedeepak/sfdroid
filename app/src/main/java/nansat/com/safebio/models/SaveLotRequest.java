package nansat.com.safebio.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phantasmist on 18/02/18.
 */

public class SaveLotRequest {

    @SerializedName("api_token")
    public String apiToken;
    @SerializedName("totalBags")
    public int totalBags;
    @SerializedName("totalWeight")
    public String totalWeight;
    @SerializedName("hcfId")
    public long hcfId;
    @SerializedName("lotId")
    public long lotId;
    @SerializedName("lotdetails")
    public List<Bag> lotdetails;
    @SerializedName("lat")
    public String lat;
    @SerializedName("lng")
    public String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public int getTotalBags() {
        return totalBags;
    }

    public void setTotalBags(int totalBags) {
        this.totalBags = totalBags;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public long getHcfId() {
        return hcfId;
    }

    public void setHcfId(long hcfId) {
        this.hcfId = hcfId;
    }

    public List<Bag> getLotdetails() {
        return lotdetails;
    }

    public void setLotdetails(List<Bag> lotdetails) {
        this.lotdetails = lotdetails;
    }

    public static class Bag implements Comparable<Bag> {
        @SerializedName("weight")
        public String weight;
        @SerializedName("scanCode")
        public String scanCode;
        @SerializedName("skuId")
        public long skuId;
        @SerializedName("skuName")
        public String skuName;

        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getScanCode() {
            return scanCode;
        }

        public void setScanCode(String scanCode) {
            this.scanCode = scanCode;
        }

        public long getSkuId() {
            return skuId;
        }

        public void setSkuId(long skuId) {
            this.skuId = skuId;
        }

        @Override
        public int compareTo(@NonNull Bag bag) {
            return scanCode.compareTo(bag.scanCode);
        }
    }
}
