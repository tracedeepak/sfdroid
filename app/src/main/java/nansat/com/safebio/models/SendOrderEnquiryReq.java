package nansat.com.safebio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendOrderEnquiryReq {

    @Expose
    @SerializedName("products")
    private List<Products> products;
    @Expose
    @SerializedName("api_token")
    private String api_token;

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public static class Products {
        @Expose
        @SerializedName("qty")
        private int qty;
        @Expose
        @SerializedName("productName")
        private String productName;

        public Products(String name,int qty){
            this.productName=name;
            this.qty=qty;
        }
        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }
    }
}
