package nansat.com.safebio.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phantasmist on 11/05/18.
 */

public class StateCitesModel extends BaseObservable {

    @SerializedName("data")
    public List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data extends BaseObservable {
        @SerializedName("id")
        public String id;
        @SerializedName("name")
        public String name;
        @SerializedName("state")
        public String state;

        @Bindable
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Bindable
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Bindable
        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
