package nansat.com.safebio.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatesModel {

    @Expose
    @SerializedName("allStates")
    private List<AllStates> allStates;

    public List<AllStates> getAllStates() {
        return allStates;
    }

    public void setAllStates(List<AllStates> allStates) {
        this.allStates = allStates;
    }

    public static class AllStates implements Comparable<AllStates> {
        @Expose
        @SerializedName("country_id")
        private int country_id;
        @Expose
        @SerializedName("state_code")
        private String state_code;
        @Expose
        @SerializedName("state_name")
        private String state_name;
        @Expose
        @SerializedName("id")
        private int id;

        public int getCountry_id() {
            return country_id;
        }

        public void setCountry_id(int country_id) {
            this.country_id = country_id;
        }

        public String getState_code() {
            return state_code;
        }

        public void setState_code(String state_code) {
            this.state_code = state_code;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int compareTo(@NonNull AllStates allStates) {
            return state_name.compareTo(allStates.state_name);
        }
    }
}
