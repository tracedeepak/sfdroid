package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 08/03/18.
 */

public class SubscribePlanRequest {

    @SerializedName("api_token")
    public String api_token;
    @SerializedName("hcf_id")
    public int hcf_id;
    @SerializedName("plan_id")
    public int plan_id;
    @SerializedName("payment_mode")
    public String payment_mode;
    @SerializedName("payment_ref_number")
    public String payment_ref_number;

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public int getHcf_id() {
        return hcf_id;
    }

    public void setHcf_id(int hcf_id) {
        this.hcf_id = hcf_id;
    }

    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getPayment_ref_number() {
        return payment_ref_number;
    }

    public void setPayment_ref_number(String payment_ref_number) {
        this.payment_ref_number = payment_ref_number;
    }
}
