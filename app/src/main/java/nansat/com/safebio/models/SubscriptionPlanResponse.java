package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 08/03/18.
 */

public class SubscriptionPlanResponse {

    @SerializedName("error")
    public int error;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("subscriptionId")
        public int subscriptionId;

        public int getSubscriptionId() {
            return subscriptionId;
        }

        public void setSubscriptionId(int subscriptionId) {
            this.subscriptionId = subscriptionId;
        }
    }
}
