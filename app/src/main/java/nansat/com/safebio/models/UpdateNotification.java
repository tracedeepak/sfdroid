package nansat.com.safebio.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class UpdateNotification {

    private String message;
    private String changeLog;
    private int updateVersionCode;
    private int forceUpdateVersionCode;



    public UpdateNotification() {
    }

    public UpdateNotification(String message, String changeLog) {
        this.message = message;
        this.changeLog = changeLog;
    }

    public int getUpdateVersionCode() {
        return updateVersionCode;
    }

    public void setUpdateVersionCode(int updateVersionCode) {
        this.updateVersionCode = updateVersionCode;
    }

    public int getForceUpdateVersionCode() {
        return forceUpdateVersionCode;
    }

    public void setForceUpdateVersionCode(int forceUpdateVersionCode) {
        this.forceUpdateVersionCode = forceUpdateVersionCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(String changeLog) {
        this.changeLog = changeLog;
    }
}
