package nansat.com.safebio.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phantasmist on 20/05/18.
 */

public class UpdateOrderRequest {

    @SerializedName("api_token")
    public String api_token;
    @SerializedName("orderid")
    public int orderid;
    @SerializedName("payment_ref_number")
    public String payment_ref_number;

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public String getPayment_ref_number() {
        return payment_ref_number;
    }

    public void setPayment_ref_number(String payment_ref_number) {
        this.payment_ref_number = payment_ref_number;
    }
}
