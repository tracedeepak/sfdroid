package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import nansat.com.safebio.contracts.AvailablePlansActContract;
import nansat.com.safebio.models.AvailablePlansResponse;
import nansat.com.safebio.models.SubscribePlanRequest;
import nansat.com.safebio.models.SubscriptionPlanResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 08/03/18.
 */

public class AvailablePlansPresenter {
    AvailablePlansActContract mAvailablePlansActContract;
    Activity mActivity;

    public AvailablePlansPresenter(AvailablePlansActContract mAvailablePlansActContract) {
        this.mAvailablePlansActContract = mAvailablePlansActContract;
        this.mActivity = mAvailablePlansActContract.provideContext();
    }

    public void getActiveAvailablePlans() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<AvailablePlansResponse> mAvailablePlansResponseCall = SafeBioClient.getInstance().getApiInterface().getAvailableActivePlans(Constant.getAvailableActivePlans(), Utils.getString(mActivity, Constant.KEY_TOKEN));
        mAvailablePlansResponseCall.enqueue(new RetrofitCallback<AvailablePlansResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(AvailablePlansResponse arg0) {
                mAvailablePlansActContract.inflateAvailablePlansToGrid(arg0.getData());
            }
        });
    }

    public void subscribedButtonPressed(AvailablePlansResponse.Data mData) {
        mAvailablePlansActContract.proceedToPaymentOptions(mData);
    }


    public void submitPayment(boolean paymentMode) {

        Log.e("REFFERENCE NUm", "" + mAvailablePlansActContract.validateReferenceNum());
        Log.e("PAYMENT MODE", "" + paymentMode);
        if (paymentMode) {
            mAvailablePlansActContract.callInstaMojoPay();
        } else if (!paymentMode && mAvailablePlansActContract.validateReferenceNum()) {
            ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity, false);
            SubscribePlanRequest mSubscribePlanRequest = mAvailablePlansActContract.prepareSubscribePlanRequest();
            Call<SubscriptionPlanResponse> mSubscriptionPlanResponseCall = SafeBioClient.getInstance().getApiInterface().subscribeToAPlan(Constant.subscribeToPlan(), mSubscribePlanRequest);
            mSubscriptionPlanResponseCall.enqueue(new RetrofitCallback<SubscriptionPlanResponse>(mActivity, mProgressDialog) {
                @Override
                public void onSuccess(SubscriptionPlanResponse arg0) {
                    Utils.storeBoolean(mActivity, Constant.SUBSCRIPTION_TAKEN, true);
                    Log.e("SUBSCRIPTION TAKEN", ""+Utils.getBoolean(mActivity,Constant.SUBSCRIPTION_TAKEN));
                    Logger.showDialogAndPerformOnClickTakeButtonText(mActivity, "Done", arg0.getMessage(), (di, d) -> {
                        mAvailablePlansActContract.startNextActivity();
                    }, "Continue", false, null);
                }
            });
        }
    }

    public void submitPaymentOnline(SubscribePlanRequest request) {


        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity, false);
        SubscribePlanRequest mSubscribePlanRequest = request;
        Call<SubscriptionPlanResponse> mSubscriptionPlanResponseCall = SafeBioClient.getInstance().getApiInterface().subscribeToAPlan(Constant.subscribeToPlan(), mSubscribePlanRequest);
        mSubscriptionPlanResponseCall.enqueue(new RetrofitCallback<SubscriptionPlanResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(SubscriptionPlanResponse arg0) {
                Utils.storeBoolean(mActivity, Constant.SUBSCRIPTION_TAKEN, true);

                Logger.showDialogAndPerformOnClickTakeButtonText(mActivity, "Done", arg0.getMessage(), (di, d) -> {
                    mAvailablePlansActContract.startNextActivity();
                }, "Continue", false, null);
            }
        });

    }

}
