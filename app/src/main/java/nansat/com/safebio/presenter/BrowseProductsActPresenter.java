package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;

import nansat.com.safebio.contracts.BrowseProductActContract;
import nansat.com.safebio.models.CreateOrderRequest;
import nansat.com.safebio.models.CreateOrderResponse;
import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.ProductCategoryResponse;
import nansat.com.safebio.models.ProductsResponse;
import nansat.com.safebio.models.SendOrderEnquiryReq;
import nansat.com.safebio.models.UpdateOrderReponse;
import nansat.com.safebio.models.UpdateOrderRequest;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.ResponseModel;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 27/02/18.
 */

public class BrowseProductsActPresenter {

    BrowseProductActContract mBrowseProductActContract;
    Activity mActivity;

    public BrowseProductsActPresenter(BrowseProductActContract mBrowseProductActContract) {
        this.mBrowseProductActContract = mBrowseProductActContract;
        this.mActivity = mBrowseProductActContract.provideContext();
    }

    public void fetchCategories() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<ProductCategoryResponse> mProductCategoryResponseCall = SafeBioClient.getInstance().getApiInterface().getActiveProductCategories(Constant.getProductsCategories(), Utils.getString(mActivity, Constant.KEY_TOKEN));
        mProductCategoryResponseCall.enqueue(new RetrofitCallback<ProductCategoryResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(ProductCategoryResponse arg0) {
                mBrowseProductActContract.populateCategoriesRecView(arg0.getData());
            }
        });
    }

    public void fetchProductsByCategory(int catId) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<ProductsResponse> mProductsResponseCall = SafeBioClient.getInstance().getApiInterface().getProductsByCategoryId(Constant.getProductsByCategory(), Utils.getString(mActivity, Constant.KEY_TOKEN), catId);
        mProductsResponseCall.enqueue(new RetrofitCallback<ProductsResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(ProductsResponse arg0) {
                mBrowseProductActContract.populateProductsRecView(arg0.getData());
            }
        });
    }

    public void incrementValByFactor(int val, int factor, int position, int min_order) {
        mBrowseProductActContract.incrementValByFactor(val, factor, position, min_order);
    }

    public void decrementValByFactor(int val, int factor, int position, int min_order) {
        if (!(val <= min_order)) {
            mBrowseProductActContract.decrementValByFactor(val, factor, position, min_order);
        }
    }

    public void addToCart(int val, int factor, int position, int min_order, boolean addToCart) {
        if (addToCart) {
            mBrowseProductActContract.changeCartValueOfProduct(position, true);
            incrementValByFactor(val, 0, position, min_order);
        } else {
            mBrowseProductActContract.changeCartValueOfProduct(position, false);
            decrementValByFactor(val, 0, position, min_order);
        }
    }

    public void createOrder(CreateOrderRequest createOrderRequest) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<CreateOrderResponse> mCreateOrderCall = SafeBioClient.getInstance().getApiInterface().createOrder(Constant.createOrder(), createOrderRequest);
        mCreateOrderCall.enqueue(new RetrofitCallback<CreateOrderResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(CreateOrderResponse arg0) {
//                Logger.showDialogSetResultOkAndCloseOnClick(mActivity, "Done", "Your order has been created successfully.", null);
                mBrowseProductActContract.callInstamojoSDK("" + arg0.getData().getOrder_id());
            }
        });
    }

    public void sendOrderEnquiry(SendOrderEnquiryReq mSendOrderEnquiryReq) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<ResponseModel> mCreateOrderCall = SafeBioClient.getInstance().getApiInterface().sendOrderEnquiry(Constant.sendOrderEnquiry(), mSendOrderEnquiryReq);
        mCreateOrderCall.enqueue(new RetrofitCallback<ResponseModel>(mActivity,mProgressDialog) {
            @Override
            public void onSuccess(ResponseModel arg0) {
                Logger.showDialogSetResultOkAndCloseOnClick(mActivity, "Done", "Your enquiry has been submitted. We will get back to you soon.", null);
            }
        });
    }

    public void updateOrder(UpdateOrderRequest updateOrderRequest) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<UpdateOrderReponse> mCreateOrderCall = SafeBioClient.getInstance().getApiInterface().updateOrder(Constant.getUpdateOrderRequest(), updateOrderRequest);
        mCreateOrderCall.enqueue(new RetrofitCallback<UpdateOrderReponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(UpdateOrderReponse arg0) {
                Logger.showDialogSetResultOkAndCloseOnClick(mActivity, "Done", "Your order has been created successfully.", null);
            }
        });
    }

    public void viewTerms(String url){
        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public void getStates(){
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<LocationDTO> mLocationDTOCall=SafeBioClient.getInstance().getApiInterface().getStates(Constant.getStates());
        mLocationDTOCall.enqueue(new RetrofitCallback<LocationDTO>(mActivity,mProgressDialog) {
            @Override
            public void onSuccess(LocationDTO arg0) {
                mBrowseProductActContract.inflateStatesData(arg0);
            }
        });
    }
}
