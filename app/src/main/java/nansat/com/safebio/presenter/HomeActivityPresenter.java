package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;

import com.google.gson.Gson;

import nansat.com.safebio.contracts.HomeActContract;
import nansat.com.safebio.models.MyAccountResponse;
import nansat.com.safebio.models.SKUResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class HomeActivityPresenter {
    HomeActContract mHomeActContract;
    Activity mActivity;

    public HomeActivityPresenter(HomeActContract mHomeActContract) {
        this.mHomeActContract = mHomeActContract;
        this.mActivity = mHomeActContract.provideContext();
    }

    public void fetchSKUs() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<SKUResponse> mSkuResponseCall = SafeBioClient.getInstance().getApiInterface().getSKUApi(Constant.getSKUs(), Utils.getString(mActivity, Constant.KEY_TOKEN));
        mSkuResponseCall.enqueue(new RetrofitCallback<SKUResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(SKUResponse arg0) {
                mHomeActContract.saveSKUsToDb(arg0.getData());
            }
        });
    }

    public void fetchDetails() {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<MyAccountResponse> myAccountResponseCall = SafeBioClient.getInstance().getApiInterface().getMyAccountDetails(Constant.getMyAccountDetails(), Utils.getString(mActivity, Constant.KEY_TOKEN));
        myAccountResponseCall.enqueue(new RetrofitCallback<MyAccountResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(MyAccountResponse arg0) {
                if (!arg0.getData().isEmpty()) {
                    Utils.storeString(mActivity, Constant.KEY_HCF_DATA, new Gson().toJson(arg0.getData().get(0).getHcf()));
                }
            }
        });
    }
}
