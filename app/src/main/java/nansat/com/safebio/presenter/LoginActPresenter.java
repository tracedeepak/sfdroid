package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.TextUtils;

import com.google.gson.JsonObject;

import nansat.com.safebio.contracts.LoginActContract;
import nansat.com.safebio.models.LoginResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class LoginActPresenter {
    LoginActContract mLoginActContract;
    Activity mActivity;

    public LoginActPresenter(LoginActContract mLoginActContract) {
        this.mLoginActContract = mLoginActContract;
        this.mActivity = mLoginActContract.provideContext();
    }

    public void validateUserAndLogin(String username, String password) {
        if (TextUtils.isEmpty(username)) {
            Logger.toast(mActivity, "Please provide valid username");
        }
        if (TextUtils.isEmpty(password)) {
            Logger.toast(mActivity, "Please provide valid password");
        }
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            loginUserAndGetToken(username, password);
        }
    }

    public void loginUserAndGetToken(String username, String password) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        JsonObject mJsonObject = new JsonObject();
        mJsonObject.addProperty("username", username.trim());
        mJsonObject.addProperty("password", password.trim());
        Call<LoginResponse> mLoginActPresenterCall = SafeBioClient.getInstance().getApiInterface().getToken(Constant.getTokenApi(), mJsonObject);
        mLoginActPresenterCall.enqueue(new RetrofitCallback<LoginResponse>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(LoginResponse arg0) {
                if (arg0.getData() != null) {
                    Utils.storeString(mActivity,Constant.KEY_U,username);
                    Utils.storeString(mActivity,Constant.KEY_P,password);
                    Utils.storeString(mActivity, Constant.KEY_TOKEN, arg0.getData().getApiToken());
                    Utils.storeBoolean(mActivity, Constant.SUBSCRIPTION_TAKEN, arg0.getData().isHasSubscribed());
                    Utils.storeString(mActivity, Constant.KEY_USER_ID, "" + arg0.getData().getUserId());
                    Utils.storeString(mActivity, Constant.KEY_USER_ROLE, "" + arg0.getData().getRole());
                    Utils.storeInt(mActivity, Constant.KEY_USER_BELONGS_TO_HCF, arg0.getData().getRefId());
                    Utils.storeBoolean(mActivity, Constant.KEY_ACTIVATION_STATUS, arg0.getData().getConfirmed());
                    Utils.storeString(mActivity, Constant.KEY_USER_NAME, !TextUtils.isEmpty(arg0.getData().getName()) ? arg0.getData().getName() : "");
                    mLoginActContract.startNextActivity(arg0.getData().isHasSubscribed(), arg0.getData().getConfirmed());
                }
            }
        });
    }

    public void launchRegisterHCF() {
        mLoginActContract.launchRegisterHCF();
    }


}
