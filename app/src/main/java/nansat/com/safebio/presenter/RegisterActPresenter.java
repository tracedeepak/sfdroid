package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.TextUtils;

import com.google.gson.JsonObject;

import nansat.com.safebio.contracts.RegisterActContract;
import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.LoginResponse;
import nansat.com.safebio.models.RegisterHCFRequest;
import nansat.com.safebio.models.RegisterHCFResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class RegisterActPresenter {
    RegisterActContract mRegisterActContract;
    Activity mActivity;

    public RegisterActPresenter(RegisterActContract mRegisterActContract) {
        this.mRegisterActContract = mRegisterActContract;
        this.mActivity = mRegisterActContract.provideContext();
    }

    public void registerButtonClicked() {
        if (mRegisterActContract.checkValidation()) {
            ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
            RegisterHCFRequest mRegisterHCFRequest = mRegisterActContract.prepareRequest();
            Call<RegisterHCFResponse> mRegisterHCFResponseCall = SafeBioClient.getInstance().getApiInterface().registerNewHCF(Constant.registerHCF(), mRegisterHCFRequest);
            mRegisterHCFResponseCall.enqueue(new RetrofitCallback<RegisterHCFResponse>(mActivity, mProgressDialog) {
                @Override
                public void onSuccess(RegisterHCFResponse arg0) {
                    StringBuilder mStringBuilder = new StringBuilder();
                    mStringBuilder.append("Your username and password has been generated successfully. The same will be sent to your registered email.");
                    mStringBuilder.append("\n");
                    mStringBuilder.append("Username : " + arg0.getData().getUserName());
                    mStringBuilder.append("\n");
                    mStringBuilder.append("Password : " + arg0.getData().getPassword());

                    Logger.showDialogAndPerformOnClickTakeButtonText(mActivity, "Congratulations!", mStringBuilder.toString(), (dialogInterface, i) -> {
                        loginUserAndGetToken(arg0.getData().getUserName(), "" + arg0.getData().getPassword());
                    }, "Login", false, null);
                }
            });
        }
    }

    private void loginUserAndGetToken(String username, String password) {
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        JsonObject mJsonObject = new JsonObject();
        mJsonObject.addProperty("username", username.trim());
        mJsonObject.addProperty("password", password.trim());
        Call<LoginResponse> mLoginActPresenterCall = SafeBioClient.getInstance().getApiInterface().getToken(Constant.getTokenApi(), mJsonObject);
        mLoginActPresenterCall.enqueue(new RetrofitCallback<LoginResponse>(mActivity, mProgressDialog,true) {
            @Override
            public void onSuccess(LoginResponse arg0) {
                if (arg0.getData() != null) {
                    Utils.storeString(mActivity,Constant.KEY_U,username);
                    Utils.storeString(mActivity,Constant.KEY_P,password);
                    Utils.storeString(mActivity, Constant.KEY_TOKEN, arg0.getData().getApiToken());
                    Utils.storeBoolean(mActivity, Constant.SUBSCRIPTION_TAKEN, arg0.getData().isHasSubscribed());
                    Utils.storeString(mActivity, Constant.KEY_USER_ID, "" + arg0.getData().getUserId());
                    Utils.storeString(mActivity, Constant.KEY_USER_ROLE, "" + arg0.getData().getRole());
                    Utils.storeInt(mActivity, Constant.KEY_USER_BELONGS_TO_HCF, arg0.getData().getRefId());
                    Utils.storeString(mActivity, Constant.KEY_USER_EMAIL, arg0.getData().getEmail());
                    Utils.storeString(mActivity, Constant.KEY_USER_PHONE, arg0.getData().getMobile());
                    Utils.storeBoolean(mActivity,Constant.KEY_ACTIVATION_STATUS,arg0.getData().getConfirmed());
                    Utils.storeString(mActivity, Constant.KEY_USER_NAME, !TextUtils.isEmpty(arg0.getData().getName()) ? arg0.getData().getName() : "");
                    mRegisterActContract.startNextActivity();

                }
            }
        });
    }


    public void getStates(){
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<LocationDTO> mLocationDTOCall=SafeBioClient.getInstance().getApiInterface().getStates(Constant.getStates());
        mLocationDTOCall.enqueue(new RetrofitCallback<LocationDTO>(mActivity,mProgressDialog) {
            @Override
            public void onSuccess(LocationDTO arg0) {
                mRegisterActContract.inflateStatesData(arg0);
            }
        });
    }

}
