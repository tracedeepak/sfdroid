package nansat.com.safebio.presenter;

import android.app.Activity;

import nansat.com.safebio.contracts.SelectblueDeviceActContract;

/**
 * Created by Phantasmist on 10/02/18.
 */

public class SelectBluetoothDeviceActPresenter {
    SelectblueDeviceActContract mSelectblueDeviceActContract;
    Activity mActivity;
    public SelectBluetoothDeviceActPresenter(SelectblueDeviceActContract mSelectblueDeviceActContract) {
        this.mSelectblueDeviceActContract = mSelectblueDeviceActContract;
        this.mActivity=mSelectblueDeviceActContract.provideContext();
    }

    public void openNearbyDeviceDialog(){
        mSelectblueDeviceActContract.openNearbyDeviceDialog();
    }

}
