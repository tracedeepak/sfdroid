package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;

import nansat.com.safebio.contracts.SelectLotActContract;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 19/02/18.
 */

public class SelectLotActPresenter {
    SelectLotActContract mSelectLotActContract;
    Activity mActivity;
    public SelectLotActPresenter(SelectLotActContract mSelectLotActContract) {
        this.mSelectLotActContract = mSelectLotActContract;
        this.mActivity=mSelectLotActContract.provideContext();
    }


    public void fetchHcfLotList(int hcfId){
        ProgressDialog mProgressDialog= Logger.showProgressDialog(mActivity);
        Call<HCFLotResponse> mHcfLotResponseCall= SafeBioClient.getInstance().getApiInterface().getHCFLotListApi(Constant.getHcfLot(), Utils.getString(mActivity,Constant.KEY_TOKEN),hcfId);
        mHcfLotResponseCall.enqueue(new RetrofitCallback<HCFLotResponse>(mActivity,mProgressDialog) {
            @Override
            public void onSuccess(HCFLotResponse arg0) {
                mSelectLotActContract.addLotstoRecyclerView(arg0.getData());
            }
        });
    }

}
