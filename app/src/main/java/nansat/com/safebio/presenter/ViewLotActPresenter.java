package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.google.gson.Gson;

import nansat.com.safebio.contracts.ViewLotContract;
import nansat.com.safebio.database.LocalStorage;
import nansat.com.safebio.models.SaveLotRequest;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.ResponseModel;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 18/02/18.
 */

public class ViewLotActPresenter {

    ViewLotContract mViewLotContract;
    Activity mActivity;

    public ViewLotActPresenter(ViewLotContract mViewLotContract) {
        this.mViewLotContract = mViewLotContract;
        this.mActivity = mViewLotContract.provideContext();
    }

    public void saveLot(SaveLotRequest mSaveLotRequest) {
        Log.e("REQUEST TYPE", "SAVE LOT FIRST TIME"); //saved by hcf
        ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
        Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().saveLot(Constant.saveLot(), mSaveLotRequest);
        modelCall.enqueue(new RetrofitCallback<ResponseModel>(mActivity, mProgressDialog) {
            @Override
            public void onSuccess(ResponseModel arg0) {
                if (arg0.getError() == 0) {
                    mViewLotContract.deleteDataAndCloseActivity();
                }
            }
        });
    }

    public void depositLot(SaveLotRequest mSaveLotRequest) {
        Log.e("REQUEST TYPE", "DEPOSIT LOT"); // saved bydepositor
        if (Utils.isConnectedToInternet(mActivity)) {
            ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
            Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().receiveLot(Constant.receiveLot(), mSaveLotRequest);
            modelCall.enqueue(new RetrofitCallback<ResponseModel>(mActivity, mProgressDialog) {
                @Override
                public void onSuccess(ResponseModel arg0) {
                    if (arg0.getError() == 0) {
                        mViewLotContract.deleteDataAndCloseActivity();
                    }
                }
            });
        } else {
            LocalStorage.addPendingRequestsToQueue(mActivity, Constant.REQUEST_DEPOSIT, new Gson().toJson(mSaveLotRequest), System.currentTimeMillis());
            mViewLotContract.deleteDataAndCloseActivity();
        }
    }

    public void saveLotAndConfirm(SaveLotRequest mSaveLotRequest) {
        if (Utils.isConnectedToInternet(mActivity)) {
            Log.e("REQUEST TYPE", "SAVE LOT AND CONFIRM");// saved by cbtf pickup and he can also call save lot as well
            ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
            Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().saveLotAndConfirm(Constant.saveLotAndConfirm(), mSaveLotRequest);
            modelCall.enqueue(new RetrofitCallback<ResponseModel>(mActivity, mProgressDialog) {
                @Override
                public void onSuccess(ResponseModel arg0) {
                    if (arg0.getError() == 0) {
                        mViewLotContract.deleteDataAndCloseActivity();
                    }
                }
            });
        } else {
            LocalStorage.addPendingRequestsToQueue(mActivity, Constant.REQUEST_SAVE_LOT_AND_CONFIRM, new Gson().toJson(mSaveLotRequest), System.currentTimeMillis());
            mViewLotContract.deleteDataAndCloseActivity();
        }
    }
}
