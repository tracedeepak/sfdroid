package nansat.com.safebio.presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import nansat.com.safebio.contracts.WeightCaptureActContract;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

/**
 * Created by Phantasmist on 13/02/18.
 */

public class WeightCaptureActPresenter {
    WeightCaptureActContract mWeightCaptureActContract;
    Activity mActivity;

    public WeightCaptureActPresenter(WeightCaptureActContract mWeightCaptureActContract) {
        this.mWeightCaptureActContract = mWeightCaptureActContract;
        this.mActivity=mWeightCaptureActContract.provideContext();
    }

    public void scanAnother(){
        mWeightCaptureActContract.scanAnother();
    }

    public void saveAndScanAnother(){
//        mWeightCaptureActContract.saveToData(barcode);
    }

    public void onNext(){
        mWeightCaptureActContract.onNextPressed();
    }

    public void deleteBag(int position){
        Log.e("WEIGHT DELETE","POSITION");
        mWeightCaptureActContract.deleteBagAtPosition(position);
    }

    public void fetchHcfLotList(int hcfId, String barcode){
        if (Utils.isConnectedToInternet(mActivity)) {
            ProgressDialog mProgressDialog = Logger.showProgressDialog(mActivity);
            Call<HCFLotResponse> mHcfLotResponseCall = SafeBioClient.getInstance().getApiInterface().getHCFLotListApi(Constant.getHcfLot(), Utils.getString(mActivity, Constant.KEY_TOKEN), hcfId);
            mHcfLotResponseCall.enqueue(new RetrofitCallback<HCFLotResponse>(mActivity, mProgressDialog) {
                @Override
                public void onSuccess(HCFLotResponse arg0) {
                    mWeightCaptureActContract.setLotDetails(arg0.getData(),arg0,barcode);
                }
            });
        }
    }

}
