package nansat.com.safebio.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import nansat.com.safebio.database.LocalStorage;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.models.OfflineRequestsDTO;
import nansat.com.safebio.models.SaveLotRequest;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Utils;
import nansat.com.safebio.webservices.ResponseModel;
import nansat.com.safebio.webservices.RetrofitCallback;
import nansat.com.safebio.webservices.SafeBioClient;
import retrofit2.Call;

public class UpdatePendingRequestsService extends IntentService {
    public UpdatePendingRequestsService() {
        super("UpdatePendingRequestsService");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        OfflineRequestsDTO mOfflineRequestsDTO = LocalStorage.getExistingPendingRequests(getApplicationContext());

        for (OfflineRequestsDTO.PendingRequest pendingRequest : mOfflineRequestsDTO.getmPendingRequests()) {
            if (pendingRequest.getRequestType().equals(Constant.REQUEST_SAVE_LOT_AND_CONFIRM)) {
                SaveLotRequest mSaveLotRequest = new Gson().fromJson(pendingRequest.getRequestData(), SaveLotRequest.class);
                mSaveLotRequest.setApiToken(Utils.getString(getApplicationContext(),Constant.KEY_TOKEN));
                if (!mSaveLotRequest.getLotdetails().isEmpty()) {
                    String[] barcodeParts = mSaveLotRequest.getLotdetails().get(0).getScanCode().split("-");
                    if (barcodeParts.length == 3) {
                        String hcfId = barcodeParts[0].trim();
                        //CHECK FOR EXISTING LOT
                        fetchHcfLotList(mSaveLotRequest, Integer.valueOf(hcfId), pendingRequest);
                    }
                }
            } else if (pendingRequest.getRequestType().equals(Constant.REQUEST_DEPOSIT)) {
                SaveLotRequest mSaveLotRequest = new Gson().fromJson(pendingRequest.getRequestData(), SaveLotRequest.class);
                mSaveLotRequest.setApiToken(Utils.getString(getApplicationContext(),Constant.KEY_TOKEN));
                depositLot(mSaveLotRequest, pendingRequest);
            }
        }
    }


    public void depositLot(SaveLotRequest mSaveLotRequest, OfflineRequestsDTO.PendingRequest pendingRequest) {
        Log.e("REQUEST TYPE", "DEPOSIT LOT"); // saved bydepositor
        if (Utils.isConnectedToInternet(getApplicationContext())) {
            Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().receiveLot(Constant.receiveLot(), mSaveLotRequest);
            modelCall.enqueue(new RetrofitCallback<ResponseModel>(getApplicationContext(), null) {
                @Override
                public void onSuccess(ResponseModel arg0) {
                    if (arg0.getError() == 0) {
                        //remove pending request from queue
                        LocalStorage.updatePendingRequestStatus(getApplicationContext(), pendingRequest);
                    }
                }
            });
        }
    }

    public void saveLotAndConfirm(SaveLotRequest mSaveLotRequest, OfflineRequestsDTO.PendingRequest pendingRequest) {
        if (Utils.isConnectedToInternet(getApplicationContext())) {
            Log.e("REQUEST TYPE", "SAVE LOT AND CONFIRM");// saved by cbtf pickup and he can also call save lot as well
            Call<ResponseModel> modelCall = SafeBioClient.getInstance().getApiInterface().saveLotAndConfirm(Constant.saveLotAndConfirm(), mSaveLotRequest);
            modelCall.enqueue(new RetrofitCallback<ResponseModel>(getApplicationContext(), null) {
                @Override
                public void onSuccess(ResponseModel arg0) {
                    if (arg0.getError() == 0) {
                        LocalStorage.updatePendingRequestStatus(getApplicationContext(), pendingRequest);
                    }
                }
            });
        }
    }


    public void fetchHcfLotList(SaveLotRequest mSaveLotRequest, int hcfId, OfflineRequestsDTO.PendingRequest pendingRequest) {
        Log.e("REQUEST TYPE", "FETCH LOT");
        Call<HCFLotResponse> mHcfLotResponseCall = SafeBioClient.getInstance().getApiInterface().getHCFLotListApi(Constant.getHcfLot(), Utils.getString(getApplicationContext(), Constant.KEY_TOKEN), hcfId);
        mHcfLotResponseCall.enqueue(new RetrofitCallback<HCFLotResponse>(getApplicationContext(), null) {
            @Override
            public void onSuccess(HCFLotResponse arg0) {
                if (!arg0.getData().isEmpty()) {
                    mSaveLotRequest.setLotId(arg0.getData().get(0).getLotId());
                    saveLotAndConfirm(mSaveLotRequest, pendingRequest);
                } else {
                    saveLotAndConfirm(mSaveLotRequest, pendingRequest);
                }
            }

        });
    }
}
