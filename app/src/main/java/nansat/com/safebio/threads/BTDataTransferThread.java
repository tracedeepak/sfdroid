package nansat.com.safebio.threads;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Phantasmist on 10/02/18.
 */

public class BTDataTransferThread extends Thread {
    private final BluetoothSocket connectedBluetoothSocket;
    private final InputStream connectedInputStream;
    private final OutputStream connectedOutputStream;

    BTDataTransferListener mBtDataTransferListener;
    public interface BTDataTransferListener{
        public void dataReceivedUponConnection(String dataReceived,int bytes);
    }

    public BTDataTransferThread(BluetoothSocket socket,BTDataTransferListener mBtDataTransferListener) {
        this.mBtDataTransferListener=mBtDataTransferListener;
        this.connectedBluetoothSocket = socket;
        InputStream in = null;
        OutputStream out = null;
        Log.e("BTDataTransferThread","SOCKET INITIALISED"+connectedBluetoothSocket.isConnected());
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        connectedInputStream = in;
        connectedOutputStream = out;
    }

    @Override
    public void run() {
        Log.e("THEARD CONNECT","READING DATA");
        byte[] buffer = new byte[1024];
        int bytes;

        while (!Thread.currentThread().isInterrupted()) {
            try {
                bytes = connectedInputStream.read(buffer);
                String strReceived = new String(buffer, 0, bytes);
                final String msgReceived = strReceived;
                mBtDataTransferListener.dataReceivedUponConnection(msgReceived,bytes);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                try {
                    connectedInputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                final String msgConnectionLost = "Connection lost:\n"
                        + e.getMessage();
                Log.e("BT DATA THREAD",msgConnectionLost);
            }
        }
    }

    public void write(byte[] buffer) {
        try {
            connectedOutputStream.write(buffer);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void cancel() {
        try {
            connectedBluetoothSocket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
