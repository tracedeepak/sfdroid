package nansat.com.safebio.threads;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Phantasmist on 10/02/18.
 */

public class ConnectThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter mBluetoothAdapter;
    private final String TAG = "CONNECT THREAD";
    OnDeviceConnectedListener mOnDeviceConnectedListener;

    public interface OnDeviceConnectedListener {
        public void onDeviceConnected(BluetoothSocket mBluetoothSocket);
    }


    public ConnectThread(BluetoothDevice device, String uuid, BluetoothAdapter mBluetoothAdapter, OnDeviceConnectedListener onDeviceConnectedListener) {
        this.mBluetoothAdapter = mBluetoothAdapter;
        this.mOnDeviceConnectedListener = onDeviceConnectedListener;
        // Use a temporary object that is later assigned to mmSocket
        // because mmSocket is final.
        BluetoothSocket tmp = null;
        mmDevice = device;

        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            // MY_UUID is the app's UUID string, also used in the server code.
            tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(uuid));
        } catch (IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        this.mmSocket = tmp;
    }

    public void run() {
        // Cancel discovery because it otherwise slows down the connection.
        if (mBluetoothAdapter != null && mmSocket != null && mOnDeviceConnectedListener != null) {
            synchronized (this) {
                mBluetoothAdapter.cancelDiscovery();
                try {
                    // Connect to the remote device through the socket. This call blocks
                    // until it succeeds or throws an exception.
                    mmSocket.connect();
                    Log.e("DEVICE CONNECTED", "" + mmSocket.isConnected());
                } catch (IOException connectException) {
                    // Unable to connect; close the socket and return.
                    try {
                        mmSocket.close();
                    } catch (IOException closeException) {
                        Log.e(TAG, "Could not close the client socket", closeException);
                    }
                    return;
                }
                mOnDeviceConnectedListener.onDeviceConnected(mmSocket);

            }
            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
        }
    }

    // Closes the client socket and causes the thread to finish.
    public void cancel() {
        if (mmSocket != null) {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

}