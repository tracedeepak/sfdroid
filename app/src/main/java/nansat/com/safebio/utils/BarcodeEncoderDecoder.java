package nansat.com.safebio.utils;

import org.bouncycastle.openssl.PEMReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Decoder.BASE64Decoder;

/**
 * Created by Phantasmist on 21/02/18.
 */

public class BarcodeEncoderDecoder {


    public String getDecodedData(String encodedString) {
        BASE64Decoder mBase64Decoder = new BASE64Decoder();
        String privateKey = null;
        try {
            byte[] decodedKey = mBase64Decoder.decodeBuffer(privateKey);
            byte[] decodedStr = mBase64Decoder.decodeBuffer(encodedString);
            PrivateKey pk = strToPvtKey(new String(decodedKey));

            Cipher mCipher = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
            mCipher.init(Cipher.DECRYPT_MODE, pk);

            byte[] plainText = mCipher.doFinal(decodedStr);
            return new String(plainText);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            return null;
        } catch (BadPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

    private PrivateKey strToPvtKey(String s) {
        try {
            BufferedReader br = new BufferedReader(new StringReader(s));
            PEMReader pr = new PEMReader(br);
            KeyPair kp = (KeyPair) pr.readObject();
            pr.close();
            return kp.getPrivate();
        } catch (Exception e) {
            return null;
        }
    }

}
