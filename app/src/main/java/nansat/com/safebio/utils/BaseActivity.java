package nansat.com.safebio.utils;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import nansat.com.safebio.R;

/**
 * Created by Phantasmist on 07/02/18.
 */

public class BaseActivity extends AppCompatActivity {


    public void navigateToNextActivity(Intent intent, boolean isFinish) {
        startActivity(intent);
        if (isFinish)
            finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void navigateToPreviousActivity(Intent intent, boolean isFinish) {
        startActivity(intent);
        if (isFinish)
            finish();
        backAnimation();
    }

    public void navigateToNextActivityForResult(Intent intent, boolean isFinish,int requestCode) {
        startActivityForResult(intent,requestCode);
        if (isFinish)
            finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void backAnimation(){
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }

}
