package nansat.com.safebio.utils;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import nansat.com.safebio.R;
import nansat.com.safebio.SafeBioApplication;

/**
 * Created by Phantasmist on 24/02/18.
 */

public class BindDataUtils {
    public static Drawable getSkuColor(long skuId) {
        Log.e("DATA", "" + Integer.valueOf("" + skuId));
        switch (Integer.valueOf("" + skuId)) {
            case 1:
                return ContextCompat.getDrawable(SafeBioApplication.getInstance().getApplicationContext(), R.color.yellow);

            case 2:
                return ContextCompat.getDrawable(SafeBioApplication.getInstance().getApplicationContext(), R.color.red);

            case 3:
                return ContextCompat.getDrawable(SafeBioApplication.getInstance().getApplicationContext(), R.color.blue);
            case 4:
                return ContextCompat.getDrawable(SafeBioApplication.getInstance().getApplicationContext(), R.color.gray);

            default:
                return ContextCompat.getDrawable(SafeBioApplication.getInstance().getApplicationContext(), R.color.gray);
        }
    }


}
