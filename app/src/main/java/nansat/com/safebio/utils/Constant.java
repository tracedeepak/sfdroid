package nansat.com.safebio.utils;

/**
 * Created by Phantasmist on 07/02/18.
 */

public class Constant {

    public static final String KEY_ACTIVATION_STATUS = "activation_status";
    public static final String KEY_U = "u";
    public static final String KEY_P = "p";

    public static final int ORDER_STATUS_UNCONFIRMED = 1;
    public static final int ORDER_STATUS_CONFIRMED = 2;
    public static final int ORDER_STATUS_FULLFILLED = 3;


    static {
        System.loadLibrary("native-lib");
    }

    public static String SP_NAME = "safeBio";
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 10001;
    public static final String AUTHENTICATION_FAILED = "Unauthenticated";
    public static final String LOGIN_FAILED = "Login Failed";
    public static final String API_DATE_FORMATTER = "yyyy-MM-dd";
    public static final String API_DATE_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";
    public static final String EXPECTED_DATE_FORMATTER = "dd MMMM, yyyy";
    public static final String EXPECTED_DATE_DAY_TIME_FORMATTER = "dd/MM/yyyy EEEE, hh:mm a";
    public static final String EXPECTED_DATE_DAY_FORMATTER = "EEEE, dd MMMM yyyy";
    public static final String EVENT_EXPECTED_DATE_DAY_FORMATTER = "EEEE, MMMM dd";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String TIME_FORMAT = "hh:mm a";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm a";
    public static String KEY_TOKEN = "token";
    public static String KEY_USER_ID = "user_id";
    public static String KEY_USER_NAME = "user_name";
    public static String KEY_USER_ROLE = "user_role";
    public static String KEY_USER_EMAIL = "user_email";
    public static String KEY_USER_PHONE = "user_phone";
    public static String KEY_USER_BELONGS_TO_HCF = "user_hcf_id";
    public static String KEY_HCF_DATA = "hcf_data";
    public static final String UPDATE_CARD_TIME = "update_card_time";
    public static final String PENDING_REQUESTS = "pending_requests";


    public static String REQUEST_SAVE_LOT_AND_CONFIRM = "1";
    public static String REQUEST_DEPOSIT = "2";

    public static int PICKUP = 1;
    public static int DEPOSIT = 2;
    public static int SKIP = 3;
    //    API URLS
    public static String SAFEBIO_BASE = "http://api.safebio.in/";
    //    public static String SAFEBIO_BASE = "http://api-uat.safebio.in/";
    public static String DEFAULT_PHONE = "9416912139";
    //    USER ROLES
    public static String ROLE_ADMIN = "administrator";

    public static String ROLE_HCF = "hcf";
    public static String ROLE_CPWTF = "cpwtf";
    public static final String SUBSCRIPTION_TAKEN = "subscription_taken";


    public native static String getTokenApi();

    public native static String getSKUs();

    public native static String getHCFList();

    public native static String saveLot();

    public native static String saveLotAndConfirm();

    public native static String getHcfLot();

    public native static String receiveLot();

    public native static String getProductsByCategory();

    public native static String getProductsCategories();

    public native static String createOrder();

    public native static String registerHCF();

    public native static String getAvailableActivePlans();

    public native static String subscribeToPlan();

    public native static String getMyAccountDetails();

    public native static String getMyOrders();

    public native static String getReports();

    public native static String getUpdateOrderRequest();

    public native static String reportIssueReq();

    public native static String getStates();

    public native static String sendOrderEnquiry();


}
