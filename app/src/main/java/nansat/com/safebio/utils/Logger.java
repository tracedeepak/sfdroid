/*
 * Copyright (c) 2015. $user
 */

package nansat.com.safebio.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import nansat.com.safebio.BuildConfig;
import nansat.com.safebio.R;


/**
 * For displaying messages in logcat.
 * For displaying Dialog in common way.
 * For displaying toast and Snackbar.
 */
public class Logger {
    private static String TAG = "Logger";

    public static void setTag(String TAG) {
        Logger.TAG = TAG;
    }

    /**
     * Log.e()
     *
     * @param message which is display in logcat.
     */
    public static void e(String message) {
        if (BuildConfig.DEBUG)
            Log.e(Logger.TAG, message);
    }

    /**
     * Log.v()
     *
     * @param message which is display in logcat.
     */
    public static void v(String message) {
        if (BuildConfig.DEBUG)
            Log.v(Logger.TAG, message);
    }

    /**
     * Log.w()
     *
     * @param message which is display in logcat.
     */
    public static void w(String message) {
        if (BuildConfig.DEBUG)
            Log.w(Logger.TAG, message);
    }

    /**
     * Log.d()
     *
     * @param message which is display in logcat.
     */
    public static void d(String message) {
        if (BuildConfig.DEBUG)
            Log.d(Logger.TAG, message);
    }

    /**
     * Log.i()
     *
     * @param message which is display in logcat.
     */
    public static void i(String message) {
        if (BuildConfig.DEBUG)
            Log.i(Logger.TAG, message);
    }


    /**
     * Show Snakebar
     *
     * @param context Application/Activity context
     * @param message Message which is display in toast.
     */
    public static void toast(Context context, String message) {
        final Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        final View view = snackbar.getView();
        final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        final TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Dosis-Regular.ttf");
        tv.setTypeface(font);
        tv.setTextSize(16);
        tv.setTextColor(context.getResources().getColor(R.color.white));
        view.setLayoutParams(params);
        view.setBackgroundColor(context.getResources().getColor(R.color.black));
        snackbar.show();
    }

    /**
     * Show Default Toast
     *
     * @param context Application/Activity context
     * @param message Message which is display in toast.
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show Default dialog.
     *
     * @param context Application/Activity Context for creating dialog.
     * @param title   Title of dialog
     * @param message Message of dialog
     */
    public static void dialog(Context context, String title, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);

            builder.setPositiveButton(context.getString(android.R.string.ok), new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if (!((Activity) context).isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void showDialogAndCloseOnClick(final Activity mActivity, String title, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton(mActivity.getString(android.R.string.ok), new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mActivity.onBackPressed();
                }
            });

            if (!mActivity.isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void showDialogSetResultOkAndCloseOnClick(final Activity mActivity, String title, String message, Intent data) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton(mActivity.getString(android.R.string.ok), new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mActivity.setResult(Activity.RESULT_OK, data);
                    mActivity.finish();
                }
            });

            if (!mActivity.isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void showDialogAndPerformOnClick(Activity mActivity, String title, String message, DialogInterface.OnClickListener clickListener, boolean showNegativeButton, DialogInterface.OnClickListener negativeClickListener) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            if (showNegativeButton) {
                builder.setNegativeButton("Dismiss", negativeClickListener);
                builder.setPositiveButton("Report", clickListener);
            } else {
                builder.setPositiveButton(mActivity.getString(android.R.string.ok), clickListener);
            }
            if (!mActivity.isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void showDialogAndPerformOnClickustomText(Activity mActivity, String title, String message, DialogInterface.OnClickListener clickListener, boolean showNegativeButton, DialogInterface.OnClickListener negativeClickListener, String positiveButton, String negativeButton) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            if (showNegativeButton) {
                builder.setNegativeButton(negativeButton, negativeClickListener);
                builder.setPositiveButton(positiveButton, clickListener);
            } else {
                builder.setPositiveButton(mActivity.getString(android.R.string.ok), clickListener);
            }
            if (!mActivity.isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    public static void showDialogAndPerformOnClickTakeButtonText(Activity mActivity, String title, String message, DialogInterface.OnClickListener clickListener, String positiveButtonName, boolean showNegativeButton, DialogInterface.OnClickListener negativeClickListener) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            if (showNegativeButton) {
                builder.setNegativeButton("Dismiss", negativeClickListener);
                builder.setPositiveButton("Send Request", clickListener);
            } else {
                builder.setPositiveButton(positiveButtonName, clickListener);
            }
            if (!mActivity.isFinishing()) {
                builder.create().show();
            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
        }
    }

    /**
     * Show default dialog
     *
     * @param context Application/Activity Context
     * @param message Message of dialog
     */
    public static void dialog(Context context, String message) {
        dialog(context, context.getString(R.string.app_name), message);
    }

    /**
     * dismiss progress dialog if is visible.
     *
     * @param progressDialog
     */
    public static void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                //Crashlytics.logException(new Exception(e));
            }
        }
    }

    /**
     * Display progress dialog
     *
     * @param context
     * @return ProgressDialog
     */
    public static ProgressDialog showProgressDialog(Context context) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppTheme_ProgressDialog_Theme);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            return progressDialog;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ProgressDialog showProgressDialog(Context context, boolean isCancelable) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppTheme_ProgressDialog_Theme);
            progressDialog.setCancelable(isCancelable);
            progressDialog.setCanceledOnTouchOutside(isCancelable);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            return progressDialog;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
