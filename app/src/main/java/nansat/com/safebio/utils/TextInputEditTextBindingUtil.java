package nansat.com.safebio.utils;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

/**
 * Created by Phantasmist on 07/03/18.
 */

public class TextInputEditTextBindingUtil {
    @BindingAdapter({"app:validation", "app:errorMsg"})
    public static void setErrorEnable(TextInputLayout textInputLayout, StringRule stringRule,
                                      final String errorMsg) {
        textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("EMAIL",""+stringRule.validate(textInputLayout.getEditText().getText()));
                textInputLayout
                        .setErrorEnabled(stringRule.validate(textInputLayout.getEditText().getText()));
                if (stringRule.validate(textInputLayout.getEditText().getText())) {
                    textInputLayout.setError(errorMsg);
                } else {
                    textInputLayout.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


//        textInputLayout
//                .setErrorEnabled(stringRule.validate(textInputLayout.getEditText().getText()));
//        if (stringRule.validate(textInputLayout.getEditText().getText())) {
//            textInputLayout.setError(errorMsg);
//        } else {
//            textInputLayout.setError(null);
//        }
    }

    public static class Rule {

        public static StringRule NOT_EMPTY_RULE = s -> TextUtils.isEmpty(s.toString());
        public static StringRule EMAIL_RULE = s -> !Utils.isEmailValid(s.toString());
//        public static StringRule PHONE_NUM_RULE = s ->
    }

    public interface StringRule {

        boolean validate(Editable s);
    }

}
