package nansat.com.safebio.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nansat.com.safebio.R;
import nansat.com.safebio.SafeBioApplication;
import nansat.com.safebio.widgets.AppEditText;
import okhttp3.Cache;


public class Utils {

    static final long THOU = 1000L;
    static final long LAKH = 100000L;
    static final long CRORE = 10000000L;
    static final long ARAB = 1000000000L;


    public static final int SECOND_MILLIS = 1000;
    public static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final String REQUIRED_MSG = "required";


    /**
     * Check that network mobile data or 3G is available or not.
     *
     * @param context
     * @return boolean
     */
    public static boolean DEBUG = false;

    public static final boolean isConnectedToInternet(Context context) {
        if (context != null) {
            final ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (mgr != null) {

                final NetworkInfo mobileInfo = mgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                final NetworkInfo wifiInfo = mgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (wifiInfo != null && wifiInfo.isAvailable() && wifiInfo.isAvailable() && wifiInfo.isConnected()) {

                    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    final WifiInfo wifiInfoStatus = wifiManager.getConnectionInfo();
                    final SupplicantState supState = wifiInfoStatus.getSupplicantState();

                    if (String.valueOf(supState).equalsIgnoreCase("COMPLETED") || String.valueOf(supState).equalsIgnoreCase("ASSOCIATED")) {
                        // WiFi is connected
                        return true;
                    }
                }

                if (mobileInfo != null && mobileInfo.isAvailable() && mobileInfo.isConnected()) {
                    // Mobile Network is connected
                    return true;
                }
            }
        }
        return false;
    }


    public static String Xlat(long val) {
        if (val < THOU)
            return Long.toString(val);
        if (val < CRORE)
            return makeDecimal(val, LAKH, "L");
        if (val < ARAB)
            return makeDecimal(val, CRORE, "Cr");
        if (val < ARAB)
            return makeDecimal(val, ARAB, "Arb");
        return makeDecimal(val, LAKH, "L");
    }

    static private String makeDecimal(long val, long div, String sfx) {
        val = val / (div / 10);
        long whole = val / 10;
        long tenths = val % 10;
        if ((tenths == 0) || (whole >= 10))
            return String.format("\u20B9 %d%s", whole, sfx);
        return String.format("\u20B9 %d.%d%s", whole, tenths, sfx);
    }

    public static String convertToDefaultCurrencyFormat(long amountToConvert) {
        DecimalFormat IndianCurrencyFormat = new DecimalFormat("##,##,##,###");
//        NumberFormat formatter = NumberFormat.getCurrencyInstance( new Locale("", "in"));
        String moneyString = IndianCurrencyFormat.format(Double.valueOf(amountToConvert));
        return "\u20B9 " + moneyString;
    }

    /**
     * Check email is valid or not.
     *
     * @param email
     * @return boolean
     */
    public static final boolean isEmailValid(String email) {

        boolean isValid = false;
        String expression = "^[\\w\\.\\s-]+@([\\w\\-]+\\.)+[A-Z\\s]{2,6}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Check String is empty or not.
     *
     * @param message
     * @return boolean
     */
    public static final boolean isStringEmpty(String message) {
        boolean isValid = false;
        if (message.isEmpty()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Store string value in shared preference.
     *
     * @param context
     * @param key
     * @param value
     */
    public static final void storeString(Context context, String key, String value) {
        if (context!=null ) {
            final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.commit();
            Log.e("CONTEXT IS","NOT NULL");
        }else{
            Log.e("CONTEXT IS","NULL");
        }
    }

    public static final void emptyPreferences(Context context) {
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public static final void deleteKey(Context context, String key) {
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, null);
        editor.commit();
    }

    /**
     * Store integer value in shared preference.
     *
     * @param context
     * @param key
     * @param value
     */
    public static final void storeInt(Context context, String key, int value) {
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    /**
     * Store integer value in shared preference.
     *
     * @param context
     * @param key
     * @param value
     */
    public static final void storeLong(Context context, String key, long value) {
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * Get string value from shared preference.
     *
     * @param context
     * @param key
     * @return String
     */
    public static final String getString(Context context, String key) {
        if (context == null) {
            context= SafeBioApplication.getInstance().getApplicationContext();
        }
        String data;
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getString(key, "");
        editor.commit();
        return data;
    }

    /**
     * Get integer value from shared preference.
     *
     * @param context
     * @param key
     * @return integer
     */
    public static final int getInt(Context context, String key) {
        int data;
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getInt(key, 0);
        editor.commit();
        return data;
    }

    /**
     * Get integer value from shared preference.
     *
     * @param context
     * @param key
     * @return long
     */
    public static final long getLong(Context context, String key) {
        long data;
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getLong(key, 0);
        editor.commit();
        return data;
    }

    /**
     * Store boolean value from shared preference.
     *
     * @param context
     * @param key
     * @param value
     */
    public static final void storeBoolean(Context context, String key, boolean value) {
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Get boolean from shared preference with specified key.
     *
     * @param context
     * @param key
     * @return boolean
     */
    public static final boolean getBoolean(Context context, String key) {
        boolean data;
        final SharedPreferences preferences = context.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getBoolean(key, false);
        editor.commit();
        return data;
    }

    /**
     * Hide keyboard if it is visible
     *
     * @param context
     */
    public static final void hideSoftKeyboard(Context context) {
        final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (((Activity) context).getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    /**
     * Device is tablet or not.
     *
     * @param context
     * @return boolean
     */
    public static final boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Get unique deviceId
     *
     * @param context
     * @return String
     */
    public static final String getUniqueDeviceId(Context context) {

        String deviceId = null;
        // 1 compute IMEI
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = TelephonyMgr.getDeviceId(); // Requires // READ_PHONE_STATE

        if (deviceId == null) {
            // 2 android ID - unreliable
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        if (deviceId == null) {
            // 3 compute DEVICE ID
            deviceId = "35"
                    + // we make this look like a valid IMEI
                    Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
                    + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10 + Build.USER.length() % 10; // 13
            // digits
        }
        return deviceId;
    }

    public static boolean hasCallingFeature(Context context) {
        boolean flag;
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        switch (telephonyManager.getPhoneType()) {
            case TelephonyManager.PHONE_TYPE_CDMA:
                flag = true;
                break;
            case TelephonyManager.PHONE_TYPE_GSM:
                flag = true;
                break;
            default:
                flag = false;
                break;
        }
        return flag;
    }

//    public static boolean checkPlayServices(Activity activity) {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                apiAvailability.getErrorDialog(activity, resultCode, Constant.PLAY_SERVICES_RESOLUTION_REQUEST)
//                        .show();
//            } else {
//                Logger.i("This device is not supported.");
//                activity.finish();
//            }
//            return false;
//        }
//        return true;
//    }

    public static Spanned getStylishTitle(String title1, String title2) {
        final String title = String.format(Locale.getDefault(), "<font color='#FFFFFF'>%s</font> <font color='#00b372'>%s</font>", title1, title2);
        return Html.fromHtml(title);
    }

    public static String getFormattedDate(String time) {
        final String expectedTime = "";
        Date date = null;

        final SimpleDateFormat apiSimpleDateFormat = new SimpleDateFormat(Constant.API_DATE_FORMATTER);
        final SimpleDateFormat expectedSimpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER);

        try {
            date = apiSimpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null)
            return expectedSimpleDateFormat.format(date);
        else
            return expectedTime;
    }

    public static String getFormattedDateTime(String time) {
        final String expectedTime = "";
        Date date = null;

        final SimpleDateFormat apiSimpleDateFormat = new SimpleDateFormat(Constant.API_DATE_FORMATTER);
        final SimpleDateFormat expectedSimpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_DAY_FORMATTER);

        try {
            date = apiSimpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null)
            return expectedSimpleDateFormat.format(date);
        else
            return expectedTime;
    }

    public static String getEventFormattedDateTime(String time) {
        final String expectedTime = "";
        Date date = null;

        final SimpleDateFormat apiSimpleDateFormat = new SimpleDateFormat(Constant.API_DATE_FORMATTER);
        final SimpleDateFormat expectedSimpleDateFormat = new SimpleDateFormat(Constant.EVENT_EXPECTED_DATE_DAY_FORMATTER);

        try {
            date = apiSimpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null)
            return expectedSimpleDateFormat.format(date);
        else
            return expectedTime;
    }

    public static String getFormattedDayTime(String time) {
        final String expectedTime = "";
        Date date = null;
        final DateFormat apiSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        final SimpleDateFormat expectedSimpleDateFormat = new SimpleDateFormat(Constant.EVENT_EXPECTED_DATE_DAY_FORMATTER);
        DateTime mFetchedDateTime = new DateTime(time);
        try {
            date = apiSimpleDateFormat.parse(mFetchedDateTime.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null)
            return expectedSimpleDateFormat.format(date);
        else
            return expectedTime;
    }


    public static String lastN(String x, int n) {
        return x.substring(x.length() - n, x.length());
    }



    public static File getDirectory(final Context context) {
        final String root = Environment.getExternalStorageDirectory().toString();
        final File dir = new File(root + "/Android/data/" + context.getPackageName());

        if (!dir.exists()) {
            dir.mkdir();
        }

        final File shareDir = new File(dir + "/share/");

        if (!shareDir.exists()) {
            shareDir.mkdir();
        }

        return shareDir;
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS || diff < SECOND_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 60 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 120 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    public static String getDateTime(String dateTime) {
        String time = "";
        try {
            if (!TextUtils.isEmpty(dateTime)) {
                final TimeZone timezone = TimeZone.getTimeZone("UTC");

                final SimpleDateFormat requiredDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_DAY_TIME_FORMATTER, Locale.getDefault());
                final SimpleDateFormat apiDateFormat = new SimpleDateFormat(Constant.API_DATE_TIME_FORMATTER, Locale.getDefault());
                apiDateFormat.setTimeZone(timezone);
                time = requiredDateFormat.format(apiDateFormat.parse(dateTime));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    public static long getLongInTime(String time) {
        long timeInMillis = 0;
        if (!TextUtils.isEmpty(time)) {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat(Constant.EXPECTED_DATE_DAY_TIME_FORMATTER);
                Date date = sdf.parse(time);
                timeInMillis = date.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return timeInMillis;
    }

    public static String getCurrencySymbol(Context context, final String currency) {
        String symbol = "";
        if (currency.equalsIgnoreCase("Rs.")) {
            symbol = context.getString(R.string.rs);
        } else {
            symbol = currency;
        }
        return symbol;
    }

    public static Cache getCache(Context context) {
        final File httpCacheDirectory = new File(context.getCacheDir(), "responses");
        int cacheSize = 25 * 1024 * 1024;
        return new Cache(httpCacheDirectory, cacheSize);
    }

    public static String getUpparCaseString(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    public static void setText(TextView textView, String value) {
        String amount;
        if (!TextUtils.isEmpty(value)) {
            amount = String.format(Locale.getDefault(), "%s %s", textView.getContext().getString(R.string.rs), value);
        } else {
            amount = String.format(Locale.getDefault(), "%s 0", textView.getContext().getString(R.string.rs));
        }
        textView.setText(amount);
    }

    public static boolean hasText(AppEditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

    /***
     *
     * Convert To Date Time
     *
     *
     * */

    public static String convertToLocalTime(String fetchedDateString) {
        try {
            Log.e("Fetched Date", fetchedDateString);
            DateTimeZone utcTimeZone = DateTimeZone.UTC;
            DateTime utcTime = new DateTime(fetchedDateString);
            Log.e("Orig", "" + utcTime.getZone().getID());

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            DateFormat localFormat = new SimpleDateFormat("dd MMM yyyy  \nhh:mm aa");
            utcFormat.setTimeZone(TimeZone.getDefault());
            Date timestamp = null;

            /**For Local time reference */
            Date localTime = new Date(System.currentTimeMillis());
            try {
                /**Convert time To UTC**/
                timestamp = utcFormat.parse(utcTime.toString());

                /**Convert time from  UTC to Local Time by adding local time to UTC **/
                String output = localFormat.format(timestamp.getTime());
                return output;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static String makeFullName(String firstName, String middleName, String lastName) {
        String a = "";
        String b = "";
        String c = "";
        if (!TextUtils.isEmpty(firstName))
            a = firstName;
        if (!TextUtils.isEmpty(middleName))
            b = middleName;
        if (!TextUtils.isEmpty(lastName))
            c = lastName;

        return a + " " + b + " " + c;
    }

    public static int getCorrespondingColorToText(String status) {
        try {
            if(status != null) {
                if (status.toLowerCase().equals("new")) {
                    return R.color.newc;
                }
                if (status.toLowerCase().equals("cold")) {
                    return R.color.cold;
                }
                if (status.toLowerCase().equals("warm")) {
                    return R.color.warm;
                }
                if (status.toLowerCase().equals("hot")) {
                    return R.color.hot;
                }
                if (status.toLowerCase().equals("not interested")) {
                    return R.color.notintrested;
                }
                if (status.toLowerCase().equals("win")) {
                    return R.color.win;
                }
                if (status.toLowerCase().equals("return")) {
                    return R.color.returnc;
                }
                if (status.toLowerCase().equals("dead")) {
                    return R.color.dead;
                } else {
                    return R.color.colorAccent;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public static String loadJSONFromAsset(Context mContext, String fileName) {
        String json = null;
        try {

            InputStream is = mContext.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

//    public static void powerFireBase(final Activity mActivity) {
//        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
//
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.e("Snapshot", "" + dataSnapshot.getValue(UpdateNotification.class).getNewVersion());
//                Log.e("BUILD", "" + BuildConfig.VERSION_NAME);
//
//                String changes[] = dataSnapshot.getValue(UpdateNotification.class).getChangeLog().split(",");
//
//                StringBuilder sb = new StringBuilder();
//                sb.append("\n");
//                for (int i = 0; i < changes.length; i++) {
//                    sb.append(changes[i]);
//                    sb.append("\n");
//                }
//
//                if (!BuildConfig.VERSION_NAME.equals(dataSnapshot.getValue(UpdateNotification.class).getNewVersion())) {
//                    if (mActivity.hasWindowFocus()) {
//                        new MaterialDialog.Builder(mActivity).title("New Update Available").content(dataSnapshot.getValue(UpdateNotification.class).getMessage() + "\nChange Log:" + sb.toString()).cancelable(false).theme(Theme.LIGHT).positiveText("All right!").callback(new MaterialDialog.ButtonCallback() {
//                            @Override
//                            public void onPositive(MaterialDialog dialog) {
//                                super.onPositive(dialog);
//                                dialog.dismiss();
//                                mActivity.finish();
//                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=in.squareconnect"));
//                                mActivity.startActivity(intent);
//                            }
//                        }).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }




    public static int getStatusBarHeight(Context mContext) {
        int result = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}
