package nansat.com.safebio.webservices;

public class APIConstant {

    public static final String CONNECTION_TIMEOUT = "Connection Timeout";
    public static final String NO_INTERNET_AVAILABLE = "No Internet Connection Available";
    public static final String SERVER_NOT_RESPONDING = "Server is not responding.Please try again later.";
    public static final String UNEXPECTED_RESULT = "Unexpected result. Unable to parse response from server";
    public static final String SOMETHING_WENT_WRONG = "Something went wrong.Please try again later.";
    public static final String OFFLINE_DATA_MSG = "Date is not cache for offline. Please enable internet and visit this screen again for offline use.";
    public static final String POST_OFFLINE_DATA_MSG = "Post request are not allowed to cache. Please enable internet";
}
