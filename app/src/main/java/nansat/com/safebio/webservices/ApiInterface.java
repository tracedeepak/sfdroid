package nansat.com.safebio.webservices;

import com.google.gson.JsonObject;

import nansat.com.safebio.models.AvailablePlansResponse;
import nansat.com.safebio.models.CpwtfAccountResponse;
import nansat.com.safebio.models.CreateOrderRequest;
import nansat.com.safebio.models.CreateOrderResponse;
import nansat.com.safebio.models.HCFLotResponse;
import nansat.com.safebio.models.HcfResponse;
import nansat.com.safebio.models.LocationDTO;
import nansat.com.safebio.models.LoginResponse;
import nansat.com.safebio.models.MyAccountResponse;
import nansat.com.safebio.models.MyOrdersResponse;
import nansat.com.safebio.models.ProductCategoryResponse;
import nansat.com.safebio.models.ProductsResponse;
import nansat.com.safebio.models.RegisterHCFRequest;
import nansat.com.safebio.models.RegisterHCFResponse;
import nansat.com.safebio.models.ReportIssueRequest;
import nansat.com.safebio.models.ReportsResponse;
import nansat.com.safebio.models.SKUResponse;
import nansat.com.safebio.models.SaveLotRequest;
import nansat.com.safebio.models.SendOrderEnquiryReq;
import nansat.com.safebio.models.SubscribePlanRequest;
import nansat.com.safebio.models.SubscriptionPlanResponse;
import nansat.com.safebio.models.UpdateOrderReponse;
import nansat.com.safebio.models.UpdateOrderRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Phantasmist on 07/02/18.
 */

public interface ApiInterface {
    @POST
    Call<LoginResponse> getToken(@Url String url, @Body JsonObject mJsonObject);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<SKUResponse> getSKUApi(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<HcfResponse> getHCFListApi(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<HCFLotResponse> getHCFLotListApi(@Url String url, @Query("api_token") String apiToken, @Query("id") int hcfId);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ProductCategoryResponse> getActiveProductCategories(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ProductsResponse> getProductsByCategoryId(@Url String url, @Query("api_token") String apiToken, @Query("categoryId") int id);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<AvailablePlansResponse> getAvailableActivePlans(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<MyAccountResponse> getMyAccountDetails(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<CpwtfAccountResponse> getCpwtfAccountDetails(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<ResponseModel> saveLot(@Url String url, @Body SaveLotRequest mSaveLotRequest);

    @POST
    Call<ResponseModel> receiveLot(@Url String url, @Body SaveLotRequest mSaveLotRequest);


    @POST
    Call<ResponseModel> saveLotAndConfirm(@Url String url, @Body SaveLotRequest mSaveLotRequest);


    @POST
    Call<CreateOrderResponse> createOrder(@Url String url, @Body CreateOrderRequest mSaveLotRequest);


    @POST
    Call<UpdateOrderReponse> updateOrder(@Url String url, @Body UpdateOrderRequest mUpdateOrderRequest);


    @POST
    Call<RegisterHCFResponse> registerNewHCF(@Url String url, @Body RegisterHCFRequest mRegisterHCFRequest);


    @POST
    Call<SubscriptionPlanResponse> subscribeToAPlan(@Url String url, @Body SubscribePlanRequest mSubscribe);


    @POST
    Call<MyOrdersResponse> getMyOrdersApi(@Url String url, @Query("api_token") String apiToken);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ReportsResponse> getReports(@Url String url, @Query("api_token") String apiToken, @Query("start_date") String startDate, @Query("end_date") String endDate, @Query("status") String status);


    @POST
    Call<ResponseModel> reportIssue(@Url String url, @Body ReportIssueRequest mReportIssueRequest);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<LocationDTO> getStates(@Url String url);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<ResponseModel> sendOrderEnquiry(@Url String url, @Body SendOrderEnquiryReq mSendOrderEnquiryReq);
}
