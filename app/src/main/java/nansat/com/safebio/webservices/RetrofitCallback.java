package nansat.com.safebio.webservices;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.common.util.IOUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import nansat.com.safebio.activities.LoginActivity;
import nansat.com.safebio.models.ErrorResponse;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Logger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitCallback<T> implements Callback<T> {

    private ProgressDialog progressDialog;
    private Context context;
    private boolean validateResponse = true;

    public RetrofitCallback(Context c, ProgressDialog dialog) {
        progressDialog = dialog;
        context = c;
    }

    public RetrofitCallback(Context context, ProgressDialog progressDialog, boolean validateResponse) {
        this.progressDialog = progressDialog;
        this.context = context;
        this.validateResponse = validateResponse;
    }

    public abstract void onSuccess(T arg0);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (progressDialog != null) {
            if (!((Activity) context).isFinishing()) {
                Logger.dismissProgressDialog(progressDialog);
            }
        }
        if (!call.isCanceled()) {
            if (response.isSuccessful() && response.code() == 200) {
                Log.e("VALIDATING RESPONSE", "" + validateResponse);
                if (validateResponse) {
                    final Gson gson = new Gson();
                    final String responseString = gson.toJson(response.body());
                    final ResponseModel responseModel = gson.fromJson(responseString, ResponseModel.class);
                    if (responseModel.getError() == 0) {
                        onSuccess(response.body());
                    } else {
                        Logger.dialog(context, responseModel.getMessage());
                    }
                } else {

                    onSuccess(response.body());
                }

            } else if (response.code() == 401) {
                final Gson gson = new Gson();
                String data = response.message();
                String data2 = null;
                try {
                    data2 = new String(IOUtils.toByteArray(response.errorBody().byteStream()), "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String responseString = data2;
                final ResponseModel responseModel = gson.fromJson(responseString, ResponseModel.class);

                if (responseModel != null && responseModel.getMessage() != null && responseModel.getMessage().equalsIgnoreCase(Constant.AUTHENTICATION_FAILED)) {
                    if (context instanceof Activity) {
                        Logger.showDialogAndPerformOnClickTakeButtonText((Activity) context, "Token Expired", "You need to re login into app", (di, i) -> {
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(intent);
                            return;
                        }, "Ok", false, null);

                    } else if (context instanceof Application) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                        return;
                    }
                }
                if (responseModel != null && responseModel.getMessage() != null && responseModel.getMessage().equalsIgnoreCase(Constant.LOGIN_FAILED) && context instanceof Activity) {
                    Logger.showDialogAndPerformOnClickTakeButtonText((Activity) context, "Login Failed", "Please login with correct email and password.", (di, i) -> {
                        di.dismiss();
                        return;
                    }, "Ok", false, null);

                }
            } else if (response.code() == 504) {
                if (call.request().method().equalsIgnoreCase("GET"))
                    Logger.dialog(context, APIConstant.OFFLINE_DATA_MSG);
                else if (call.request().method().equalsIgnoreCase("POST"))
                    Logger.dialog(context, APIConstant.POST_OFFLINE_DATA_MSG);
            } else if (response.code() == 500) {
                try {
                    ErrorResponse model= new Gson().fromJson(response.errorBody().string(),ErrorResponse.class);
                    Logger.dialog(context, new GsonBuilder().disableHtmlEscaping().create().toJson(model.getMessage().toString()));
                }catch (Exception e){
                    Log.e("Exception",e.getLocalizedMessage());
                }
            } else {
                Logger.dialog(context, response.message());
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable error) {
        if (!call.isCanceled()) {
            String errorMsg;
            error.printStackTrace();

            if (error instanceof SocketTimeoutException) {
                errorMsg = APIConstant.CONNECTION_TIMEOUT;
            } else if (error instanceof UnknownHostException) {
                errorMsg = APIConstant.NO_INTERNET_AVAILABLE;
            } else if (error instanceof ConnectException) {
                errorMsg = APIConstant.SERVER_NOT_RESPONDING;
            } else if (error instanceof JSONException || error instanceof JsonSyntaxException) {
                errorMsg = APIConstant.UNEXPECTED_RESULT;
            } else {
                errorMsg = APIConstant.SOMETHING_WENT_WRONG;
            }

            if ((context instanceof Activity)) {

                if (!((Activity) context).isFinishing()) {
                    Logger.dismissProgressDialog(progressDialog);
                }
                Logger.dialog(context, errorMsg);
            }
        }
    }
}

