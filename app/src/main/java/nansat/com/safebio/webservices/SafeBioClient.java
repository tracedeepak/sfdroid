package nansat.com.safebio.webservices;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import nansat.com.safebio.BuildConfig;
import nansat.com.safebio.utils.Constant;
import nansat.com.safebio.utils.Utils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Phantasmist on 17/02/18.
 */

public class SafeBioClient {
    private static SafeBioClient instance;
    private ApiInterface apiInterface;
    private static Context context;

    public SafeBioClient(final Context context) {
        instance = this;
        SafeBioClient.context = context;


        final Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ItemTypeAdapterFactory()).setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'").create();
        final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(120, TimeUnit.SECONDS);
        okHttpClient.writeTimeout(120, TimeUnit.SECONDS);
        okHttpClient.readTimeout(120, TimeUnit.SECONDS);
        okHttpClient.cache(Utils.getCache(context));

        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(interceptor);
        }

        okHttpClient.addInterceptor(offlineInterceptor);
        okHttpClient.addNetworkInterceptor(onlineInterceptor);

        final Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constant.SAFEBIO_BASE);

        retrofitBuilder.client(okHttpClient.build());
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson));

        final Retrofit retrofit = retrofitBuilder.build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    public static SafeBioClient getInstance() {
        return instance;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }

    private static final Interceptor onlineInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            final Response originalResponse = chain.proceed(chain.request());

            String cacheControl = originalResponse.header("Cache-Control");
            if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains("no-cache") ||
                    cacheControl.contains("must-revalidate") || cacheControl.contains("max-age=0")) {
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + 10)
                        .build();
            }

            return originalResponse;
        }
    };

    private static final Interceptor offlineInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (!Utils.isConnectedToInternet(context)) {
                request = request.newBuilder().header("Cache-Control", "public, max-age" + 10).build();
            }
            return chain.proceed(request);
        }
    };
}
