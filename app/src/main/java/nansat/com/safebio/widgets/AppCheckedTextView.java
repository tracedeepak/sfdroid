package nansat.com.safebio.widgets;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AppCheckedTextView extends RelativeLayout implements View.OnClickListener {

    private TextView nameTextView;
    private CheckBox checkBox;
    private onCheckedListener onCheckedListener;

    public AppCheckedTextView(Context context) {
        super(context);
    }

    public AppCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppCheckedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void setName(String name) {
        nameTextView.setText(name);
    }

    public void setCheckBox(boolean flag) {
        checkBox.setChecked(flag);
    }

    public boolean isChecked() {
        return checkBox.isChecked();
    }

    @Override
    public void onClick(View view) {
        checkBox.setChecked(!isChecked());
        if (onCheckedListener != null) {
            onCheckedListener.onCheck(checkBox.isChecked(), view);
        }
    }

    public void setOnCheckedListener(AppCheckedTextView.onCheckedListener onCheckedListener) {
        this.onCheckedListener = onCheckedListener;
    }

    public interface onCheckedListener {
        void onCheck(boolean flag, View view);
    }
}
