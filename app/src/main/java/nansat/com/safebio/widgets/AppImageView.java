
package nansat.com.safebio.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import nansat.com.safebio.R;
import nansat.com.safebio.utils.PicassoBigCache;


public class AppImageView extends FrameLayout implements Target {

    private ImageView imageView;
    private ImageView placeHolderImageView;
    private ProgressBar progressBar;
    private Context context;
    private boolean isGradient = false;
    private LayoutParams imageParams;
    private OnLoadBitmap onLoadBitmap;
    private boolean isCenterCrop = true;

    public void setOnLoadBitmap(OnLoadBitmap onLoadBitmap) {
        this.onLoadBitmap = onLoadBitmap;
    }

    public AppImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public AppImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AppImageView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context c) {
        this.context = c;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        imageView = new ImageView(context);
        imageView.setAdjustViewBounds(true);
        placeHolderImageView = new ImageView(context);
        placeHolderImageView.setImageResource(R.drawable.ic_launcher_background);

        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleSmall);
        progressBar.setIndeterminate(true);
        final LayoutParams progressParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        progressParams.gravity = Gravity.CENTER;

        imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        imageParams.gravity = Gravity.CENTER;

        final LayoutParams placeHolderParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        placeHolderParams.gravity = Gravity.CENTER;

        addView(placeHolderImageView, placeHolderParams);
        addView(imageView, imageParams);
        addView(progressBar, progressParams);

    }

    public void loadImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            PicassoBigCache.INSTANCE.getPicassoBigCache(context).load(url)
                    .priority(Picasso.Priority.NORMAL)
                    .config(Bitmap.Config.RGB_565)
                    .into(this);
        } else {
            removeView(progressBar);
            removeView(imageView);
            if (isGradient && getChildCount() == 1) {
                final View gradientView = new View(context);
                gradientView.setBackgroundResource(R.drawable.gradient_drawable);
                addView(gradientView, imageParams);
            }
        }
    }

    public void loadImageWithCallBack(String url, final Button mButton) {
        if (!TextUtils.isEmpty(url)) {
            PicassoBigCache.INSTANCE.getPicassoBigCache(context).load(url)
                    .priority(Picasso.Priority.NORMAL)
                    .config(Bitmap.Config.RGB_565)
                    .fit()
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            mButton.setVisibility(View.VISIBLE);
                            removeView(progressBar);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            removeView(progressBar);
            removeView(imageView);
            if (isGradient && getChildCount() == 1) {
                final View gradientView = new View(context);
                gradientView.setBackgroundResource(R.drawable.gradient_drawable);
                addView(gradientView, imageParams);
            }
        }
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        removeView(progressBar);
        removeView(placeHolderImageView);
        imageView.setScaleType(ScaleType.FIT_XY);

        if (isCenterCrop)
            imageView.setScaleType(ScaleType.FIT_XY);

        imageView.setImageBitmap(bitmap);

        if (isGradient && getChildCount() == 1) {
            final View gradientView = new View(context);
            gradientView.setBackgroundResource(R.drawable.gradient_drawable);
            addView(gradientView, imageParams);
        }
        if (onLoadBitmap != null)
            onLoadBitmap.loadBitmap(bitmap);
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        Log.e("Bitmap Failded","true");
        removeView(progressBar);
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void setImageScaleType(ScaleType scaleType) {
        imageView.setScaleType(scaleType);
    }

    public void setGradient(boolean gradient) {
        isGradient = gradient;
    }

    public interface OnLoadBitmap {
        void loadBitmap(Bitmap bitmap);
    }

    public void setCenterCrop(boolean centerCrop) {
        isCenterCrop = centerCrop;
    }
}
