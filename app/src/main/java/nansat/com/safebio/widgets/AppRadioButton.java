package nansat.com.safebio.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import nansat.com.safebio.R;


public class AppRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    public AppRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode())
            return;

        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.fontStyle);
        final String ttfName = ta.getText(0).toString();

        final Typeface font = Typeface.createFromAsset(context.getAssets(), ttfName + ".ttf");
        setTypeface(font);
        ta.recycle();

    }

}
