package nansat.com.safebio.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;


public class AppScrollView extends ScrollView {

    private int current_page = 1;
    private long mLastEndCall = 0;
    private onScrollListener onScrollListener;

    public void setOnScrollListener(AppScrollView.onScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    public AppScrollView(Context context) {
        super(context);
    }

    public AppScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void resetCurrentPage(){
        this.current_page=1;
    }


    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        View view = (View) getChildAt(getChildCount() - 1);
        int distanceToEnd = (view.getHeight() - (getHeight() + getScrollY()));
        // if difference is zero, then the bottom has been reached

        if (distanceToEnd == 0) {
            // load more data
            current_page+=1;
            onScrollListener.onScrollChanged(current_page);
        }
    }


        public interface onScrollListener {
        void onScrollChanged(int current_page);
    }

}
