package nansat.com.safebio.widgets;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URISyntaxException;

import nansat.com.safebio.utils.Logger;


public class AppWebView extends WebViewClient {

    private Context context;

    public AppWebView(Context context) {
        this.context = context;
    }

    private ProgressDialog progressDialog;

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        showDialog();
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        hideDialog();
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        super.onReceivedHttpError(view, request, errorResponse);
        hideDialog();
    }

    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
        showDialog();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        hideDialog();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("whatsapp")){
            try {
                Intent intent = Intent.parseUri(url.toString(), Intent.URI_INTENT_SCHEME);
                if(intent.resolveActivity(context.getPackageManager()) != null)
                    context.startActivity(intent);
                return true;
            } catch (URISyntaxException use) {
                Log.e("", use.getMessage());
            }
        }else {
            view.loadUrl(url);
        }
        return false;
    }

    private void showDialog() {
        if (progressDialog == null) {
            progressDialog = Logger.showProgressDialog(context, true);
        }
    }

    private void hideDialog() {
        if(progressDialog != null)
            Logger.dismissProgressDialog(progressDialog);
    }


}
