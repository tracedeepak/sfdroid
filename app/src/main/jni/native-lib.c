#include <jni.h>

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getTokenApi(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/get_token");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getSKUs(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/get_active_sku");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getHCFList(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/get_active_hcf");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_saveLot(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/savelot");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getHcfLot(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/gethcflots");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_saveLotAndConfirm(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env,"api/secure/save_and_confirmlot");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_receiveLot(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/receive_lot");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getProductsByCategory(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/get_category_product");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getProductsCategories(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/get_active_categories");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_createOrder(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/create_order");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_registerHCF(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/register_hcf");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getAvailableActivePlans(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "/api/secure/get_active_plans");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_subscribeToPlan(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/subscribe_plan");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getMyAccountDetails(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/get_account_details");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getMyOrders(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/myorders");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getReports(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/collection_report");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getUpdateOrderRequest(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/update_order_payment");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_reportIssueReq(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/report_issue");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_getStates(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/get_all_districts");
}

JNIEXPORT jstring JNICALL
Java_nansat_com_safebio_utils_Constant_sendOrderEnquiry(JNIEnv *env, jclass type) {

    // TODO


    return (*env)->NewStringUTF(env, "api/secure/send_order_enquiry");
}